package com.jgasca.practica1_UD3.com;

import javax.persistence.*;
import java.util.Objects;

@Entity
public class Recambio {
    private int id;
    private String nombre;
    private String cantidad;
    private double precio;
    private TiendaRecambio tienda_recambio;

    @Id
    @Column(name = "id")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "nombre")
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @Basic
    @Column(name = "cantidad")
    public String getCantidad() {
        return cantidad;
    }

    public void setCantidad(String cantidad) {
        this.cantidad = cantidad;
    }

    @Basic
    @Column(name = "precio")
    public double getPrecio() {
        return precio;
    }

    public void setPrecio(double precio) {
        this.precio = precio;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Recambio recambio = (Recambio) o;
        return id == recambio.id &&
                Double.compare(recambio.precio, precio) == 0 &&
                Objects.equals(nombre, recambio.nombre) &&
                Objects.equals(cantidad, recambio.cantidad);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, nombre, cantidad, precio);
    }

    @ManyToOne
    @JoinColumn(name = "id", referencedColumnName = "id_recambio", nullable = false)
    public TiendaRecambio getTienda_recambio() {
        return tienda_recambio;
    }

    public void setTienda_recambio(TiendaRecambio tienda_recambio) {
        this.tienda_recambio = tienda_recambio;
    }
}
