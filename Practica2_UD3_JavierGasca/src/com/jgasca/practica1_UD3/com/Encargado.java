package com.jgasca.practica1_UD3.com;

import javax.persistence.*;
import java.util.Objects;

@Entity
public class Encargado {
    private int id;
    private String nombre;
    private String apellidos;
    private String telefono;
    private TiendaEncargado tienda_encargado;

    @Id
    @Column(name = "id")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "nombre")
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @Basic
    @Column(name = "apellidos")
    public String getApellidos() {
        return apellidos;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    @Basic
    @Column(name = "telefono")
    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Encargado encargado = (Encargado) o;
        return id == encargado.id &&
                Objects.equals(nombre, encargado.nombre) &&
                Objects.equals(apellidos, encargado.apellidos) &&
                Objects.equals(telefono, encargado.telefono);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, nombre, apellidos, telefono);
    }

    @OneToOne(mappedBy = "encargado")
    public TiendaEncargado getTienda_encargado() {
        return tienda_encargado;
    }

    public void setTienda_encargado(TiendaEncargado tienda_encargado) {
        this.tienda_encargado = tienda_encargado;
    }
}
