package com.jgasca.practica1_UD3.com;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "tienda_encargado", schema = "recambios_informatica3", catalog = "")
public class TiendaEncargado {
    private Tienda tienda;
    private Encargado encargado;

    @OneToOne
    @JoinColumn(name = "id_tienda", referencedColumnName = "id", nullable = false)
    public Tienda getTienda() {
        return tienda;
    }

    public void setTienda(Tienda tienda) {
        this.tienda = tienda;
    }

    @OneToOne
    @JoinColumn(name = "id_encargado", referencedColumnName = "id", nullable = false)
    public Encargado getEncargado() {
        return encargado;
    }

    public void setEncargado(Encargado encargado) {
        this.encargado = encargado;
    }
}
