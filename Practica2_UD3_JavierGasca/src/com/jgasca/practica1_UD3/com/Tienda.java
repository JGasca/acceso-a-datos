package com.jgasca.practica1_UD3.com;

import javax.persistence.*;
import java.util.List;
import java.util.Objects;

@Entity
public class Tienda {
    private int id;
    private String codigo;
    private String direccion;
    private String telefono;
    private TiendaEncargado tienda_encargado;
    private TiendaRecambio tienda_recambio;
    private TiendaTrabajador tienda_trabajador;
    private List<Proveedor> proveedor;

    public Tienda (String codigo, String direccion, String telefono){
        this.codigo = codigo;
        this.direccion = direccion;
        this.telefono = telefono;
    }

    public Tienda(){}

    @Id
    @Column(name = "id")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "codigo")
    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    @Basic
    @Column(name = "direccion")
    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    @Basic
    @Column(name = "telefono")
    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Tienda tienda = (Tienda) o;
        return id == tienda.id &&
                Objects.equals(codigo, tienda.codigo) &&
                Objects.equals(direccion, tienda.direccion) &&
                Objects.equals(telefono, tienda.telefono);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, codigo, direccion, telefono);
    }

    @OneToOne(mappedBy = "tienda")
    public TiendaEncargado getTienda_encargado() {
        return tienda_encargado;
    }

    public void setTienda_encargado(TiendaEncargado tienda_encargado) {
        this.tienda_encargado = tienda_encargado;
    }

    @ManyToOne
    @JoinColumn(name = "id", referencedColumnName = "id_tienda", nullable = false)
    public TiendaRecambio getTienda_recambio() {
        return tienda_recambio;
    }

    public void setTienda_recambio(TiendaRecambio tienda_recambio) {
        this.tienda_recambio = tienda_recambio;
    }

    @ManyToOne
    @JoinColumn(name = "id", referencedColumnName = "id_tienda", nullable = false)
    public TiendaTrabajador getTienda_trabajador() {
        return tienda_trabajador;
    }

    public void setTienda_trabajador(TiendaTrabajador tienda_trabajador) {
        this.tienda_trabajador = tienda_trabajador;
    }

    @ManyToMany(mappedBy = "tienda")
    public List<Proveedor> getProveedor() {
        return proveedor;
    }

    public void setProveedor(List<Proveedor> proveedor) {
        this.proveedor = proveedor;
    }
}
