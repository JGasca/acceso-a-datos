package com.jgasca.practica1_UD3.com;

import javax.persistence.*;
import java.util.List;
import java.util.Objects;

@Entity
public class Proveedor {
    private int id;
    private String nombre;
    private String direccion;
    private String telefono;
    private List<Tienda> tienda;

    @Id
    @Column(name = "id")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "nombre")
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @Basic
    @Column(name = "direccion")
    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    @Basic
    @Column(name = "telefono")
    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Proveedor proveedor = (Proveedor) o;
        return id == proveedor.id &&
                Objects.equals(nombre, proveedor.nombre) &&
                Objects.equals(direccion, proveedor.direccion) &&
                Objects.equals(telefono, proveedor.telefono);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, nombre, direccion, telefono);
    }

    @ManyToMany
    @JoinTable(name = "tienda_proveedor", catalog = "", schema = "recambios_informatica3", joinColumns = @JoinColumn(name = "id_proveedor", referencedColumnName = "id", nullable = false), inverseJoinColumns = @JoinColumn(name = "id_tienda", referencedColumnName = "id", nullable = false))
    public List<Tienda> getTienda() {
        return tienda;
    }

    public void setTienda(List<Tienda> tienda) {
        this.tienda = tienda;
    }
}
