package com.jgasca.practica1_UD3.com.gui;

import com.jgasca.practica1_UD3.com.*;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;

import javax.persistence.Query;
import java.util.ArrayList;

/**
 * Clase Modelo
 */
public class Modelo {
    SessionFactory sessionFactory;

    /**
     * Metodo que desconecta con la base de datos
     */
    public void desconectar(){
        if (sessionFactory != null && sessionFactory.isOpen()){
            sessionFactory.close();
        }
    }

    /**
     * Metodoq ue conecta con la base de datos
     */
    public void conectar(){
        Configuration configuracion =  new Configuration();
        configuracion.configure("hibernate.cfg.xml");
        //Tablas
        configuracion.addAnnotatedClass(Encargado.class);
        configuracion.addAnnotatedClass(Proveedor.class);
        configuracion.addAnnotatedClass(Recambio.class);
        configuracion.addAnnotatedClass(Tienda.class);
        configuracion.addAnnotatedClass(Trabajador.class);
        //Trablas intermedias
        configuracion.addAnnotatedClass(TiendaEncargado.class);
        configuracion.addAnnotatedClass(TiendaRecambio.class);
        configuracion.addAnnotatedClass(TiendaTrabajador.class);

        StandardServiceRegistry ssr = new StandardServiceRegistryBuilder().applySettings(configuracion.getProperties()).build();

        sessionFactory = configuracion.buildSessionFactory(ssr);
    }

    //************************ Altas *******************************
    /**
     * Metodo que da de alta un Trabajador
     * @param nuevoTrabajador Objeto de la clase Trabajador
     */
    public void altaTrabajador(Trabajador nuevoTrabajador){
        Session sesion = sessionFactory.openSession();

        sesion.beginTransaction();
        sesion.save(nuevoTrabajador);
        sesion.getTransaction().commit();

        sesion.clear();
    }

    /**
     * Metodo que da de alta un encargado
     * @param nuevoEncargado Objeto de la clase Encargado
     */
    public void altaEncargado(Encargado nuevoEncargado){
        Session sesion = sessionFactory.openSession();

        sesion.beginTransaction();
        sesion.save(nuevoEncargado);
        sesion.getTransaction().commit();

        sesion.clear();
    }

    /**
     * Metodo que da de alta un recambio
     * @param nuevoRecambio Objeto de la clase Recambio
     */
    public void altaRecambio(Recambio nuevoRecambio){
        Session sesion = sessionFactory.openSession();

        sesion.beginTransaction();
        sesion.save(nuevoRecambio);
        sesion.getTransaction().commit();

        sesion.clear();
    }

    /**
     * Metodo que da de alta un tienda
     * @param nuevaTienda Objeto de la clase Tienda
     */
    public void altaTienda(Tienda nuevaTienda){
        Session sesion = sessionFactory.openSession();

        sesion.beginTransaction();
        sesion.save(nuevaTienda);
        sesion.getTransaction().commit();

        sesion.clear();
    }

    //*********************** Borrado *****************************

    /**
     * Metodo que borra una tienda
     * @param trabajador Objeto tienda a borrar
     */
    public void borrarTrabajador(Trabajador trabajador){
        Session session = sessionFactory.openSession();
        session.beginTransaction();

        session.delete(trabajador);
        session.getTransaction().commit();
        session.close();
    }

    /**
     * Metodooq que borra un encargado
     * @param encargado Objeto Encargado a borrar
     */
    public void borrarEncargado(Encargado encargado){
        Session session = sessionFactory.openSession();
        session.beginTransaction();

        session.delete(encargado);
        session.getTransaction().commit();
        session.close();
    }

    /**
     * Metodo que borra un recambio
     * @param recambio Objeto Recambio a borrar
     */
    public void borrarRecambio(Recambio recambio){
        Session session = sessionFactory.openSession();
        session.beginTransaction();

        session.delete(recambio);
        session.getTransaction().commit();
        session.close();
    }

    /**
     * Metodo que borra una tienda
     * @param tienda Objeto Tienda a borrar
     */
    public void borrarTienda(Tienda tienda){
        Session session = sessionFactory.openSession();
        session.beginTransaction();

        session.delete(tienda);
        session.getTransaction().commit();
        session.close();
    }

    /**
     * Metodo que borra un objeto si es de estas clases (Tienda,Recambio,Trabajador,Encargado)
     * @param objeto Objeto a borrar
     */
    public void borrarObjeto(Object objeto){
        if (objeto instanceof Tienda || objeto instanceof Recambio || objeto instanceof Trabajador || objeto instanceof Encargado){
            Session session = sessionFactory.openSession();
            session.beginTransaction();

            session.delete(objeto);
            session.getTransaction().commit();
            session.close();
        }
    }

    //*********************** Modificación ************************

    public void modificarObjeto(Object objeto) {
        Session sesion=sessionFactory.openSession();

        sesion.beginTransaction();
        sesion.saveOrUpdate(objeto);
        sesion.getTransaction().commit();

        sesion.close();
    }

    //*********************** Get de Obsjetos *********************
    public ArrayList<Trabajador> getTrabajadores(){
        Session session = sessionFactory.openSession();
        Query query = session.createQuery("FROM Trabajador");
        ArrayList<Trabajador> lista = (ArrayList<Trabajador>) query.getResultList();
        session.clear();
        return lista;
    }

    public ArrayList<Encargado> getEncargados(){
        Session session = sessionFactory.openSession();
        Query query = session.createQuery("FROM Encargado");
        ArrayList<Encargado> lista = (ArrayList<Encargado>) query.getResultList();
        session.clear();
        return lista;
    }

    public ArrayList<Recambio> getRecambios(){
        Session session = sessionFactory.openSession();
        Query query = session.createQuery("FROM Recambio");
        ArrayList<Recambio> lista = (ArrayList<Recambio>) query.getResultList();
        session.clear();
        return lista;
    }

    public ArrayList<Tienda> getTiendas(){
        Session session = sessionFactory.openSession();
        Query query = session.createQuery("FROM Tienda");
        ArrayList<Tienda> lista = (ArrayList<Tienda>) query.getResultList();
        session.clear();
        return lista;
    }
}
