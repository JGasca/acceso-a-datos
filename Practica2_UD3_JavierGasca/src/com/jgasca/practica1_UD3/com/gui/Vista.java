package com.jgasca.practica1_UD3.com.gui;

import javax.swing.*;

public class Vista {
    JFrame frame;
    JPanel panel1;
    private JTabbedPane tabbedPane1;
    JTextField txtNombreTrabajador;
    JTextField txtNombreRecambio;
    JTextField txtCantidadRecambio;
    JTextField txtPrecioRecambio;
    JTextField txtCodigoTienda;
    JTextField txtDireccionTienda;
    JTextField txtTelefonoTienda;
    JTextField txtNombreEncargado;
    JTextField txtApellidosEncargado;
    JTextField txtTelefonoEncargado;
    JTextField txtApellidosTrabajador;
    JTextField txtTelefonoTrabajador;
    JButton btnNuevoRecambio;
    JButton btnEliminarRecambio;
    JButton btnModificarRecambio;
    JButton btnNuevoTienda;
    JButton btnEliminarTienda;
    JButton btnModificarTienda;
    JButton btnNuevoEncargado;
    JButton btnEliminarEncargado;
    JButton btnModificarEncargado;
    JButton btnNuevoTrabajador;
    JButton btnEliminarTrabajador;
    JButton btnModificarTrabajador;
    JList listRecambios;
    JList listTienda;
    JList listEncargado;
    JList listTrabajador;

    JMenuItem conexionItem;
    JMenuItem itemConectar;
    JMenuItem itemSalir;

    public Vista(){
        frame = new JFrame("Vista");
        frame.setContentPane(panel1);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);

        crearMenu();
        //crearModelos();

        frame.setLocationRelativeTo(null);
    }

    private void crearMenu() {
        itemConectar = new JMenuItem("Conectar");
        itemConectar.setActionCommand("Conectar");
        itemSalir = new JMenuItem("Salir");
        itemSalir.setActionCommand("Salir");

        JMenu menuArchivo = new JMenu("Archivo");
        menuArchivo.add(itemConectar);
        menuArchivo.add(itemSalir);
        JMenuBar barraMenu = new JMenuBar();
        barraMenu.add(menuArchivo);

        frame.setJMenuBar(barraMenu);
        System.out.println("Hola aqui llega");
    }


    private void createUIComponents() {
        // TODO: place custom component creation code here
    }
}
