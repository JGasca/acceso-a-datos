package com.jgasca.practica1_UD3.com.gui;

import com.jgasca.practica1_UD3.com.Encargado;
import com.jgasca.practica1_UD3.com.Recambio;
import com.jgasca.practica1_UD3.com.Tienda;
import com.jgasca.practica1_UD3.com.Trabajador;

import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.event.TableColumnModelEvent;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Controlador implements ActionListener, ListSelectionListener {
    private Vista vista;
    private Modelo modelo;

    public Controlador(Vista vista, Modelo modelo) {
        this.vista = vista;
        this.modelo = modelo;

        addActionListeners(this);
        addListSelectionListener(this);
    }


    public void addActionListeners(ActionListener listener){
        //Conectar
        vista.itemConectar.addActionListener(listener);
        //Boton Nuevo
        vista.btnNuevoRecambio.addActionListener(listener);
        vista.btnNuevoEncargado.addActionListener(listener);
        vista.btnNuevoTienda.addActionListener(listener);
        vista.btnNuevoTrabajador.addActionListener(listener);
        //Boton Eliminar
        vista.btnEliminarRecambio.addActionListener(listener);
        vista.btnEliminarEncargado.addActionListener(listener);
        vista.btnEliminarTienda.addActionListener(listener);
        vista.btnEliminarTrabajador.addActionListener(listener);
        //Boton Modificar
        vista.btnModificarRecambio.addActionListener(listener);
        vista.btnModificarEncargado.addActionListener(listener);
        vista.btnModificarTienda.addActionListener(listener);
        vista.btnModificarTrabajador.addActionListener(listener);
    }

    public void addListSelectionListener(ListSelectionListener listener){
        vista.listEncargado.addListSelectionListener(listener);
        vista.listRecambios.addListSelectionListener(listener);
        vista.listTienda.addListSelectionListener(listener);
        vista.listTrabajador.addListSelectionListener(listener);

    }
    @Override
    public void actionPerformed(ActionEvent e) {
        String comando = e.getActionCommand();

        switch (comando){
            case "Conectar":
                vista.conexionItem.setEnabled(false);
                modelo.conectar();
                break;
            case "NuevoRecambio":
                Recambio recambio = new Recambio();
                recambio.setNombre(vista.txtNombreRecambio.getText());
                recambio.setCantidad(vista.txtCantidadRecambio.getText());
                recambio.setPrecio(Double.parseDouble(vista.txtPrecioRecambio.getText()));
                modelo.altaRecambio(recambio);
                break;
            case "EliminarRecambio":
                Recambio recambio2 = (Recambio) vista.listRecambios.getSelectedValue();
                modelo.borrarRecambio(recambio2);
                break;
            case "ModificarRecambio":

                break;
            case "NuevoTienda":
                Tienda tienda = new Tienda();
                tienda.setCodigo(vista.txtCodigoTienda.getText());
                tienda.setDireccion(vista.txtDireccionTienda.getText());
                tienda.setTelefono(vista.txtTelefonoTienda.getText());
                modelo.altaTienda(tienda);
                break;
            case "EliminarTienda":
                Tienda Tienda2 = (Tienda) vista.listTienda.getSelectedValue();
                modelo.borrarTienda(Tienda2);
                break;
            case "ModificarTienda":

                break;
            case "NuevoEncargado":
                Encargado encargado = new Encargado();
                encargado.setNombre(vista.txtNombreEncargado.getText());
                encargado.setApellidos(vista.txtApellidosEncargado.getText());
                encargado.setTelefono(vista.txtTelefonoEncargado.getText());
                modelo.altaEncargado(encargado);
                break;
            case "EliminarEncargado":
                Encargado encargado2 = (Encargado) vista.listEncargado.getSelectedValue();
                modelo.borrarEncargado(encargado2);
                break;
            case "ModificarEncargado":

                break;
            case "NuevoTrabajador":
                Trabajador trabajador = new Trabajador();
                trabajador.setNombre(vista.txtNombreTrabajador.getText());
                trabajador.setApellidos(vista.txtNombreTrabajador.getText());
                trabajador.setTelefono(vista.txtTelefonoTrabajador.getText());
                modelo.altaTrabajador(trabajador);
                break;
            case "EliminarTrabajador":
                Trabajador trabajador2 = (Trabajador) vista.listTrabajador.getSelectedValue();
                modelo.borrarTrabajador(trabajador2);
                break;
            case "ModificarTrabajador":

                break;
        }
    }

    @Override
    public void valueChanged(ListSelectionEvent e) {
        if(e.getValueIsAdjusting()){
            if(e.getSource() == vista.listTrabajador) {
                Trabajador trabajador = (Trabajador) vista.listTrabajador.getSelectedValue();
                vista.txtNombreTrabajador.setText(String.valueOf(trabajador.getNombre()));
                vista.txtApellidosTrabajador.setText(String.valueOf(trabajador.getTelefono()));
                vista.txtTelefonoTrabajador.setText(String.valueOf(trabajador.getTelefono()));
            }
            if(e.getSource() == vista.listTienda) {
                Tienda tienda = (Tienda) vista.listTienda.getSelectedValue();
                vista.txtCodigoTienda.setText(String.valueOf(tienda.getCodigo()));
                vista.txtDireccionTienda.setText(String.valueOf(tienda.getDireccion()));
                vista.txtTelefonoTienda.setText(String.valueOf(tienda.getTelefono()));
            }
            if(e.getSource() == vista.listRecambios) {
                Recambio recambio = (Recambio) vista.listRecambios.getSelectedValue();
                vista.txtNombreRecambio.setText(String.valueOf(recambio.getNombre()));
                vista.txtCantidadRecambio.setText(String.valueOf(recambio.getCantidad()));
                vista.txtPrecioRecambio.setText(String.valueOf(recambio.getPrecio()));
            }
            if(e.getSource() == vista.listEncargado) {
                Encargado encargado = (Encargado) vista.listEncargado.getSelectedValue();
                vista.txtNombreEncargado.setText(String.valueOf(encargado.getNombre()));
                vista.txtApellidosEncargado.setText(String.valueOf(encargado.getApellidos()));
                vista.txtTelefonoEncargado.setText(String.valueOf(encargado.getTelefono()));
            }
        }
    }
}
