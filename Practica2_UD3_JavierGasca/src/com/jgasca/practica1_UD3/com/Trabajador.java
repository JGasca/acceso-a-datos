package com.jgasca.practica1_UD3.com;

import javax.persistence.*;
import java.util.Objects;

@Entity
public class Trabajador {
    private int id;
    private String nombre;
    private String apellidos;
    private String telefono;
    private TiendaTrabajador tienda_trabajador;

    @Id
    @Column(name = "id")
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "nombre")
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @Basic
    @Column(name = "apellidos")
    public String getApellidos() {
        return apellidos;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    @Basic
    @Column(name = "telefono")
    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Trabajador that = (Trabajador) o;
        return id == that.id &&
                Objects.equals(nombre, that.nombre) &&
                Objects.equals(apellidos, that.apellidos) &&
                Objects.equals(telefono, that.telefono);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, nombre, apellidos, telefono);
    }

    @OneToOne(mappedBy = "trabajador")
    public TiendaTrabajador getTienda_trabajador() {
        return tienda_trabajador;
    }

    public void setTienda_trabajador(TiendaTrabajador tienda_trabajador) {
        this.tienda_trabajador = tienda_trabajador;
    }
}
