package com.jgasca.practica1_UD3.com;

import javax.persistence.*;
import java.sql.Date;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name = "tienda_trabajador", schema = "recambios_informatica3", catalog = "")
public class TiendaTrabajador {
    private Date fecha;
    private Trabajador trabajador;
    private List<Tienda> tienda;

    @Basic
    @Column(name = "fecha")
    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TiendaTrabajador that = (TiendaTrabajador) o;
        return Objects.equals(fecha, that.fecha);
    }

    @Override
    public int hashCode() {
        return Objects.hash(fecha);
    }

    @OneToOne
    @JoinColumn(name = "id_trabajador", referencedColumnName = "id", nullable = false)
    public Trabajador getTrabajador() {
        return trabajador;
    }

    public void setTrabajador(Trabajador trabajador) {
        this.trabajador = trabajador;
    }

    @OneToMany(mappedBy = "tienda_trabajador")
    public List<Tienda> getTienda() {
        return tienda;
    }

    public void setTienda(List<Tienda> tienda) {
        this.tienda = tienda;
    }
}
