package com.jgasca.practica1_UD3.com;

import javax.persistence.*;
import java.sql.Date;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name = "tienda_recambio", schema = "recambios_informatica3", catalog = "")
public class TiendaRecambio {
    private Date fecha;
    private List<Tienda> tienda;
    private List<Recambio> recambio;

    @Basic
    @Column(name = "fecha")
    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TiendaRecambio that = (TiendaRecambio) o;
        return Objects.equals(fecha, that.fecha);
    }

    @Override
    public int hashCode() {
        return Objects.hash(fecha);
    }

    @OneToMany(mappedBy = "tienda_recambio")
    public List<Tienda> getTienda() {
        return tienda;
    }

    public void setTienda(List<Tienda> tienda) {
        this.tienda = tienda;
    }

    @OneToMany(mappedBy = "tienda_recambio")
    public List<Recambio> getRecambio() {
        return recambio;
    }

    public void setRecambio(List<Recambio> recambio) {
        this.recambio = recambio;
    }
}
