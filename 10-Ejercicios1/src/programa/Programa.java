package programa;

import clases.GestorTrabajos;

public class Programa {

	public static void main(String[] args) {
		
		//1- Crear instancia de GestorTrabajos.
		GestorTrabajos gestor = new GestorTrabajos();
		
		//2- Alta de 3 Responsables uno con dni repetido.
		gestor.altaResponsable("752125364A", "Federico", "2020-02-20");
		gestor.altaResponsable("752654211B", "Marco", "2020-09-25");
		gestor.altaResponsable("831548264C", "Javier", "1995-11-05");
		
		//3- Listar Responsables.
		System.out.println("------ Listado de los responsables ------");
		gestor.listarResponsables();
		
		//4- Busca a un Responsable y muestra sus datos, y busca a otro que no exista.
		System.out.println("------ Buscando un responsable ------");
		System.out.println(gestor.buscarResponsables("752654211B"));
		System.out.println(gestor.buscarResponsables("752115211B"));
		
		//5- Dar de alta 3 Trabajos (dos con fecha de 2019, y uno de 2020).
		gestor.altaTrabajo("caldera", "Fernando", 2000, "2020-05-02");
		gestor.altaTrabajo("suelo", "Pedro", 2000, "2019-12-12");
		gestor.altaTrabajo("armarios", "Carlos", 2000, "2019-07-12");
		
		//6- Asignar el mismo Responsable a dos Trabajos.
		gestor.asignarResponsables("752654211B", "caldera");
		gestor.asignarResponsables("831548264C", "caldera");
		
		//7- Listar los Trabajos del Responsable anterior.
		System.out.println("----- Listando trabajos de responsable -----");
		gestor.listarTrabajosDeResponsable("831548264C");
		//8- Listar los Trabajos de 2020.
		System.out.println("----- Listado de los trabajos de 2019");
		gestor.listarTrabajosAnno(2019);
		//9- Eliminar un trabajo que tenga asignado un Responsable.
		System.out.println("----- Eliminamos un trabajo -----");
		gestor.eliminarTrabajo("caldera");
		//10- Lista de nuevo los Trabajos de ese Responsable.
		System.out.println("---- Listamos de nuevo los trabajos del responsable -----");
		gestor.listarTrabajosDeResponsable("831548264C");
		//11- Llama al m�todo ere() y lista de nuevo los Responsables
		System.out.println("Listado de los responsables");
		gestor.ere();
	}

}
