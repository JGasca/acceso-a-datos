package clases;

import java.time.LocalDate;
import java.util.ArrayList;

public class GestorTrabajos {
	private ArrayList<Trabajo> listaTrabajos;
	private ArrayList<Responsable> listaResponsables;
	
	public GestorTrabajos() {
		this.listaTrabajos = new ArrayList<Trabajo>();
		this.listaResponsables = new ArrayList<Responsable>();
	}

	public ArrayList<Trabajo> getListaTrabajos() {
		return listaTrabajos;
	}

	public void setListaTrabajos(ArrayList<Trabajo> listaTrabajos) {
		this.listaTrabajos = listaTrabajos;
	}

	public ArrayList<Responsable> getListaResponsables() {
		return listaResponsables;
	}

	public void setListaResponsables(ArrayList<Responsable> listaResponsables) {
		this.listaResponsables = listaResponsables;
	}
	
	public void altaResponsable(String dni , String nombre, String fechaContratacion) {
		for (int i = 0; i < listaResponsables.size(); i++) {
			if (listaResponsables.get(i).getDni().equals(dni)) {
				break;
			}
		}
		Responsable res1 = new Responsable(dni, nombre);
		res1.setFechaContratacion(LocalDate.parse(fechaContratacion));
		listaResponsables.add(res1);
	}
	
	public void listarResponsables() {
		for (int i = 0; i < listaResponsables.size(); i++) {
			System.out.println(listaResponsables.get(i));
		}
	}
	
	public Responsable buscarResponsables(String dni) {
		for (int i = 0; i < listaResponsables.size(); i++) {
			if (listaResponsables.get(i).getDni().equals(dni)) {
				return listaResponsables.get(i);
			}
		}
		return null;
	}
	
	public void altaTrabajo(String nombre, String cliente, double presupuesto, String fechaconsecion) {
		Trabajo trab1 = new Trabajo(nombre, cliente, presupuesto);
		trab1.setFechaConcesion(LocalDate.parse(fechaconsecion));
		listaTrabajos.add(trab1);
	}
	
	public void eliminarTrabajo(String nombre) {
		for (int i = 0; i < listaTrabajos.size(); i++) {
			if (listaTrabajos.get(i).getNombre().equals(nombre)) {
				listaTrabajos.remove(i);
			}
		}
	}
	
	public void asignarResponsables(String dni, String nombreTrabajo) {
		Trabajo trab2 = buscarTrabajo(nombreTrabajo);
		if (trab2 != null) {
			Responsable res1 = buscarResponsables(dni);
			if (res1 != null) {
				trab2.setResponsableTrabajo(res1);
			}
		}
		
	}
	
	public Trabajo buscarTrabajo(String nombreTrabajo) {
		for (int i = 0; i < listaTrabajos.size(); i++) {
			if (listaTrabajos.get(i).getNombre().equals(nombreTrabajo)) {
				return listaTrabajos.get(i);
			}
		}
		return null;
	}
	
	public void asignarResponsableExperto(String nombreTrabajo) {
		Trabajo trabajo = buscarTrabajo(nombreTrabajo);
		if (trabajo != null) {
			trabajo.setResponsableTrabajo(buscarResponsableExperto());
		}
	}
	
	public Responsable buscarResponsableExperto() {
		LocalDate fechaAntigua = null;
		for (int i = 0; i < listaResponsables.size(); i++) {
			Responsable responsableActual = listaResponsables.get(i);
			if (responsableActual != null && i == 0) {
				fechaAntigua = responsableActual.getFechaContratacion();
			}else {
				if (responsableActual != null && responsableActual.getFechaContratacion().isAfter(fechaAntigua)) {
					fechaAntigua = responsableActual.getFechaContratacion();
				}
			}
		}
		for (Responsable responsable : listaResponsables) {
			if (responsable != null && responsable.getFechaContratacion().equals(fechaAntigua)) {
				return responsable;
			}
		}
		return null;
	}
	
	public void listarTrabajosDeResponsable(String dni) {		
		for (Trabajo trabajo : listaTrabajos) {
			if (trabajo.getResponsableTrabajo() != null && trabajo.getResponsableTrabajo().getDni().equals(dni)) {
				System.out.println(trabajo);
			}
		}
	}
	
	public void listarTrabajosAnno(int anno) {
		for (Trabajo trabajo: listaTrabajos) {
			if (trabajo.getFechaConcesion().getYear() == anno) {
				System.out.println(trabajo);
			}
			
		}
	}
	
	public void ere() {
		for(Responsable responsable: listaResponsables) {
			boolean estaEnTrabajo = false;
			for(Trabajo trabajo: listaTrabajos) {
				if (trabajo.getResponsableTrabajo() != null && trabajo.getResponsableTrabajo().getDni().equals(responsable.getDni())) {
					estaEnTrabajo = true;
				}
			}
			if (!estaEnTrabajo) {
				responsable = null;
			}
		}
	}
	
	
}
