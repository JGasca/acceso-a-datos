package base;

import jdk.internal.org.xml.sax.SAXException;
import org.w3c.dom.*;

import javax.swing.*;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.*;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.util.LinkedList;

public class Vista {
    private JFrame frame;
    private JPanel panel1;
    private JLabel lblMarcaCoche;
    private JTextField marcaTxt;
    private JLabel lblModeloCoche;
    private JTextField modeloTxt;
    private JComboBox comboBox;
    private JButton altaCocheBtn;
    private JButton mostrarCocheBtn;
    private JLabel lblCoche;

    //Elementos añadidos por mi
    private LinkedList<Coche> lista;
    private DefaultComboBoxModel<Coche> dtm;

    public Vista() {
        frame = new JFrame("Vista");
        frame.setContentPane(panel1);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setSize(500,400);
        //frame.pack();
        frame.setVisible(true);
        crearMenu();
        frame.setLocationRelativeTo(null);

        lista = new LinkedList<>();
        dtm = new DefaultComboBoxModel<>();

        comboBox.setModel(dtm);
        altaCocheBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                //Dar de alta el coche
                altaCoche(marcaTxt.getText(),modeloTxt.getText());
                //listar coches de la lista del combo
                refrescarComboBox();
            }
        });
        mostrarCocheBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                //Muestro el coche seleccionado
                Coche seleccionado = (Coche) dtm.getSelectedItem();
                lblCoche.setText(seleccionado.toString());
            }
        });
    }

    private void refrescarComboBox(){
        dtm.removeAllElements();
        for (Coche coche :
                lista) {
            dtm.addElement(coche);
        }
    }

    public static void main(String[] args) {
        Vista vista = new Vista();
    }

    public void altaCoche(String marca, String modelo){
        lista.add(new Coche(marca, modelo));
        modeloTxt.setText("");
        marcaTxt.setText("");
    }

    private void crearMenu(){
        JMenuBar barra = new JMenuBar();
        JMenu menu =  new JMenu("Archivo");
        JMenuItem itemExportarXML = new JMenuItem("Exportar XML");
        JMenuItem itemImportarXML = new JMenuItem("Importar XML");

        menu.add(itemExportarXML);
        menu.add(itemImportarXML);

        itemExportarXML.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                JFileChooser selectorArchivo = new JFileChooser();
                int opcionSeleccionada = selectorArchivo.showSaveDialog(null);
                if (opcionSeleccionada == JFileChooser.APPROVE_OPTION){
                    File fichero = selectorArchivo.getSelectedFile();
                    exportarrXML(fichero);
                }
            }
        });

        itemImportarXML.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                JFileChooser selectorArchivo = new JFileChooser();
                int opcion = selectorArchivo.showOpenDialog(null);
                if (opcion == JFileChooser.APPROVE_OPTION){
                    File fichero = selectorArchivo.getSelectedFile();
                    importarXML(fichero);
                    refrescarComboBox();
                }
            }
        });


        barra.add(menu);
        frame.setJMenuBar(barra);
    }

    private void importarXML(File fichero){
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        try {
            DocumentBuilder builder = factory.newDocumentBuilder();
            Document documento = builder.parse(fichero);

            //Recorro cada uno de los nodos coche para obtener sus campos
            NodeList coches = documento.getElementsByTagName("coche");
            for (int i = 0; i < coches.getLength(); i++) {
                Node coche = coches.item(i);
                Element elemento = (Element) coche;

                //Obtengo los campos marca y modelo
                String marca = elemento.getElementsByTagName("marca").item(0).getChildNodes().item(0).getNodeValue();
                String modelo = elemento.getElementsByTagName("modelo").item(0).getChildNodes().item(0).getNodeValue();

                altaCoche(marca, modelo);
            }
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (org.xml.sax.SAXException e) {
            e.printStackTrace();
        }
    }

    private void exportarrXML(File fichero){
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = null;

        try {
            builder = factory.newDocumentBuilder();
            DOMImplementation dom = builder.getDOMImplementation();

            //Creo documento que representa arbol XML
            Document documento = dom.createDocument(null, "xml", null);

            //Creo el nodo raiz (coches) y lo añado al documento
            Element raiz = documento.createElement("coches");
            documento.getDocumentElement().appendChild(raiz);

            Element nodoCoche;
            Element nodoDatos;
            Text dato;

            //Por cada coche de la lista, creo un nodo coche
            for (Coche coche : lista) {

                //Creo un nodo coche y lo añado al nodo raiz (coches)
                nodoCoche = documento.createElement("coche");
                raiz.appendChild(nodoCoche);

                //A cada nodo coche le añado los nodos marca y modelo
                nodoDatos = documento.createElement("marca");
                nodoCoche.appendChild(nodoDatos);

                //A cada nodo de datos le añado el dato
                dato = documento.createTextNode(coche.getMarca());
                nodoDatos.appendChild(dato);

                nodoDatos = documento.createElement("modelo");
                nodoCoche.appendChild(nodoDatos);

                dato = documento.createTextNode(coche.getModelo());
                nodoDatos.appendChild(dato);
            }

            //Transformo el documento anterior en un ficho de texto plano
            Source src = new DOMSource(documento);
            Result result = new StreamResult(fichero);

            Transformer transformer = null;
            transformer = TransformerFactory.newInstance().newTransformer();
            transformer.transform(src, result);

        } catch (TransformerConfigurationException e) {
            e.printStackTrace();
        } catch (TransformerException e) {
            e.printStackTrace();
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        }
    }
}
