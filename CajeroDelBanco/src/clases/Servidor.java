package clases;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

public class Servidor {
    public static final String ANSI_GREEN = "\u001B[32m";
    public static final String ANSI_RESET = "\u001B[0m";
    public static final String ANSI_CYAN = "\u001B[36m";
    private ServerSocket socketServidor;
    private int puerto;

    public Servidor(int puerto){
        this.puerto = puerto;
        arrancarServidor();
    }

    public void arrancarServidor(){
        try {
            socketServidor = new ServerSocket(this.puerto);
            System.out.println(ANSI_GREEN + "*** Servidor arrancado ***" + ANSI_RESET);
            String exit = "";
            do {
                Socket cliente = socketServidor.accept();
                System.out.println(ANSI_CYAN + "*** Nuevo cliente ***" + ANSI_RESET);
                String nombre = "";
                String contra = "";
                PrintStream salida = new PrintStream(cliente.getOutputStream());
                DataInputStream entrada = new DataInputStream(cliente.getInputStream());
                try{
                    do {
                        nombre = entrada.readLine();
                        System.out.println("Nombre recibido --> " + nombre);
                        contra = entrada.readLine();
                        System.out.println("Contraseña recibida --> " + contra);
                    }while (!comprobarNombreContrasena(nombre, contra, salida));
                    String num = null;
                    do {
                        num = entrada.readLine();
                        System.out.println("Numero recibido --> " + num);
                        nombre = entrada.readLine();
                        System.out.println("Nombre recibido --> " + nombre);
                        switch (num){
                            case "1":
                                String saldo = revisarSaldo(nombre);
                                salida.println(saldo);
                                break;
                            case "2":
                                String dinero = entrada.readLine();
                                System.out.println("Dinero recibido para sacar --> " + dinero);
                                if (sacarDinero( nombre,dinero)){
                                    salida.println(true);
                                }else {
                                    salida.println(false);
                                }
                                break;
                            case "3":
                                String dinero2 = entrada.readLine();
                                System.out.println("Dinero recibido para meter --> " + dinero2);
                                if (meterDinero(nombre,dinero2)){
                                    salida.println(true);
                                }else {
                                    salida.println(false);
                                }
                                break;
                            case "5":
                                String movimientos = movimientos(nombre);
                                movimientos = movimientos.replace("\n","||");
                                salida.println(movimientos);
                                break;
                        }

                    }while (!num.equals("4"));

                }catch (SocketException e){
                    System.err.println("*** Cliente desconectado da manera inesperada ***" );
                    exit = "exit";
                }
            }while (exit.equals("exit"));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private boolean comprobarNombreContrasena(String nombre,String contra ,PrintStream salida){
        File archivo = new File(nombre+".properties");
        System.out.println(archivo);
        if (archivo.exists() && comprobarContra(contra,nombre)){
            salida.println(true);
            return true;
        }
        salida.println(false);
        return false;
    }

    /**
     * Metodo que comprueba si la contraseña introducida y la que esta almacenada son iguales
     * @param contra Contraseña introducida por parametro
     * @param nombre Nombre del tirular de la cuenta
     * @return Devuelve true o false en función de si son iguales las contraseñas
     */
    private boolean comprobarContra(String contra, String nombre){
        InputStream inputStream = null;
        try {
            Properties prop = new Properties();
            String propFileName = nombre+".properties";

            inputStream = new FileInputStream(propFileName);

            prop.load(inputStream);
            String contrasena = prop.getProperty("pass");
            if (contrasena.equals(contra)){
                return true;
            }
        } catch (Exception e) {
            System.out.println("Exception: " + e);
        } finally {
            try {
                if (inputStream != null) inputStream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return false;
    }

    public static void main(String[] args) {
        Servidor server = new Servidor(4444);
    }

    /**
     * Metodo que devuelve el saldo de una cuenta
     * @param nombre Nombre de la persona que solicita el saldo
     * @return Devuelve un String con el saldo
     */
    private String revisarSaldo(String nombre){
        InputStream inputStream = null;
        try {
            Properties prop = new Properties();
            String propFileName = nombre+".properties";

            inputStream = new FileInputStream(propFileName);

            prop.load(inputStream);
            String saldo = prop.getProperty("saldo");
            System.out.println(ANSI_GREEN + "--- Saldo revisado ---" + ANSI_RESET);
            return saldo;
        } catch (Exception e) {
            System.out.println("Exception: " + e);
        } finally {
            try {
                if (inputStream != null) inputStream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return null;
    }

    private boolean sacarDinero(String nombre, String dineroFuera){
        Float saldoActual = Float.parseFloat(revisarSaldo(nombre));
        Float dineroFuera2 = Float.parseFloat(dineroFuera);
        try {
            List<String> lista = obtenerTodosDatos(nombre);
            Properties prop = new Properties();
            Float dinero = saldoActual - dineroFuera2;
            prop.setProperty("pass", lista.get(0));
            prop.setProperty("saldo", dinero + "");
            prop.setProperty("movimientos", lista.get(1) + "\n" + saldoActual + "--> -" + dineroFuera2 + " --> " + dinero);


            OutputStream out = new FileOutputStream(nombre+".properties");
            prop.store(out, null);
            System.out.println(ANSI_GREEN + "--- Dinero sacado ---" + ANSI_RESET);
            return true;
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return false;
    }

    private boolean meterDinero(String nombre, String dineroDentro){
        Float saldoActual = Float.parseFloat(revisarSaldo(nombre));
        Float dineroDentro2 = Float.parseFloat(dineroDentro);
        try {
            List<String> lista = obtenerTodosDatos(nombre);
            Properties prop = new Properties();
            Float dinero = saldoActual + dineroDentro2;
            prop.setProperty("pass", lista.get(0));
            prop.setProperty("saldo", dinero + "");
            prop.setProperty("movimientos", lista.get(1) + "\n" + saldoActual + "--> +" + dineroDentro2 + " --> " + dinero);

            OutputStream out = new FileOutputStream(nombre+".properties");
            prop.store(out, null);
            System.out.println(ANSI_GREEN + "--- Dinero metido en la cuenta ---" + ANSI_RESET);
            return true;
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return false;
    }

    private String movimientos(String nombre){
        List<String> lista = obtenerTodosDatos(nombre);
        return lista.get(1);
    }

    private List obtenerTodosDatos(String nombre){
        InputStream inputStream = null;
        List<String> lista = new ArrayList();
        try {
            Properties prop = new Properties();
            String propFileName = nombre+".properties";

            inputStream = new FileInputStream(propFileName);

            prop.load(inputStream);
            lista.add(prop.getProperty("pass"));
            lista.add(prop.getProperty("movimientos"));

        } catch (Exception e) {
            System.out.println("Exception: " + e);
        } finally {
            try {
                if (inputStream != null) inputStream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return lista;
    }
}
