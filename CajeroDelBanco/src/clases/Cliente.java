package clases;

import Util.Util;

import java.io.DataInputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.net.ConnectException;
import java.net.Socket;
import java.util.Scanner;

public class Cliente {
    public static final String ANSI_GREEN = "\u001B[32m";
    public static final String ANSI_RESET = "\u001B[0m";
    protected Socket sk;
    private int puerto;
    private String host;
    private DataInputStream entrada;
    private PrintStream salida;
    private int ini = 0;
    private String nombre;

    public Cliente(String host, int puerto){
        this.puerto = puerto;
        this.host = host;
    }

    public static void main(String[] args) {
        Cliente cliente = new Cliente("localhost",4444);
        try {
            cliente.conectar();
            boolean conectado = false;
            String nombre=null;
            /**
             * Do/while que establece si hemos conectado o no con nuestro usuario
             */
            do {
                cliente.setNombre(null);
                conectado =  cliente.menuLogin();
            }while (!conectado);
            System.out.println(ANSI_GREEN + "*** Login correcto ***" + ANSI_RESET);
            cliente.menuCliente();

        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public void menuCliente(){
        boolean numero = false;
        String num = null;
        do {
            System.out.println("*** Menu cliente ***");
            System.out.println("1-Revisar saldo\n2-Sacar dinero\n3-Meter Dinero\n4-Salir\n5-Movimientos\nSelecciona un numero de la lista:");
            Scanner scan = new Scanner(System.in);
            num =  scan.nextLine().toLowerCase();
            if (!isNumeric(num) || Integer.parseInt(num) > 5){
                System.err.println("No has introducido un numero valido");
            }else {
                enviarNumMenuCliente(num);
                enviarNombre(nombre);
                switch (num){
                    case "1":
                        System.out.println("En su cuenta hay " + recibirDato() + "€");
                        break;
                    case "2":
                        String money = null;
                        do {
                            System.out.println("Dinero a sacar:");
                            money = scan.nextLine().toLowerCase();
                            if (!isNumeric(money)){
                                System.err.println("No has introducido un numero");
                            }else{
                                salida.println(money);
                                try {
                                    if (Boolean.parseBoolean(entrada.readLine())){
                                        System.out.println(ANSI_GREEN + "Dinero sacado satisfactoriamente" + ANSI_RESET);
                                    }else if (!Boolean.parseBoolean(entrada.readLine())){
                                        System.err.println("--- Error al sacar dinero ---");
                                    }

                                } catch (IOException e) {
                                    e.printStackTrace();
                                }
                            }
                        }while (!isNumeric(money));

                        break;
                    case "3":
                        money = null;
                        do {
                            System.out.println("Dinero a meter:");
                            money = scan.nextLine().toLowerCase();
                            if (!isNumeric(money)){
                                System.err.println("No has introducido un numero");
                            }else{
                                salida.println(money);
                                try {
                                    if (Boolean.parseBoolean(entrada.readLine())){
                                        System.out.println(ANSI_GREEN + "Dinero introducido satisfactoriamente" + ANSI_RESET);
                                    }else if (!Boolean.parseBoolean(entrada.readLine())){
                                        System.err.println("--- Error al meter dinero ---");
                                    }

                                } catch (IOException e) {
                                    e.printStackTrace();
                                }
                            }
                        }while (!isNumeric(money));
                        break;
                    case "5":

                        try {
                            enviarNombre(nombre);
                            String movimientos = entrada.readLine();
                            movimientos = movimientos.replace("||","\n");
                            System.out.println("*** Movimientos ***" + movimientos + "\n");
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        break;
                }
            }
        }while (!numero && !num.equals("4"));

    }

    public Boolean menuLogin(){
        String nombre = null;
        String contra= null;
        Scanner scan = new Scanner(System.in);
        System.out.println("*** Login ***");
        System.out.println("Introduce un nombre:");
        nombre = scan.nextLine().toLowerCase();
        setNombre(nombre);
        enviarNombre(nombre);
        System.out.println("Introduce la contraseña:");
        contra = scan.nextLine().toLowerCase();
        enviarContra(contra);

        boolean conectado = false;
        conectado = Boolean.parseBoolean(recibirDato());
        if (!conectado){
            System.err.println("**** Error, el nombre o la contraseña son incorrectos");
        }

        return conectado;
    }

    public void enviarNombre(String nombre){
        salida.println(nombre);
    }
    public void enviarContra(String contra){
        salida.println(contra);
    }
    public void enviarNumMenuCliente(String num){
        salida.println(num);
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String recibirDato(){
        try {
            return entrada.readLine();
        } catch (IOException e) {
            System.err.println("*** Error del servidor ***");
        }
        return null;
    }

    public void conectar() throws IOException {
        try{
            sk = new Socket(host, puerto);
            ini = 1;
        }catch (ConnectException e){
            if (ini == 0){
                System.err.println("*** !!!MELON¡¡¡ no has encendido el servidor ***");
                System.exit(0);
            }
        }

        entrada = new DataInputStream(sk.getInputStream());
        salida = new PrintStream(sk.getOutputStream());
    }

    /**
     * Metodo que comprueba si una cadena solo contiene numeros
     * @param cadena String cadena a analizar
     * @return Boolean true o false
     */
    private boolean isNumeric(String cadena) {
        boolean resultado;
        try {
            Integer.parseInt(cadena);
            resultado = true;
        } catch (NumberFormatException excepcion) {
            try {
                Float.parseFloat(cadena);
                resultado = true;
            }catch (NumberFormatException excepcion2){
                resultado = false;
            }
        }
        return resultado;
    }
}
