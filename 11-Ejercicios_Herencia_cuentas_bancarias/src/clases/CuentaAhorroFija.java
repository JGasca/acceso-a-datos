package clases;

public class CuentaAhorroFija extends Cuenta{
	public CuentaAhorroFija() {
		super();
		this.interes = 2.6;
	}
	
	public CuentaAhorroFija(String numero, String titular, double saldo, double interes) {
		super(numero, titular, saldo, interes);
	}
}
