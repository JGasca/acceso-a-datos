package clases;

public class CuentaPlanPensiones extends Cuenta{
	private double cotizacion;

	public CuentaPlanPensiones() {
		super();
		this.interes= 3.22;
		this.cotizacion = 6.5;
	}
	
	public CuentaPlanPensiones(String numero, String titular, double cotizacion, double interes) {
		super(numero, titular, interes, cotizacion);
		this.interes = interes;
		this.cotizacion = cotizacion;
	}
	
	public double getCotizacion() {
		return cotizacion;
	}

	public void setCotizacion(double cotizacion) {
		this.cotizacion = cotizacion;
	}

	@Override
	public String toString() {
		return "CuentaPlanPensiones [cotizacion=" + cotizacion + ", saldo=" + saldo + ", interes=" + interes
				+ ", getCotizacion()=" + getCotizacion() + ", getNumero()=" + getNumero() + ", getTitular()="
				+ getTitular() + ", getSaldo()=" + getSaldo() + ", getInteres()=" + getInteres() + ", toString()="
				+ super.toString() + ", getClass()=" + getClass() + "]";
	}
	
	
}
