package programa;

import clases.Cuenta;
import clases.CuentaAhorroFija;
import clases.CuentaCorriente;
import clases.CuentaPlanPensiones;

public class PrincipalCunetas {

	public static void main(String[] args) {
		System.out.println("----- Creamos una cuenta ----");
		Cuenta c1 = new Cuenta();
		System.out.println("----- Muestro la cuenta -----");
		System.out.println(c1);
		
		System.out.println("----- Creamos una cuenta con datos -----");
		Cuenta c2 = new Cuenta("11111111", "Javier", 5000.2, 0.1);
		System.out.println(c2);
		
		System.out.println("----- Creamos otra cuenta corriente c3 -----");
		CuentaCorriente c3 = new CuentaCorriente("33333333", "Pedro", 22, 3);
		System.out.println("Muestro los datos");
		System.out.println(c3);
		
		System.out.println("----- Creamos otra cuenta Ahorro Fija c4 -----");
		CuentaAhorroFija c4 = new CuentaAhorroFija("44444444", "Juanjo", 2500, 2);
		System.out.println("Muestro los datos");
		System.out.println(c4);
		
		System.out.println("----- Creamos otra cuenta Plan pensiones c5 -----");
		CuentaPlanPensiones c5 = new CuentaPlanPensiones("5555", "Juanjo", 2500, 2);
		System.out.println("Muestro los datos");
		System.out.println(c5);

	}

}
