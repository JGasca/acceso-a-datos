package com.javiergc.Practica04.main;

import com.javiergc.Practica04.gui.Controlador;
import com.javiergc.Practica04.gui.Modelo;
import com.javiergc.Practica04.gui.Vista;

/**
 * Clase Principal
 */
public class Principal {
    /**
     * Metodo main que inicializa el controlador, modelo y la vista
     * @param args
     */
    public static void main(String[] args) {
        new Controlador(new Modelo(), new Vista());
    }
}
