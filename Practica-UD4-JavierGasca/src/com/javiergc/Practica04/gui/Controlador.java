package com.javiergc.Practica04.gui;

import com.javiergc.Practica04.base.Encargado;
import com.javiergc.Practica04.base.Recambio;
import com.javiergc.Practica04.base.Tienda;
import com.javiergc.Practica04.util.Util;

import javax.jws.WebParam;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.List;

/**
 * Clase Controlador
 */
public class Controlador implements ActionListener, KeyListener, ListSelectionListener {
    private Modelo modelo;
    private Vista vista;

    /**
     * Constructor de la clase Controlador
     * @param modelo Modelo del programa
     * @param vista Vista del programa
     */
    public Controlador(Modelo modelo, Vista vista) {
        this.vista = vista;
        this.modelo = modelo;

        conectar();
        inicializar();
        rellenarCombo();

        addActionListeners(this);
        addKeyListeners(this);
        addListSelectionListeners(this);
    }

    /**
     * Metodo que lista los recambios, las tiendas y los encargados en sus dtms
     */
    private void inicializar(){
        listarRecambios(modelo.getRecambios());
        listarTiendas(modelo.getTiendas());
        listarEncargados(modelo.getEncargados());
    }

    /**
     * Metodo que añade los actionListener a los botones
     * @param listener ActionListener
     */
    private void addActionListeners(ActionListener listener){
        vista.btnNuevoRecambio.addActionListener(listener);
        vista.btnNuevaTienda.addActionListener(listener);
        vista.btnNuevoEncargado.addActionListener(listener);
        vista.btnEliminarEncargado.addActionListener(listener);
        vista.btnEliminarRecambio.addActionListener(listener);
        vista.btnEliminarTienda.addActionListener(listener);
        vista.btnModificarEncargado.addActionListener(listener);
        vista.btnModificarRecambio.addActionListener(listener);
        vista.btnModificarTienda.addActionListener(listener);
        vista.btnBuscarRecambiosTienda.addActionListener(listener);

    }

    /**
     * Metodo que añade los SelectionListeners a las list
     * @param listener ListSelectionListener
     */
    private void addListSelectionListeners(ListSelectionListener listener){
        vista.listTienda.addListSelectionListener(listener);
        vista.listRecambio.addListSelectionListener(listener);
        vista.listEncargado.addListSelectionListener(listener);
    }

    /**
     * Metodo que añade los KeyListener a el campo de texto de buscar encargado
     * @param listener KeyListener
     */
    private void addKeyListeners(KeyListener listener){
        vista.txtBuscarEncargado.addKeyListener(listener);
    }

    /**
     * Metodo que se ejecuta cada vez que presionamos un boton y segun el boton presionado ejecuta distinto codigo
     * @param e ActionEvent
     */
    @Override
    public void actionPerformed(ActionEvent e) {
        Recambio recambio;
        Tienda tienda;
        Encargado encargado;

        switch (e.getActionCommand()) {
            case "NuevoRecambio":
                recambio = new Recambio();
                if (modificarRecambioFromCampos(recambio)){
                    modelo.guardarObjeto(recambio);
                    limpiarCamposRecambio();
                    listarRecambios(modelo.getRecambios());
                }
                break;
            case "EliminarRecambio":
                if (vista.listRecambio.getSelectedValue() != null){
                    recambio = (Recambio) vista.listRecambio.getSelectedValue();
                    modelo.borrarRecambio(recambio);
                    limpiarCamposRecambio();
                    listarRecambios(modelo.getRecambios());
                    vista.btnNuevoRecambio.setEnabled(true);
                }else {
                    Util.showWarningAlert("No has selecinado una recambio");
                }

                break;
            case "ModificarRecambio":
                if (vista.listRecambio.getSelectedValue() != null){
                    recambio = (Recambio) vista.listRecambio.getSelectedValue();
                    if (modificarRecambioFromCampos(recambio)){
                        modelo.modificarRecambio(recambio);
                        limpiarCamposRecambio();
                        listarRecambios(modelo.getRecambios());
                        vista.btnNuevoRecambio.setEnabled(true);
                    }
                }else {
                    Util.showWarningAlert("No has selecinado una recambio");
                }

                break;
            case "NuevaTienda":
                tienda = new Tienda();
                if (modificarTiendaFromCampos(tienda)){
                    modelo.guardarObjeto(tienda);
                    limpiarCamposTienda();
                    listarTiendas(modelo.getTiendas());
                }
                break;
            case "EliminarTienda":
                if (vista.listTienda.getSelectedValue() != null){
                    tienda = (Tienda) vista.listTienda.getSelectedValue();
                    modelo.borrarTienda(tienda);
                    limpiarCamposTienda();
                    listarTiendas(modelo.getTiendas());
                    vista.btnNuevaTienda.setEnabled(true);
                }else {
                    Util.showWarningAlert("No has selecinado una tienda");
                }

                break;
            case "ModificarTienda":
                if (vista.listTienda.getSelectedValue() != null){
                    tienda = (Tienda) vista.listTienda.getSelectedValue();
                    modificarTiendaFromCampos(tienda);
                    if (modificarTiendaFromCampos(tienda)){
                        modelo.modificarTienda(tienda);
                        limpiarCamposTienda();
                        listarTiendas(modelo.getTiendas());
                        vista.btnNuevaTienda.setEnabled(true);
                    }
                }else {
                    Util.showWarningAlert("No has selecinado una tienda");
                }

                break;
            case "NuevoEncargado":
                encargado = new Encargado();
                if (modificarEncargadoFromCampos(encargado)){
                    modelo.guardarObjeto(encargado);
                    limpiarCamposEncargado();
                    listarEncargados(modelo.getEncargados());
                    setBotonesActivados(true);
                }
                break;
            case "EliminarEncargado":
                if (vista.listEncargado.getSelectedValue() != null){
                    encargado = (Encargado) vista.listEncargado.getSelectedValue();
                    modelo.borrarEncargado(encargado);
                    limpiarCamposEncargado();
                    listarEncargados(modelo.getEncargados());
                    vista.btnNuevoEncargado.setEnabled(true);
                }else {
                    Util.showWarningAlert("No has selecinado un encargado");
                }
                break;
            case "ModificarEncargado":
                if (vista.listEncargado.getSelectedValue() != null){
                    encargado = (Encargado) vista.listEncargado.getSelectedValue();
                    if (modificarEncargadoFromCampos(encargado)){
                        modelo.modificarEncargado(encargado);
                        limpiarCamposEncargado();
                        listarEncargados(modelo.getEncargados());
                        vista.btnNuevoEncargado.setEnabled(true);
                    }
                }else {
                    Util.showWarningAlert("No has selecinado un encargado");
                }

                break;
            case "BuscarRecambiosTienda":
                if (vista.listTienda.getSelectedValue() != null){
                    tienda = (Tienda) vista.listTienda.getSelectedValue();
                    modificarTiendaFromCampos(tienda);
                    listarRecambiosTienda(modelo.getRecambiosTienda(tienda.getId()));
                }else {
                    Util.showErrorAlert("No has seleccionado una tienda");
                }
                break;

        }
        rellenarCombo();
    }

    /**
     * Metodo que conecta con la base de datos
     */
    private void conectar(){
        if (modelo.getCliente() == null) {
            modelo.conectar();
            vista.frame.setTitle("Recambios informatica - <CONECTADO>");
            setBotonesActivados(true);
        }
    }

    /**
     * Metodo que activa o desactiva todos los botones
     * @param activados true o false
     */
    private void setBotonesActivados(boolean activados) {
        vista.btnNuevoRecambio.setEnabled(activados);
        vista.btnNuevaTienda.setEnabled(activados);
        vista.btnNuevoEncargado.setEnabled(activados);
        vista.btnEliminarEncargado.setEnabled(activados);
        vista.btnEliminarRecambio.setEnabled(activados);
        vista.btnEliminarTienda.setEnabled(activados);
        vista.btnModificarEncargado.setEnabled(activados);
        vista.btnModificarRecambio.setEnabled(activados);
        vista.btnModificarTienda.setEnabled(activados);
    }

    /**
     * Metodo que modifica un objeto de la clase Recambio con los campos de texto de la ventana
     * @param recambio Objeto de la clase recambio
     * @return True si esta todo correcto o false si algun campo esta vacio
     */
    private boolean modificarRecambioFromCampos(Recambio recambio) {
        if (!vista.txtNombreRecambio.getText().equals("") && !vista.txtCantidadRecambio.getText().equals("") && !vista.txtPrecioRecambio.getText().equals("") && !vista.txtPrecioRecambio.getText().equals("") && !vista.datepicker.getText().equals("")){
            if (isNumeric(vista.txtCantidadRecambio.getText()) && isNumeric(vista.txtPrecioRecambio.getText()) ){
                recambio.setNombre(vista.txtNombreRecambio.getText());
                recambio.setCantidad(Integer.parseInt(vista.txtCantidadRecambio.getText()));
                recambio.setPrecio(Float.parseFloat(vista.txtPrecioRecambio.getText()));
                recambio.setFecha(vista.datepicker.getDate());
                if (vista.comboTiendaRecambio.getSelectedItem() != null){
                    Tienda tienda = (Tienda) vista.comboTiendaRecambio.getSelectedItem();
                    recambio.setId_tienda(tienda.getId());
                }
                return true;
            }else {
                Util.showErrorAlert("La cantidad o el precio no son numericos");
                return false;
            }
        }else {
            Util.showErrorAlert("Error, Algun campo esta vacio");
           return false;
        }
    }

    /**
     * Metodo que limpia todos los campos de Recambios
     */
    private void limpiarCamposRecambio() {
        vista.txtNombreRecambio.setText("");
        vista.txtCantidadRecambio.setText("");
        vista.txtPrecioRecambio.setText("");
        vista.datepicker.clear();
    }

    /**
     * Metodo que modificar un objeto de la clase Tienda
     * @param tienda Objeto de la clase Tieda a modificar
     * @return true si todo esta bien y false si faltaba algun campo
     */
    private boolean modificarTiendaFromCampos(Tienda tienda) {
        if (!vista.txtNombreTienda.getText().equals("") && !vista.txtDireccionTienda.getText().equals("") && !vista.txtTelefonoTienda.getText().equals("")){
            tienda.setNombre(vista.txtNombreTienda.getText());
            tienda.setDireccion(vista.txtDireccionTienda.getText());
            tienda.setTelefono(vista.txtTelefonoTienda.getText());
            return true;
        }else {
            Util.showWarningAlert("Algun campo esta vacio");
            return false;
        }
    }

    /**
     * Metodo que limpia los campos de Tienda
     */
    private void limpiarCamposTienda() {
        vista.txtNombreTienda.setText("");
        vista.txtDireccionTienda.setText("");
        vista.txtTelefonoTienda.setText("");
    }

    /**
     * Metodo que modifica un objeto Encargado con los datos de los campos
     * @param encargado Objeto de la clase Encargado
     * @return true si todo esta bien y false si faltaba algun campo
     */
    private boolean modificarEncargadoFromCampos(Encargado encargado) {
        if (!vista.txtNombreEncargado.getText().equals("") && !vista.txtApellidosEncargado.getText().equals("") && !vista.txtTelefonoEncargado.getText().equals("")){
            encargado.setNombre(vista.txtNombreEncargado.getText());
            encargado.setApellidos(vista.txtApellidosEncargado.getText());
            encargado.setTelefono(vista.txtTelefonoEncargado.getText());
            return true;
        }else {
            Util.showWarningAlert("Algun campo esta vacio");
            return false;
        }
    }

    /**
     * Metodo que limpia los campos de Encargado
     */
    private void limpiarCamposEncargado() {
        vista.txtNombreEncargado.setText("");
        vista.txtApellidosEncargado.setText("");
        vista.txtTelefonoEncargado.setText("");
    }

    /**
     * Metodo que lista los recambios en su lista
     * @param lista Lista de recambios
     */
    private void listarRecambios(List<Recambio> lista){
        vista.dlmRecambio.clear();
        for (Recambio recambio:lista) {
            vista.dlmRecambio.addElement(recambio);
        }
    }

    /**
     * Metodo que lista las tiendas en su list
     * @param lista lista de tiendas
     */
    private void listarTiendas(List<Tienda> lista){
        vista.dlmTienda.clear();
        for (Tienda tienda:lista) {
            vista.dlmTienda.addElement(tienda);
        }
    }

    /**
     * Metodo que lista los encargados en su list
     * @param lista lista de encargados
     */
    private void listarEncargados(List<Encargado> lista){
        vista.dlmEncargado.clear();
        for (Encargado encargado:lista) {
            vista.dlmEncargado.addElement(encargado);
        }
    }

    /**
     * Metodo que lista los recambios de una tienda
     * @param lista lista de recambios
     */
    private void listarRecambiosTienda(List<Recambio> lista){
        vista.dlmRecambioTienda.clear();
        for (Recambio recambio:lista) {
            vista.dlmRecambioTienda.addElement(recambio);
        }
    }


    /**
     * Metodo que captura el la seleccion de los List y la posiciona en los txt
     * @param e ListSelectionEvent
     */
    @Override
    public void valueChanged(ListSelectionEvent e) {
        if (e.getSource() == vista.listRecambio) {
            if (vista.listRecambio.getSelectedValue() != null) {
                vista.btnNuevoRecambio.setEnabled(false);
                Recambio recambio = (Recambio) vista.listRecambio.getSelectedValue();
                vista.txtNombreRecambio.setText(recambio.getNombre());
                vista.txtCantidadRecambio.setText(String.valueOf(recambio.getCantidad()));
                vista.txtPrecioRecambio.setText(String.valueOf(recambio.getPrecio()));
                vista.datepicker.setDate(recambio.getFecha());
            }
        } else if (e.getSource() == vista.listTienda) {
            if (vista.listTienda.getSelectedValue() != null) {
                vista.btnNuevaTienda.setEnabled(false);
                Tienda tienda = (Tienda) vista.listTienda.getSelectedValue();
                vista.txtNombreTienda.setText(tienda.getNombre());
                vista.txtDireccionTienda.setText(tienda.getDireccion());
                vista.txtTelefonoTienda.setText(tienda.getTelefono());
            }
        } else if (e.getSource() == vista.listEncargado) {
            if (vista.listEncargado.getSelectedValue() != null) {
                vista.btnNuevoEncargado.setEnabled(false);
                Encargado encargado = (Encargado) vista.listEncargado.getSelectedValue();
                vista.txtNombreEncargado.setText(encargado.getNombre());
                vista.txtApellidosEncargado.setText(encargado.getApellidos());
                vista.txtTelefonoEncargado.setText(encargado.getTelefono());
            }
        }
    }

    /**
     * Metodo que comprueba si una cadena solo contiene numeros
     * @param cadena String cadena a analizar
     * @return Boolean true o false
     */
    public boolean isNumeric(String cadena) {
        boolean resultado;
        try {
            Integer.parseInt(cadena);
            resultado = true;
        } catch (NumberFormatException excepcion) {
            try {
                Float.parseFloat(cadena);
                resultado = true;
            }catch (NumberFormatException excepcion2){
                resultado = false;
            }
        }
        return resultado;
    }

    /**
     * Metodo que rellena el combobox con las tiendas
     */
    public void rellenarCombo(){
        vista.comboTiendaRecambio.removeAllItems();
        modelo.setComboBoxTienda(modelo.getTiendas(),vista.comboTiendaRecambio);
    }


    @Override
    public void keyTyped(KeyEvent e) {

    }

    @Override
    public void keyPressed(KeyEvent e) {

    }

    /**
     * Metodo que captura cuando se levanta una letra pulsada
     * @param e KeyEvent
     */
    @Override
    public void keyReleased(KeyEvent e) {
        if (e.getSource() == vista.txtBuscarEncargado) {
            listarEncargados(modelo.getEncargados(vista.txtBuscarEncargado.getText()));
        }
    }
}
