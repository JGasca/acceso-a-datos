package com.javiergc.Practica04.gui;

import com.javiergc.Practica04.base.Encargado;
import com.javiergc.Practica04.base.Recambio;
import com.javiergc.Practica04.base.Tienda;
import com.javiergc.Practica04.util.Util;
import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import org.bson.Document;
import org.bson.types.ObjectId;

import javax.swing.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Clase Modelo
 */
public class Modelo {
    private MongoClient cliente;
    private MongoCollection<Document> recambios;
    private MongoCollection<Document> tiendas;
    private MongoCollection<Document> encargados;

    /**
     * Metodo conectar que conecta con la base de datos
     */
    public void conectar() {
        cliente = new MongoClient();
        String DATABASE = "RecambiosInformatica";
        MongoDatabase db = cliente.getDatabase(DATABASE);

        String COLECCION_PRODUCTOS = "Recambio";
        recambios = db.getCollection(COLECCION_PRODUCTOS);
        String COLECCION_EMPLEADOS = "Tienda";
        tiendas = db.getCollection(COLECCION_EMPLEADOS);
        String COLECCION_DEPARTAMENTOS = "Encargado";
        encargados = db.getCollection(COLECCION_DEPARTAMENTOS);
    }

    /**
     * Metodo que desconecta de la base de datos
     */
    public void desconectar() {
        cliente.close();
        cliente = null;
    }

    /**
     * Metodo que guarda en la base de datos cualquier objeto
     * @param obj Objeto de la clase Recambio, Tienda o Encargado
     */
    public void guardarObjeto(Object obj) {
        if (obj instanceof Recambio) {
            recambios.insertOne(objectToDocument(obj));
        } else if (obj instanceof Tienda) {
            tiendas.insertOne(objectToDocument(obj));
        } else if (obj instanceof Encargado) {
            encargados.insertOne(objectToDocument(obj));
        }
    }

    /**
     * Metodo que devulve un arraylist con todos los recambios
     * @return Devuelve un arraylist con todos los recambios
     */
    public ArrayList<Recambio> getRecambios() {
        ArrayList<Recambio> lista = new ArrayList<>();

        for (Document document : recambios.find()) {
            lista.add(documentToRecambio(document));
        }
        return lista;
    }

    /**
     * Metodo que devulve un arraylist con todos las tiendas
     * @return Devuelve un arraylist con todos las tiendas
     */
    public ArrayList<Tienda> getTiendas() {
        ArrayList<Tienda> lista = new ArrayList<>();

        for (Document document : tiendas.find()) {
            lista.add(documentToTienda(document));
        }
        return lista;
    }

    /**
     * Metodo que devulve un arraylist con todos los encargados
     * @return Devuelve un arraylist con todos los encargados
     */
    public ArrayList<Encargado> getEncargados() {
        ArrayList<Encargado> lista = new ArrayList<>();

        for (Document document : encargados.find()) {
            lista.add(documentToEncargado(document));
        }
        return lista;
    }

    /**
     * Metodo que devuelve el MongoClient
     * @return MongoClient
     */
    public MongoClient getCliente() {
        return cliente;
    }

    /**
     * Metodo que borrar el recambio
     * @param recambio recambio a borrar
     */
    public void borrarRecambio(Recambio recambio) {
        recambios.deleteOne(objectToDocument(recambio));
    }

    /**
     * Metoo que borra una tienda
     * @param tienda tienda a borrar
     */
    public void borrarTienda(Tienda tienda) {
        tiendas.deleteOne(objectToDocument(tienda));
    }

    /**
     * Metodo que borra un encargado
     * @param encargado encargado a borrar
     */
    public void borrarEncargado(Encargado encargado) {
        encargados.deleteOne(objectToDocument(encargado));
    }

    /**
     * Metodo que modifica un recambio
     * @param recambio recambio a modificar
     */
    public void modificarRecambio(Recambio recambio) {
        recambios.replaceOne(new Document("_id",recambio.getId()), objectToDocument(recambio));
    }

    /**
     * Metodo que modifica una tienda
     * @param tienda tienda a modificar
     */
    public void modificarTienda(Tienda tienda) {
        tiendas.replaceOne(new Document("_id",tienda.getId()), objectToDocument(tienda));
    }

    /**
     * Metodo que modifica un encargado
     * @param encargado encargado a modificar
     */
    public void modificarEncargado(Encargado encargado) {
        encargados.replaceOne(new Document("_id",encargado.getId()), objectToDocument(encargado));
    }

    /**
     * Metodo que pasa un documento a un objeto de la clase Recambio
     * @param dc Documento que transformar
     * @return Objeto de la clase Recambio
     */
    public Recambio documentToRecambio(Document dc) {
        Recambio recambio = new Recambio();

        recambio.setId(dc.getObjectId("_id"));
        recambio.setNombre(dc.getString("nombre"));
        recambio.setCantidad(dc.getInteger("cantidad"));
        recambio.setPrecio((Float.parseFloat(String.valueOf(dc.getDouble("precio")))));
        recambio.setFecha(Util.parsearFecha(dc.getString("fecha")));
        recambio.setId_tienda(dc.getObjectId("id_tienda"));
        return recambio;
    }

    /**
     * Metodo que pasa un documento a un objeto de la clase Tienda
     * @param dc Documento que transformar
     * @return Objeto de la clase Tienda
     */
    public Tienda documentToTienda(Document dc) {
        Tienda tienda = new Tienda();

        tienda.setId(dc.getObjectId("_id"));
        tienda.setNombre(dc.getString("nombre"));
        tienda.setDireccion(dc.getString("direccion"));
        tienda.setTelefono(dc.getString("telefono"));
        return tienda;
    }

    /**
     * Metodo que pasa un documento a un objeto de la clase Encargado
     * @param dc Documento que transformar
     * @return Objeto de la clase Encargado
     */
    public Encargado documentToEncargado(Document dc) {
        Encargado encargado = new Encargado();

        encargado.setId(dc.getObjectId("_id"));
        encargado.setNombre(dc.getString("nombre"));
        encargado.setApellidos(dc.getString("apellidos"));
        encargado.setTelefono(dc.getString("telefono"));
        return encargado;
    }

    /**
     * Metodo que pasa un objeto de las clase Recambio, Tienda o Encargado a un Document
     * @param obj Objeto de la clase Recambio, Tienda o Encargado
     * @return Devuelve un documento
     */
    public Document objectToDocument(Object obj) {
        Document dc = new Document();

        if (obj instanceof Recambio) {
            Recambio recambio = (Recambio) obj;

            dc.append("nombre", recambio.getNombre());
            dc.append("cantidad", recambio.getCantidad());
            dc.append("precio", recambio.getPrecio());
            dc.append("precio", recambio.getPrecio());
            dc.append("fecha", Util.formatearFecha(recambio.getFecha()));
            dc.append("id_tienda",recambio.getId_tienda());
        } else if (obj instanceof Tienda) {
            Tienda empleado = (Tienda) obj;

            dc.append("nombre", empleado.getNombre());
            dc.append("direccion", empleado.getDireccion());
            dc.append("telefono", empleado.getTelefono().toString());

        } else if (obj instanceof Encargado) {
            Encargado encargado = (Encargado) obj;

            dc.append("nombre", encargado.getNombre());
            dc.append("apellidos", encargado.getApellidos());
            dc.append("telefono", encargado.getTelefono());
        } else {
            return null;
        }
        return dc;
    }

    /**
     * Metodo que rellena el ComboBox de tienda
     * @param lista lista de tiendas
     * @param combo ComboBox al que las queremos aññadir
     */
    public void setComboBoxTienda(ArrayList<Tienda> lista, JComboBox combo){
        combo.addItem("");
        for (Tienda tienda: lista) {
            combo.addItem(tienda);
        }
        combo.setSelectedIndex(-1);
    }

    /**
     * Metodo que devuelve todos los recambios de una tienda
     * @param id_tienda ObjectId de la tienda
     * @return ArrayList de Recambios
     */
    public ArrayList<Recambio> getRecambiosTienda(ObjectId id_tienda) {
        ArrayList<Recambio> lista = new ArrayList<>();
        Document query = new Document();

        query.append("id_tienda",id_tienda);

        for (Document document : recambios.find(query)) {
            lista.add(documentToRecambio(document));
        }
        return lista;
    }

    /**
     * Metodo que lista los encargados por nombre o apellidos
     * @param comparador cadena que queremos buscar
     * @return ArrayList de encargados
     */
    public ArrayList<Encargado> getEncargados(String comparador) {
        ArrayList<Encargado> lista = new ArrayList<>();
        Document query = new Document();
        List<Document> listaCriterios = new ArrayList<>();

        listaCriterios.add(new Document("nombre", new Document("$regex", "/*" + comparador + "/*")));
        listaCriterios.add(new Document("apellidos", new Document("$regex", "/*" + comparador + "/*")));
        query.append("$or", listaCriterios);

        for (Document document : encargados.find(query)) {
            lista.add(documentToEncargado(document));
        }
        return lista;
    }
}
