package com.javiergc.Practica04.gui;

import com.github.lgooddatepicker.components.DatePicker;
import com.javiergc.Practica04.base.Encargado;
import com.javiergc.Practica04.base.Recambio;
import com.javiergc.Practica04.base.Tienda;

import javax.swing.*;
import java.awt.*;

/**
 * Clase Vista
 */
public class Vista extends JFrame {
    private JPanel panel1;
    JFrame frame;
    private JTabbedPane tabbedPane1;
    JButton btnNuevoRecambio;
    JButton btnEliminarRecambio;
    JButton btnModificarRecambio;
    JTextField txtNombreRecambio;
    JTextField txtPrecioRecambio;
    JTextField txtCantidadRecambio;
    JList listRecambio;
    JButton btnNuevaTienda;
    JButton btnEliminarTienda;
    JButton btnModificarTienda;
    JTextField txtNombreTienda;
    JTextField txtDireccionTienda;
    JTextField txtTelefonoTienda;
    JList listTienda;
    JButton btnNuevoEncargado;
    JButton btnEliminarEncargado;
    JButton btnModificarEncargado;
    JTextField txtNombreEncargado;
    JTextField txtApellidosEncargado;
    JTextField txtTelefonoEncargado;
    JList listEncargado;
    DatePicker datepicker;
    JComboBox comboTiendaRecambio;
    JButton btnBuscarRecambiosTienda;
    JList listRecambiosTienda;
    JList listBuscarEncargados;
    JTextField txtBuscarEncargado;

    DefaultListModel<Recambio> dlmRecambio;
    DefaultListModel<Recambio> dlmRecambioTienda;
    DefaultListModel<Tienda> dlmTienda;
    DefaultListModel<Encargado> dlmEncargado;
    DefaultListModel<Encargado> dlmEncargadoBuscar;

    /**
     * Constructor de la clase Vista
     */
    public Vista() {
        frame = new JFrame();
        frame.setTitle("Recambios informatica - <SIN CONEXION>");
        frame.setContentPane(panel1);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
       // setPreferredSize(new Dimension(800, 410));
        frame.setResizable(false);
        frame.pack();
        frame.setSize(new Dimension(frame.getWidth()+100,frame.getHeight()));
        frame.setVisible(true);

        inicializarModelos();

        frame.setLocationRelativeTo(null);
    }

    /**
     * Metodo que inicializa los dlm
     */
    public void inicializarModelos(){
        dlmEncargado = new DefaultListModel<>();
        listEncargado.setModel(dlmEncargado);
        dlmRecambio = new DefaultListModel<>();
        listRecambio.setModel(dlmRecambio);
        dlmRecambioTienda = new DefaultListModel<>();
        listRecambiosTienda.setModel(dlmRecambioTienda);
        dlmTienda = new DefaultListModel<>();
        listTienda.setModel(dlmTienda);

    }
}
