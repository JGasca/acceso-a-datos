package com.javiergc.Practica04.util;

import javax.swing.*;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

/**
 * Clase Util
 */
public class Util {
    /**
     * Metodo que formatea la fecha
     * @param fechaMatriculacion Fecha a formatear
     * @return Fecha formateada
     */
    public static String formatearFecha(LocalDate fechaMatriculacion) {
        DateTimeFormatter formateador = DateTimeFormatter.ofPattern("dd-MM-yyyy");
        return formateador.format(fechaMatriculacion);
    }

    /**
     * Metodo que parsea la fecha
     * @param fecha Fecha a parsear
     * @return fecha parseada
     */
    public static LocalDate parsearFecha(String fecha){
        DateTimeFormatter formateador = DateTimeFormatter.ofPattern("dd-MM-yyyy");
        return LocalDate.parse(fecha, formateador);
    }

    /**
     * Metodo que lanza un mensaje de error introducido por parametro
     * @param message Mensaje que queremos que se muestre
     */
    public static void showErrorAlert(String message) {
        JOptionPane.showMessageDialog(null, message, "Error", JOptionPane.ERROR_MESSAGE);
    }

    /**
     * Metodo que lanza un mensaje de alerta introducido por parametro
     * @param mensaje Mensaje que queremos que se muestre
     */
    public static void showWarningAlert(String mensaje){
        JOptionPane.showMessageDialog(null, mensaje, "Error", JOptionPane.WARNING_MESSAGE);
    }
}
