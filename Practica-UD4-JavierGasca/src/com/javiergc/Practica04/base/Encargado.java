package com.javiergc.Practica04.base;

import org.bson.types.ObjectId;

/**
 * Clase Encargado
 */
public class Encargado {
    private ObjectId id;
    private String nombre;
    private String apellidos;
    private String telefono;

    /**
     * Constructor de la clase Encargado
     * @param nombre Nombre del encargado
     * @param apellidos Apellidos del encargado
     * @param telefono Telefono del encargado
     */
    public Encargado(String nombre, String apellidos, String telefono) {
        this.nombre = nombre;
        this.apellidos = apellidos;
        this.telefono = telefono;
    }

    /**
     * Constructor vacio del encargado
     */
    public Encargado(){}

    /**
     * Metodo que devuelve el nombre del encargado
     * @return Nombre del encargado
     */
    public String getNombre() {
        return nombre;
    }

    /**
     * Metodo que establece el nombre del encargado
     * @param nombre Nombre del encargado
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    /**
     * Metodo que devuelve los apellidos del encargado
     * @return Apellidos del encargado
     */
    public String getApellidos() {
        return apellidos;
    }

    /**
     * Metodo que establece los apellidos del encargado
     * @param apellidos Apellidos del encargado
     */
    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    /**
     * Metodo que devuelve el telefono del encargado
     * @return Telefono del encargado
     */
    public String getTelefono() {
        return telefono;
    }

    /**
     * Metodo que establece el telefono del encargado
     * @param telefono Telefono del encargado
     */
    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    /**
     * Metodo que devuelve el id del Encargado
     * @return Id del encargado
     */
    public ObjectId getId() {
        return id;
    }

    /**
     * Metodo que establece el id del encargado
     * @param id Id del encargado
     */
    public void setId(ObjectId id) {
        this.id = id;
    }

    /**
     * Metodo que pasa a String el Encargado
     * @return String con los datos del encargado
     */
    @Override
    public String toString() {
        return nombre + " | " + apellidos + " | " + telefono;
    }
}
