package com.javiergc.Practica04.base;

import org.bson.types.ObjectId;

import java.time.LocalDate;

/**
 * Clase Recambio
 */
public class Recambio {
    private ObjectId id;
    private String nombre;
    private int cantidad;
    private float precio;
    private LocalDate fecha;
    private ObjectId id_tienda;

    /**
     * Constructor de la clase Recambio
     * @param nombre Nombre del recambio
     * @param cantidad Cantidad del recambio
     * @param precio Precio del recambio
     * @param fecha Fecha de expiración
     * @param id_tienda id de la tienda relacionada
     */
    public Recambio(String nombre, int cantidad, float precio, LocalDate fecha, ObjectId id_tienda){
       this.nombre = nombre;
       this.cantidad = cantidad;
       this.precio = precio;
       this.fecha = fecha;
       this.id_tienda = id_tienda;
    }

    /**
     * Constructor vacio de la clase Recambio
     */
    public Recambio(){}

    /**
     * Metodo que devuelve el id del recambio
     * @return Id del recambio
     */
    public ObjectId getId() {
        return id;
    }

    /**
     * Metodo que establece el id del recambio
     * @param id id del recambio
     */
    public void setId(ObjectId id) {
        this.id = id;
    }

    /**
     * Metodo que devuelve el nombre del recambio
     * @return Nombre del recambio
     */
    public String getNombre() {
        return nombre;
    }

    /**
     * Metodo que establece el nombre del recambio
     * @param nombre Nombre del recambio
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    /**
     * Metodo que devuelve la cantidad del recambio
     * @return Cantidad del recambio
     */
    public int getCantidad() {
        return cantidad;
    }

    /**
     * Metodo que establece la cantidad del recambio que tiene
     * @param cantidad Cantidad del recambio
     */
    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }

    /**
     * Metodo que devuelve el precio del recambio
     * @return Precio de recambio
     */
    public float getPrecio() {
        return precio;
    }

    /**
     * Metodo que establece el precio del recambio
     * @param precio Precio del recambio
     */
    public void setPrecio(float precio) {
        this.precio = precio;
    }

    /**
     * Metodo que devuelve la fecha de expiración
     * @return Fecha de expiración
     */
    public LocalDate getFecha() {
        return fecha;
    }

    /**
     * Metodo que establece la fecha de expiración del recambio
     * @param fecha Fecha de expiración del recambio
     */
    public void setFecha(LocalDate fecha) {
        this.fecha = fecha;
    }

    /**
     * Metodo que devuelve el id de la tienda asociada al recambio
     * @return Devuelve el id de la tienda asociada al recambio
     */
    public ObjectId getId_tienda() {
        return id_tienda;
    }

    /**
     * Metodo que establece el id_tienda
     * @param id_tienda id de la tienda
     */
    public void setId_tienda(ObjectId id_tienda) {
        this.id_tienda = id_tienda;
    }

    /**
     * Metodo que devuelve el recambio pasado a String
     * @return String con los atributos
     */
    @Override
    public String toString() {
        return nombre + " | " + cantidad + " | " + precio + "€ | " + fecha ;
    }
}
