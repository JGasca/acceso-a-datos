package com.javiergc.Practica04.base;

import org.bson.types.ObjectId;

/**
 * Clase Tienda
 */
public class Tienda {
    private ObjectId id;
    private String nombre;
    private String direccion;
    private String telefono;

    /**
     * Constructor de la clase tienda
     * @param nombre Nombre de la tienda
     * @param direccion Direccion de la tienda
     * @param telefono Telefono de la tienda
     */
    public Tienda(String nombre, String direccion, String telefono) {
        this.nombre = nombre;
        this.direccion = direccion;
        this.telefono = telefono;
    }

    /**
     * Constructor vacio de la clase Tienda
     */
    public Tienda(){}

    /**
     * Metodo que devuelve el id de la tienda
     * @return Devuelve el id dela tienda
     */
    public ObjectId getId() {
        return id;
    }

    /**
     * Metodo que establece el id de la tienda
     * @param id Nuevo id de la tienda
     */
    public void setId(ObjectId id) {
        this.id = id;
    }

    /**
     * Metodo que devuelve el nombre de la tienda
     * @return Nombre de la tienda
     */
    public String getNombre() {
        return nombre;
    }

    /**
     * Metodo que establece el nombre de la tienda
     * @param nombre Nuevo nombre de la tienda
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    /**
     * Metodo que devuelve la dirección de la tienda
     * @return Devuelvela dirección de la tienda
     */
    public String getDireccion() {
        return direccion;
    }

    /**
     * Metodo que establece el nombre de la tienda
     * @param direccion Nuevo nombre de la tienda
     */
    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    /**
     * Metodo que devuelve el telefono de la tienda
     * @return Devuelve el telefono de la tienda
     */
    public String getTelefono() {
        return telefono;
    }

    /**
     * Metodo que establece el telefono de la tienda
     * @param telefono Nuevo telefono de la tienda
     */
    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    /**
     * Metodo que pasa el objeto Tienda a String
     * @return El objeto tienda a String
     */
    @Override
    public String toString() {
        return nombre + " | " + direccion + " |" + telefono ;
    }
}
