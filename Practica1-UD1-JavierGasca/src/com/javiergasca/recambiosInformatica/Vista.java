package com.javiergasca.recambiosInformatica;

import com.javiergasca.recambiosInformatica.clases.Recambios;
import org.w3c.dom.*;

import javax.swing.*;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.*;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.util.LinkedList;

public class Vista {
    private JTextField txtnombre;
    private JFrame frame;
    private JPanel panel1;
    private JTextField txtCantidad;
    private JTextField txtPrecio;
    private JButton añadirRecambioButton;
    private JLabel lblTipoRecambio;
    private JLabel lblCantidad;
    private JLabel lblPeso;
    private JButton mostrarRecambioButton;
    private JComboBox cbRecambios;
    private JTextArea textArea;
    private JButton eliminarRecambioButton;
    private JButton borrarMostradosButton;

    private LinkedList<Recambios> lista;
    private DefaultComboBoxModel<String> dtm;

    public static void main(String[] args) {
        Vista vista = new Vista();
    }

    public Vista(){
        frame = new JFrame("Vista");
        frame.setContentPane(panel1);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setSize(600,400);
        //frame.pack();
        crearMenu();
        frame.setVisible(true);
        frame.setLocationRelativeTo(null);

        lista = new LinkedList<>();
        dtm = new DefaultComboBoxModel<>();

        cbRecambios.setModel(dtm);

        añadirRecambioButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                altaRecambio(txtnombre.getText(), txtCantidad.getText(), txtPrecio.getText());
                refrescarComboBox();
            }
        });
        mostrarRecambioButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (!lista.isEmpty()){
                    Recambios seleccion = buscar((String) dtm.getSelectedItem());
                    textArea.setText(textArea.getText() + "\n" + seleccion.toString());
                }
            }
        });
        borrarMostradosButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                textArea.setText("");
            }
        });
        eliminarRecambioButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (!lista.isEmpty()){
                    eliminarRecambio(buscar((String) dtm.getSelectedItem()));
                    refrescarComboBox();
                }
            }
        });
    }


    private void eliminarRecambio(Recambios recambio){
        lista.remove(recambio);
    }
    private Recambios buscar(String nombre){
        for (Recambios recambio : lista) {
            if (recambio.getNombre().equals(nombre)){
                return recambio;
            }
        }
        return null;
    }

    private void altaRecambio(String nombre, String cantidad, String precio){
        lista.add(new Recambios(nombre,cantidad,precio));
        txtCantidad.setText("");
        txtnombre.setText("");
        txtPrecio.setText("");
        refrescarComboBox();
    }

    private void importarXML(File fichero){
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        try {
            DocumentBuilder builder = factory.newDocumentBuilder();
            Document documento = builder.parse(fichero);

            //Recorro cada uno de los nodos recambio para obtener sus campos
            NodeList recambios = documento.getElementsByTagName("recambio");
            for (int i = 0; i < recambios.getLength(); i++) {
                Node recambio = recambios.item(i);
                Element elemento = (Element) recambio;
                System.out.println(elemento.toString());

                //Obtengo los campos nombre, cantidad y precio
                String nombre = elemento.getElementsByTagName("nombre").item(0).getChildNodes().item(0).getNodeValue();
                String cantidad = elemento.getElementsByTagName("cantidad").item(0).getChildNodes().item(0).getNodeValue();
                String precio = elemento.getElementsByTagName("precio").item(0).getChildNodes().item(0).getNodeValue();

                altaRecambio(nombre,cantidad,precio);
            }
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (org.xml.sax.SAXException e) {
            e.printStackTrace();
        }
    }

    private void exportarrXML(File fichero){
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = null;

        try {
            builder = factory.newDocumentBuilder();
            DOMImplementation dom = builder.getDOMImplementation();

            //Creo documento que representa arbol XML
            Document documento = dom.createDocument(null, "xml", null);

            //Creo el nodo raiz (recambios) y lo añado al documento
            Element raiz = documento.createElement("recambios");
            documento.getDocumentElement().appendChild(raiz);

            Element nodoRecambios;
            Element nodoDatos;
            Text dato;

            //Por cada recambio de la lista, creo un nodo recambio
            for (Recambios recambio : lista) {
                System.out.println(recambio.toString());
                //Creo un nodo recambio y lo añado al nodo raiz (recambios)
                nodoRecambios = documento.createElement("recambio");
                raiz.appendChild(nodoRecambios);

                //A cada nodo recambio le añado los nodos nombre, cantidad, precio
                nodoDatos = documento.createElement("nombre");
                nodoRecambios.appendChild(nodoDatos);
                dato = documento.createTextNode(recambio.getNombre());
                nodoDatos.appendChild(dato);

                nodoDatos = documento.createElement("cantidad");
                nodoRecambios.appendChild(nodoDatos);
                dato = documento.createTextNode(recambio.getCantidad());
                nodoDatos.appendChild(dato);

                nodoDatos = documento.createElement("precio");
                nodoRecambios.appendChild(nodoDatos);
                dato = documento.createTextNode(recambio.getPrecio());
                nodoDatos.appendChild(dato);
            }

            //Transformo el documento anterior en un ficho de texto plano
            Source src = new DOMSource(documento);
            Result result = new StreamResult(fichero);

            Transformer transformer = null;
            transformer = TransformerFactory.newInstance().newTransformer();
            transformer.transform(src, result);

        } catch (TransformerConfigurationException e) {
            e.printStackTrace();
        } catch (TransformerException e) {
            e.printStackTrace();
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        }
    }

    private void refrescarComboBox(){
        dtm.removeAllElements();
        for (Recambios recambio :
                lista) {
            dtm.addElement(recambio.getNombre());
        }
    }

    private void crearMenu(){
        JMenuBar barra = new JMenuBar();
        JMenu menu =  new JMenu("Archivo");
        JMenuItem itemExportarXML = new JMenuItem("Exportar XML");
        JMenuItem itemImportarXML = new JMenuItem("Importar XML");

        menu.add(itemExportarXML);
        menu.add(itemImportarXML);

        itemExportarXML.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                JFileChooser selectorArchivo = new JFileChooser();
                int opcionSeleccionada = selectorArchivo.showSaveDialog(null);
                if (opcionSeleccionada == JFileChooser.APPROVE_OPTION){
                    File fichero = selectorArchivo.getSelectedFile();
                    exportarrXML(fichero);
                }
            }
        });

        itemImportarXML.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                JFileChooser selectorArchivo = new JFileChooser();
                int opcion = selectorArchivo.showOpenDialog(null);
                if (opcion == JFileChooser.APPROVE_OPTION){
                    File fichero = selectorArchivo.getSelectedFile();
                    importarXML(fichero);
                    refrescarComboBox();
                }
            }
        });


        barra.add(menu);
        frame.setJMenuBar(barra);
    }

}
