package com.javiergasca.recambiosInformatica.clases;

public class Recambios {
    String nombre;
    String cantidad;
    String precio;

    public Recambios(String nombre, String cantidad, String peso){
        this.nombre = nombre;
        this.cantidad = cantidad;
        this.precio = peso;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getCantidad() {
        return cantidad;
    }

    public void setCantidad(String cantidad) {
        this.cantidad = cantidad;
    }

    public String getPrecio() {
        return precio;
    }

    public void setPrecio(String precio) {
        this.precio = precio;
    }

    @Override
    public String toString() {
        return "Recambios: " +
                "Nombre= " + nombre  +
                ", Cantidad= " + cantidad +
                ", peso= " + precio;
    }
}
