package com.javiergasca.practica2UD2.main;

import com.javiergasca.practica2UD2.gui.Controlador;
import com.javiergasca.practica2UD2.gui.Modelo;
import com.javiergasca.practica2UD2.gui.Vista;

/**
 * Clase Principal que se encarga de inicializar el resto del programa
 */
public class Principal {
    /**
     * Metodo que ejecuta las clases
     * @param args
     */
    public static void main(String[] args) {
        Vista vista = new Vista();
        Modelo modelo = new Modelo();
        Controlador controlador = new Controlador(vista,modelo);
    }
}
