package com.javiergasca.practica2UD2.gui;

import Encriptacion.Encriptar;
import com.javiergasca.practica2UD2.Util.Util;

import javax.swing.*;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.DefaultTableModel;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

/**
 * Clase Controlador
 */
public class Controlador implements ActionListener, TableModelListener {
    private Vista vista;
    private Modelo modelo;

    private enum tipoEstado {conectado, desconectado};
    private tipoEstado estado;

    /**
     * Constructor de la clase Controlador
     * @param vista Vista de nuestro programa
     * @param modelo Modelo de nuestro programa
     */
    public Controlador(Vista vista, Modelo modelo){
        this.modelo = modelo;
        File archivo = new File("config.properties");
        if (!archivo.exists()){
            modelo.setPropValuesSinEncrip("un6GhAmO3VCleL4w17yHJA==","VrBD1zB/fmBckQYAO2rlEw==","s6a2jguPXMTIQiaqBcTGvg==","1ARVn2Auq2/WAqx2gNrL+q3RNjAzXpUfCXrzkA6d4Xa22yhRLy4AC50E+6UTPoscbo31nbOoq51gvkuXzJ6B2w==");
        }
        this.vista = vista;
        setOptions();
        estado = tipoEstado.desconectado;
        iniciarTabla();
        addActionListener(this);
        addActionListenerTable();
    }

    /**
     * Metodo que establece los listener para todos los botones
     * @param listener ActionListener listener
     */
    private void addActionListener(ActionListener listener){
        vista.btnBuscar.addActionListener(listener);
        vista.btnEliminar.addActionListener(listener);
        vista.btnNuevo.addActionListener(listener);
        vista.btnModificar.addActionListener(listener);
        vista.itemConectar.addActionListener(listener);
        vista.itemSalir.addActionListener(listener);
        vista.itemOpcionesBBDD.addActionListener(listener);
        vista.btnValidate.addActionListener(listener);
        vista.optionDialog.btnGuardar.addActionListener(listener);
        vista.itemCrearTablaRecambios.addActionListener(listener);

        vista.btnNuevoCliente.addActionListener(listener);
        vista.btnEliminarCliente.addActionListener(listener);
        vista.btnModificarCliente.addActionListener(listener);
        vista.btnNuevoTienda.addActionListener(listener);
        vista.btnEliminarTienda.addActionListener(listener);
        vista.btnModificarTienda.addActionListener(listener);
        vista.btnAsignar.addActionListener(listener);

    }

    /**
     * Metodo que esta a la escucha de cuando se selecciona una fila del JTable
     */
    private void addActionListenerTable(){
        vista.table.addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent e) {
                int row = vista.table.getSelectedRow();
                int col = vista.table.getSelectedColumn();
                vista.txtNombre.setText((String) vista.table.getValueAt(row,1));
                vista.txtCantidad.setText((String) vista.table.getValueAt(row,2));
                vista.txtPrecio.setText((String) vista.table.getValueAt(row,3));
                vista.dateTimePicker.setDateTimeStrict(formatearFecha(vista.table.getValueAt(row,4).toString()));
                vista.combo_tipo.setSelectedItem(vista.table.getValueAt(row,5));
                vista.btnNuevo.setEnabled(false);
            }
        });

        vista.tableCliente.addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent e) {
                int row = vista.tableCliente.getSelectedRow();
                int col = vista.tableCliente.getSelectedColumn();
                vista.txtNombreCliente.setText((String) vista.tableCliente.getValueAt(row,1));
                vista.txtApellidosCliente.setText((String) vista.tableCliente.getValueAt(row,2));
                vista.txtDireccionCliente.setText((String) vista.tableCliente.getValueAt(row,3));
                vista.txtTelefonoCliente.setText((String) vista.tableCliente.getValueAt(row,4));
                vista.comboCliente.setSelectedItem(vista.tableCliente.getValueAt(row,5));

                vista.btnNuevoCliente.setEnabled(false);
            }
        });

        vista.tablaTienda.addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent e) {
                int row = vista.tablaTienda.getSelectedRow();
                int col = vista.tablaTienda.getSelectedColumn();
                vista.txtNombreTienda.setText((String) vista.tablaTienda.getValueAt(row,1));
                vista.txtDireccionTienda.setText((String) vista.tablaTienda.getValueAt(row,2));
                vista.txtTelefonoTienda.setText((String) vista.tablaTienda.getValueAt(row,3));

                vista.btnNuevoTienda.setEnabled(false);
            }
        });
    }

    /**
     * Metodo que formatea la fecha de String a LocalDateTime
     * @param fecha fecha en formato String
     * @return Devuelve la fecha en formato LocalDateTime
     */
    private LocalDateTime formatearFecha(String fecha){
        fecha =  fecha.substring(0, fecha.length() - 2);
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        LocalDateTime fecha2 = LocalDateTime.parse(fecha,formatter);
        return fecha2;
    }

    /**
     * Metodo que establece las cabeceras de la tabla
     */
    private void iniciarTabla(){
        String[] headersRecambio = {"id", "nombre", "cantidad","precio","fecha expiracion", "tipo"};
        String[] headersCliente = {"id", "nombre", "Apellidos","Dirección","Telefono","Edad"};
        String[] headersTienda = {"id", "nombre", "Dirección","Telefono"};
        String[] headersAsignar = {"Nombre cliente", "Apellidos cliente","Nombre Recambio","Nombre tienda","Dirección tienda", "Fecha"};
        vista.dtm.setColumnIdentifiers(headersRecambio);
        vista.dtmCliente.setColumnIdentifiers(headersCliente);
        vista.dtmTienda.setColumnIdentifiers(headersTienda);
        vista.dtmAsignar.setColumnIdentifiers(headersAsignar);
    }

    /**
     * Metodo que carga los datos recibios en el JTable de recambios
     * @param resultSet Resultado de una consulta de obtención de datos
     * @throws SQLException
     */
    private void cargarFilasRecambio(ResultSet resultSet) throws SQLException {
        vista.btnNuevo.setEnabled(true);
        Object[] fila = new Object[6];
        vista.dtm.setRowCount(0);

        while(resultSet.next()){
            fila[0]=resultSet.getObject(1);
            fila[1]=resultSet.getObject(2);
            fila[2]=resultSet.getObject(3);
            fila[3]=resultSet.getObject(4);
            fila[4]=resultSet.getObject(5);
            fila[5]=resultSet.getObject(6);

            vista.dtm.addRow(fila);
        }
        if (resultSet.last()){
            vista.lblAccion.setVisible(true);
            vista.lblAccion.setText(resultSet.getRow() + " filas cargadas");
        }
    }

    /**
     * Metodo que carga los datos recibidos en la tabla de clientes
     * @param resultSet Resultado de una consulta de obtención de datos
     * @throws SQLException
     */
    private void cargarFilasCliente(ResultSet resultSet) throws SQLException {
        vista.btnNuevoCliente.setEnabled(true);
        Object[] fila = new Object[6];
        vista.dtmCliente.setRowCount(0);

        while(resultSet.next()){
            fila[0]=resultSet.getObject(1);
            fila[1]=resultSet.getObject(2);
            fila[2]=resultSet.getObject(3);
            fila[3]=resultSet.getObject(4);
            fila[4]=resultSet.getObject(5);
            fila[5]=resultSet.getObject(6);

            vista.dtmCliente.addRow(fila);
        }
        if (resultSet.last()){
            vista.lblActionCliente.setVisible(true);
            vista.lblActionCliente.setText(resultSet.getRow() + " filas cargadas");
        }
    }

    /**
     * Metodo que carga los datos recibidos en la tabla tienda
     * @param resultSet Resultado de una consulta de obtención de datos
     *      * @throws SQLException
     * @throws SQLException
     */
    private void cargarFilasTienda(ResultSet resultSet) throws SQLException {
        vista.btnNuevoTienda.setEnabled(true);
        Object[] fila = new Object[4];
        vista.dtmTienda.setRowCount(0);

        while(resultSet.next()){
            fila[0]=resultSet.getObject(1);
            fila[1]=resultSet.getObject(2);
            fila[2]=resultSet.getObject(3);
            fila[3]=resultSet.getObject(4);

            vista.dtmTienda.addRow(fila);
        }
        if (resultSet.last()){
            vista.lblActionTienda.setVisible(true);
            vista.lblActionTienda.setText(resultSet.getRow() + " filas cargadas");
        }
    }

    /**
     * Metodo que carga las filas de la tabla asignar
     * @param resultSet Resultado de una consulta de obtención de datos
     * @throws SQLException
     */
    private void cargarFilasAsignar(ResultSet resultSet) throws SQLException {
        vista.btnAsignar.setEnabled(true);
        Object[] fila = new Object[6];
        vista.dtmAsignar.setRowCount(0);

        while(resultSet.next()){
            fila[0]=resultSet.getObject(1);
            fila[1]=resultSet.getObject(2);
            fila[2]=resultSet.getObject(3);
            fila[3]=resultSet.getObject(4);
            fila[4]=resultSet.getObject(5);
            fila[5]=resultSet.getObject(6);

            vista.dtmAsignar.addRow(fila);
        }
    }

    /**
     * Metodo que ejecuta lo que correnponda segun el boton seleccionado
     * @param e
     */
    @Override
    public void actionPerformed(ActionEvent e) {
        String comando = e.getActionCommand();

        switch (comando){
            case "btnNuevoRecambio":
                try {
                    if (!vista.txtNombre.getText().equals("") && !vista.txtCantidad.getText().equals("") && !vista.txtPrecio.getText().equals("") && vista.dateTimePicker.getDateTimePermissive() != null && vista.combo_tipo.getSelectedItem() != null && isNumeric(vista.txtPrecio.getText()) && isNumeric(vista.txtCantidad.getText()) ){
                        int error = modelo.addRecambio(vista.txtNombre.getText(),vista.txtCantidad.getText(),vista.txtPrecio.getText(),vista.dateTimePicker.getDateTimePermissive(),vista.combo_tipo.getSelectedItem().toString());
                        System.out.println(error);
                        limpiarCamposRecambio();
                        cargarFilasRecambio(modelo.obtenerDatos("recambio"));
                        vista.lblAccion.setText("Recambio añadido");
                    }else {
                        if (!isNumeric(vista.txtPrecio.getText())|| !isNumeric(vista.txtCantidad.getText())){
                            Util.showErrorAlert("Precio o cantidad no son numericos");
                        }else {
                            Util.showErrorAlert("No has rellenado todos los campos");
                        }
                    }
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
                break;

            case "btnNuevoCliente":
                try {
                    if (!vista.txtNombreCliente.getText().equals("") && !vista.txtApellidosCliente.getText().equals("") && !vista.txtDireccionCliente.getText().equals("") && !vista.txtTelefonoCliente.getText().equals("") && vista.comboCliente.getSelectedItem() != null){
                        int error = modelo.addcliente(vista.txtNombreCliente.getText(), vista.txtApellidosCliente.getText(), vista.txtDireccionCliente.getText(), vista.txtTelefonoCliente.getText(), vista.comboCliente.getSelectedItem().toString());
                        limpiarCamposCliente();
                        cargarFilasCliente(modelo.obtenerDatos("cliente"));
                        vista.lblActionTienda.setText("Filas cargadas");
                    }else {
                        Util.showErrorAlert("No has rellenado todos los campos");
                    }
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
                break;

            case "btnNuevaTienda":
                try {
                    if (!vista.txtNombreTienda.getText().equals("")  && !vista.txtDireccionTienda.getText().equals("") && !vista.txtTelefonoTienda.getText().equals("")){
                        System.out.println("Entraaa aquiii");
                        System.out.println(vista.txtNombreTienda.getText());
                        int error = modelo.addTienda(vista.txtNombreTienda.getText(),vista.txtDireccionTienda.getText(),vista.txtTelefonoTienda.getText());
                        limpiarCamposTienda();
                        cargarFilasTienda(modelo.obtenerDatos("tienda"));
                        vista.lblActionTienda.setText("Filas cargadas");
                    }else {
                        Util.showErrorAlert("No has rellenado todos los campos");
                    }
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
                break;

            case "btnEliminarRecambio":
                vista.btnNuevo.setEnabled(true);
                try {
                    if (vista.table.getSelectedRow() != -1) {
                        int filaBorrar = vista.table.getSelectedRow();
                        int idBorrar = (int) vista.dtm.getValueAt(filaBorrar, 0);
                        modelo.eliminarRecambio(idBorrar);
                        vista.dtm.removeRow(filaBorrar);
                        vista.lblAccion.setText("Fila eliminada");
                        limpiarCamposRecambio();
                    }else{
                        Util.showWarningAlert("No has seleccionado ningun recambio");
                    }
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
                break;

            case "btnEliminarCliente":
                vista.btnNuevoCliente.setEnabled(true);
                try {
                    if (vista.tableCliente.getSelectedRow() != -1) {
                        int filaBorrar = vista.tableCliente.getSelectedRow();
                        int idBorrar = (int) vista.dtmCliente.getValueAt(filaBorrar, 0);
                        modelo.eliminarCliente(idBorrar);
                        vista.dtmCliente.removeRow(filaBorrar);
                        vista.lblActionCliente.setText("Fila eliminada");
                        limpiarCamposCliente();
                    }else {
                        Util.showWarningAlert("No has seleccionado ningun Cliente");
                    }
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
                break;

            case "btnEliminarTienda":
                vista.btnNuevoTienda.setEnabled(true);
                try {
                    if (vista.tablaTienda.getSelectedRow() != -1) {
                        int filaBorrar = vista.tablaTienda.getSelectedRow();
                        int idBorrar = (int) vista.dtmTienda.getValueAt(filaBorrar, 0);
                        modelo.eliminarTienda(idBorrar);
                        vista.dtmTienda.removeRow(filaBorrar);
                        vista.lblActionTienda.setText("Fila eliminada");
                        limpiarCamposTienda();
                    }else {
                        Util.showWarningAlert("No has seleccionado ninguna Tienda");
                    }
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
                break;

            case "btnModificarRecambio":
                int row = vista.table.getSelectedRow();
                if (vista.table.getSelectedRow() != -1){
                    int id = (int) vista.table.getValueAt(row,0);
                    try {
                        modelo.modificarRecambio(id,vista.txtNombre.getText(),vista.txtCantidad.getText(),vista.txtPrecio.getText(),vista.dateTimePicker.getDateTimePermissive(),vista.combo_tipo.getSelectedItem().toString());
                        cargarFilasRecambio(modelo.obtenerDatos("recambio"));
                        vista.lblAccion.setText("Recambio modificado");
                    } catch (SQLException ex) {
                        ex.printStackTrace();
                        Util.showErrorAlert("Erro al modificar el recambio");
                    }
                }else {
                    Util.showWarningAlert("Selecciona el recambio a modificar");
                }
                limpiarCamposRecambio();
                break;

            case "btnModificarTienda":
                row = vista.tablaTienda.getSelectedRow();
                if (vista.tablaTienda.getSelectedRow() != -1){
                    int id = (int) vista.tablaTienda.getValueAt(row,0);
                    try {
                        modelo.modificarTienda(id,vista.txtNombreTienda.getText(),vista.txtDireccionTienda.getText(),vista.txtTelefonoTienda.getText());
                        cargarFilasTienda(modelo.obtenerDatos("tienda"));
                        vista.lblActionTienda.setText("Tienda modificada");
                    } catch (SQLException ex) {
                        ex.printStackTrace();
                        Util.showErrorAlert("Erro al modificar la tienda");
                    }
                }else {
                    Util.showWarningAlert("Selecciona la tienda a modificar");
                }
                limpiarCamposTienda();
                break;

            case "btnModificarCliente":
                row = vista.tableCliente.getSelectedRow();
                if (vista.tableCliente.getSelectedRow() != -1){
                    int id = (int) vista.tableCliente.getValueAt(row,0);
                    try {
                        modelo.modificarCliente(id,vista.txtNombreCliente.getText(),vista.txtApellidosCliente.getText(),vista.txtDireccionCliente.getText(),vista.txtTelefonoCliente.getText(),vista.comboCliente.getSelectedItem().toString());
                        cargarFilasCliente(modelo.obtenerDatos("cliente"));
                        vista.lblActionCliente.setText("Cliente modificado");
                    } catch (SQLException ex) {
                        ex.printStackTrace();
                        Util.showErrorAlert("Erro al modificar el cliente");
                    }
                }else {
                    Util.showWarningAlert("Selecciona el cliente a modificar");
                }
                limpiarCamposCliente();
                break;

            case "Buscar":
                String tipo = vista.combo_tipo2.getSelectedItem().toString();
                try {
                    cargarFilasRecambio(modelo.buscarTipo(tipo));
                    vista.lblAccion.setText("Busqueda completada");
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
                break;
            case "Salir":
                System.exit(0);
                break;
            case "Conectar":
                if (estado == tipoEstado.desconectado){
                    try {
                        modelo.conectar();
                        vista.itemConectar.setText("Desconectar");
                        estado = tipoEstado.conectado;
                        vista.lblAccion.setText("Conectado");
                        vista.lblActionCliente.setText("Conectado");
                        vista.lblActionTienda.setText("Conectado");

                        cargarFilasRecambio(modelo.obtenerDatos("recambio"));
                        cargarFilasCliente(modelo.obtenerDatos("cliente"));
                        cargarFilasTienda(modelo.obtenerDatos("tienda"));
                        cargarFilasAsignar(modelo.obtenerClientesTiendasRecambio());

                        rellenarCombosAsignar();

                        activarBotones();
                    } catch (SQLException ex) {
                        JOptionPane.showMessageDialog(null,"Error de conexión", "Error", JOptionPane.ERROR_MESSAGE);
                        ex.printStackTrace();
                    }

                }else {
                    try {

                        modelo.desconectar();
                        vista.itemConectar.setText("Conectar");
                        estado = tipoEstado.desconectado;
                        vista.lblAccion.setText("Desconectando");
                        vista.lblAccion.setVisible(true);
                        limpiarTabla(vista.table);
                        limpiarTabla(vista.tableCliente);
                        limpiarTabla(vista.tablaTienda);
                        limpiarTabla(vista.tablaAsignar);

                        limpiarCamposRecambio();
                        limpiarCamposTienda();
                        limpiarCamposCliente();

                        desactivarBotones();

                        vista.btnNuevo.setEnabled(false);
                        vista.btnModificar.setEnabled(false);
                        vista.btnBuscar.setEnabled(false);
                        vista.btnEliminar.setEnabled(false);

                        vista.comboBoxCliente.removeAllItems();
                        vista.comboBoxTienda.removeAllItems();
                        vista.comboBoxRecambio.removeAllItems();
                    } catch (SQLException ex) {
                        JOptionPane.showMessageDialog(null,"Error al desconectar", "Error", JOptionPane.ERROR_MESSAGE);
                    }
                }
                break;
            case "Opciones BBDD":
                vista.adminPasswordDialog.setVisible(true);
                break;
            case "abrirOpciones":

                Boolean verifi = null;
                try {
                    verifi = Encriptar.verificarhas(modelo.getAdminPassword(),String.valueOf(vista.adminPassword.getPassword()));
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
                if(verifi) {
                    vista.adminPassword.setText("");
                    vista.adminPasswordDialog.dispose();
                    vista.optionDialog.setVisible(true);
                } else {
                    Util.showErrorAlert("La contraseña introducida no es correcta.");
                }
                break;
            case "Guardar":
                String contrasenaAdmin;
                if (vista.optionDialog.txtContrasenaAdmin.getText().isEmpty()){
                    contrasenaAdmin = modelo.getAdminPassword();
                }else {
                    contrasenaAdmin = vista.optionDialog.txtContrasenaAdmin.getText();
                }

                String contrasena;
                if (vista.optionDialog.txtContrasena.getText().isEmpty()){
                    contrasena = "";
                }else {
                    contrasena = vista.optionDialog.txtContrasena.getText();
                }
                modelo.setPropValues(vista.optionDialog.txtIp.getText(), vista.optionDialog.txtUsuario.getText(),
                        contrasena, contrasenaAdmin);
                vista.optionDialog.dispose();
                vista.frame.dispose();
                new Controlador(new Vista(), new Modelo());
                break;
            case "crearTablaRecambios":
                try {
                    modelo.crearTablaRecambios();
                } catch (SQLException ex) {
                    Util.showErrorAlert("Error al crear la tabla recambios");
                    ex.printStackTrace();
                }
                break;

            case "Asignar":
                if (vista.comboBoxTienda.getSelectedItem() != null && vista.comboBoxCliente.getSelectedItem() != null && vista.comboBoxRecambio.getSelectedItem() != null){
                    int recambio = Integer.parseInt(vista.comboBoxRecambio.getSelectedItem().toString().split("-")[0]);
                    System.out.println(recambio);
                    int tienda = Integer.parseInt(vista.comboBoxTienda.getSelectedItem().toString().split("-")[0]);
                    System.out.println(tienda);
                    int cliente = Integer.parseInt(vista.comboBoxCliente.getSelectedItem().toString().split("-")[0]);
                    System.out.println(cliente);
                    try {
                        modelo.insertarClienteTiendaRecambio(recambio,cliente,tienda);
                        modelo.restarUnRecambio(recambio);
                        cargarFilasAsignar(modelo.obtenerClientesTiendasRecambio());
                        vista.comboBoxRecambio.setSelectedItem(-1);
                        vista.comboBoxCliente.setSelectedItem(-1);
                        vista.comboBoxTienda.setSelectedItem(-1);

                        rellenarCombosAsignar();

                        cargarFilasAsignar(modelo.obtenerClientesTiendasRecambio());
                    } catch (SQLException ex) {
                        Util.showErrorAlert("Error al asignar");
                        ex.printStackTrace();
                    }
                }else {
                    Util.showWarningAlert("Faltan elementos por seleccionar");
                }
                break;
        }
    }

    /**
     * Metodo que rellena los comboBox del panel Asignar
     * @throws SQLException
     */
    private void rellenarCombosAsignar() throws SQLException {
        vista.comboBoxCliente.removeAllItems();
        vista.comboBoxTienda.removeAllItems();
        vista.comboBoxRecambio.removeAllItems();

        modelo.setComboBoxAsignar(modelo.obtenerDatos("recambio"), vista.comboBoxRecambio);
        modelo.setComboBoxAsignar(modelo.obtenerDatos("cliente"), vista.comboBoxCliente);
        modelo.setComboBoxAsignar(modelo.obtenerDatos("tienda"), vista.comboBoxTienda);
    }

    /**
     * Metodo que activa todos los botones para su uso
     */
    private void activarBotones() {
        vista.btnNuevo.setEnabled(true);
        vista. btnNuevoCliente.setEnabled(true);
        vista.btnNuevoTienda.setEnabled(true);

        vista.btnModificar.setEnabled(true);
        vista.btnModificarCliente.setEnabled(true);
        vista.btnModificarTienda.setEnabled(true);

        vista. btnBuscar.setEnabled(true);

        vista.btnEliminar.setEnabled(true);
        vista.btnEliminarCliente.setEnabled(true);
        vista.btnEliminarTienda.setEnabled(true);

        vista.btnAsignar.setEnabled(true);
    }

    /**
     * Metodo que descativa los botones de la aplicación
     */
    private void desactivarBotones() {
        vista.btnNuevo.setEnabled(false);
        vista. btnNuevoCliente.setEnabled(false);
        vista.btnNuevoTienda.setEnabled(false);

        vista.btnModificar.setEnabled(false);
        vista.btnModificarCliente.setEnabled(false);
        vista.btnModificarTienda.setEnabled(false);

        vista. btnBuscar.setEnabled(false);

        vista.btnEliminar.setEnabled(false);
        vista.btnEliminarCliente.setEnabled(false);
        vista.btnEliminarTienda.setEnabled(false);

        vista.btnAsignar.setEnabled(false);
    }

    /**
     * Metodo que vacia todos los campos de recambios
     */
    private void limpiarCamposRecambio() {
        vista.txtPrecio.setText(null);
        vista.txtCantidad.setText(null);
        vista.txtNombre.setText(null);
        vista.txtPrecio.setText(null);
        vista.dateTimePicker.setDateTimePermissive(null);
        vista.combo_tipo.setSelectedIndex(-1);
        vista.txtNombre.requestFocus();
    }

    /**
     * Metodo que vacia todos los campos de clientes
     */
    private void limpiarCamposCliente() {
        vista.txtNombreCliente.setText(null);
        vista.txtApellidosCliente.setText(null);
        vista.txtTelefonoCliente.setText(null);
        vista.txtDireccionCliente.setText(null);
        vista.comboCliente.setSelectedIndex(-1);
        vista.txtNombreCliente.requestFocus();
    }

    /**
     * Metodo que vacia todos los campos de tienda
     */
    private void limpiarCamposTienda() {
        vista.txtNombreTienda.setText(null);
        vista.txtTelefonoTienda.setText(null);
        vista.txtDireccionTienda.setText(null);
        vista.txtNombreTienda.requestFocus();
    }

    /**
     * Metodo que eliminar todas las filas del JTable
     * @param tabla JTable de nuestro programa
     */
    public void limpiarTabla(JTable tabla){
        try {
            DefaultTableModel modelo=(DefaultTableModel) tabla.getModel();
            int filas=tabla.getRowCount();
            for (int i = 0;filas>i; i++) {
                modelo.removeRow(0);
            }
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Error al limpiar la tabla.");
        }
    }

    /**
     * Metodo que establece la ip, usuario y password de la base de datos en la ventana de administracion de la base de datos
     */
    private void setOptions() {
        vista.optionDialog.txtIp.setText(modelo.getIP());
        vista.optionDialog.txtUsuario.setText(modelo.getUser());
        vista.optionDialog.txtContrasena.setText(modelo.getPassword());
    }

    /**
     * Metodo que comprueba si una cadena solo contiene numeros
     * @param cadena String cadena a analizar
     * @return Boolean true o false
     */
    public boolean isNumeric(String cadena) {
        boolean resultado;
        try {
            Integer.parseInt(cadena);
            resultado = true;
        } catch (NumberFormatException excepcion) {
            try {
                Float.parseFloat(cadena);
                resultado = true;
            }catch (NumberFormatException excepcion2){
                resultado = false;
            }
        }
        return resultado;
    }

    @Override
    public void tableChanged(TableModelEvent e) {

    }
}
