package com.javiergasca.practica2UD2.gui;

import javax.swing.*;
import java.awt.*;

/**
 * Clase que ejecuta una ventana para cambiar los datos de conexión a una base de datos
 */
public class OptionDialog extends JDialog{
    private JPanel panel1;
    private JLabel lblIp;
    JTextField txtIp;
    JTextField txtUsuario;
    JTextField txtContrasena;
    JTextField txtContrasenaAdmin;
    public JButton btnGuardar;
    private Frame frame;

    /**
     * Constructor de la clase.
     * @param owner propietario del dialog
     */
    public OptionDialog(Frame owner) {
        super(owner,"Opciones", true);
        this.frame = owner;
        initDialog();
    }

    /**
     * Inicializa el JDialog
     */
    private void initDialog() {
        this.setContentPane(panel1);
        this.panel1.setBorder(BorderFactory.createEmptyBorder(20, 20, 20, 20));
        this.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
        this.pack();
        this.setSize(new Dimension(this.getWidth()+200, this.getHeight()));
        this.setLocationRelativeTo(frame);
    }
}
