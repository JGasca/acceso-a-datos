package com.javiergasca.practica2UD2.gui;

import com.github.lgooddatepicker.components.DateTimePicker;
import com.javiergasca.practica2UD2.emun.Edad;
import com.javiergasca.practica2UD2.emun.TipoMaquina;

import javax.swing.*;
import javax.swing.border.BevelBorder;
import javax.swing.border.Border;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;

/**
 * Clase que crea la vista del programa
 */
public class Vista {
    JFrame frame;
    JPanel panel1;
    JTabbedPane tabbedPane1;
    JTextField txtNombre;
    JTextField txtCantidad;
    JTextField txtPrecio;
    DateTimePicker dateTimePicker;
    JComboBox combo_tipo;
    JButton btnNuevo;
    JButton btnEliminar;
    JButton btnModificar;
    JLabel lblAccion;
    JTable table;
    JButton btnBuscar;
    JComboBox combo_tipo2;
    JLabel lblNombre;
    JLabel lblCantidad;
    JLabel lblPrecio;
    JLabel lblFechaExpira;
    JLabel lblTipo;
    JTextField txtNombreCliente;
    JTextField txtApellidosCliente;
    JTextField txtDireccionCliente;
    JTextField txtTelefonoCliente;
    JTextField txtNombreTienda;
    JTextField txtDireccionTienda;
    JTextField txtTelefonoTienda;
    JButton btnNuevoCliente;
    JButton btnModificarCliente;
    JTable tableCliente;
    JLabel lblActionCliente;
    JButton btnEliminarCliente;
    JButton btnNuevoTienda;
    JTable tablaTienda;
    JLabel lblActionTienda;
    private JPanel Asignar;
    JButton btnModificarTienda;
    JButton btnEliminarTienda;
    JComboBox comboCliente;
    JComboBox comboBoxRecambio;
    JComboBox comboBoxCliente;
    JComboBox comboBoxTienda;
    JTable tablaAsignar;
    JButton btnAsignar;

    DefaultTableModel dtm;
    DefaultTableModel dtmCliente;
    DefaultTableModel dtmTienda;
    DefaultTableModel dtmAsignar;

    JMenuItem itemConectar;
    JMenuItem itemOpcionesBBDD;
    JMenuItem itemSalir;
    JMenuItem itemCrearTablaRecambios;

    /*OPTION DIALOG*/
    OptionDialog optionDialog;
    /*SAVECHANGESDIALOG*/
    JDialog adminPasswordDialog;
    JButton btnValidate;
    JPasswordField adminPassword;

    /**
     * Constructor de la clase Vista
     */
    public Vista(){
        frame = new JFrame("Vista");
        frame.setContentPane(panel1);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setSize(new Dimension(frame.getWidth()+200,frame.getHeight()+23));
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);

        dtm =new DefaultTableModel(){
            @Override
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        };
        dtmCliente =new DefaultTableModel(){
            @Override
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        };
        dtmTienda =new DefaultTableModel(){
            @Override
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        };
        dtmAsignar =new DefaultTableModel(){
            @Override
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        };

        table.setModel(dtm);
        tableCliente.setModel(dtmCliente);
        tablaTienda.setModel(dtmTienda);
        tablaAsignar.setModel(dtmAsignar);

        optionDialog = new OptionDialog(frame);
        crearMenu();
        setEnumComboBox();
        setAdminDialog();

        DefaultTableCellRenderer centerRenderer = new DefaultTableCellRenderer();
        centerRenderer.setHorizontalAlignment( JLabel.CENTER );
        table.setDefaultRenderer(String.class, centerRenderer);

        desactivarBotones();

        txtCantidad.setBorder(BorderFactory.createEmptyBorder(0,0,0,0));
        txtPrecio.setBorder(BorderFactory.createEmptyBorder(0,0,0,0));

        //txtNombre.setBorder(BorderFactory.createBevelBorder(10,Color.WHITE,Color.WHITE));
        txtNombre.setBorder(BorderFactory.createBevelBorder(BevelBorder.LOWERED));
    }

    /**
     * Metodo que desactiva los botones el programa
     */
    private void desactivarBotones() {
        btnNuevo.setEnabled(false);
        btnNuevoCliente.setEnabled(false);
        btnNuevoTienda.setEnabled(false);

        btnModificar.setEnabled(false);
        btnModificarCliente.setEnabled(false);
        btnModificarTienda.setEnabled(false);

        btnBuscar.setEnabled(false);

        btnEliminar.setEnabled(false);
        btnEliminarCliente.setEnabled(false);
        btnEliminarTienda.setEnabled(false);

        btnAsignar.setEnabled(false);
    }

    /**
     * Metodo que crea el menu de arriba con las opciones de conectar, opciones BBDD, CrearTablaRecambios y salir
     */
    private void crearMenu() {
        itemConectar = new JMenuItem("Conectar");
        itemConectar.setActionCommand("Conectar");
        itemOpcionesBBDD = new JMenuItem("Opciones BBDD");
        itemOpcionesBBDD.setActionCommand("Opciones BBDD");
        itemCrearTablaRecambios = new JMenuItem("Crear tabla recambios");
        itemCrearTablaRecambios.setActionCommand("crearTablaRecambios");
        itemSalir = new JMenuItem("Salir");
        itemSalir.setActionCommand("Salir");

        JMenu menuArchivo = new JMenu("Archivo");
        menuArchivo.add(itemConectar);
        menuArchivo.add(itemOpcionesBBDD);
        menuArchivo.add(itemCrearTablaRecambios);
        menuArchivo.add(itemSalir);
        JMenuBar barraMenu = new JMenuBar();
        barraMenu.add(menuArchivo);

        frame.setJMenuBar(barraMenu);
    }

    /**
     * Metodo que establece los valores de los comboBox
     */
    public void setEnumComboBox(){
        combo_tipo2.addItem("Todos");
        for(TipoMaquina constant: TipoMaquina.values()) {
            combo_tipo.addItem(constant.getValor());
            combo_tipo2.addItem(constant.getValor());
        }
        combo_tipo.setSelectedIndex(-1);

        for(Edad edad: Edad.values()) {
            comboCliente.addItem(edad.getValor());
        }
        comboCliente.setSelectedIndex(-1);
    }

    /**
     * Metodo que inicializa la ventana de administración de los datos de la base de datos
     */
    private void setAdminDialog() {
        //contraseña para validar 1234
        btnValidate = new JButton("Validar");
        btnValidate.setActionCommand("abrirOpciones");
        adminPassword = new JPasswordField();
        adminPassword.setPreferredSize(new Dimension(100, 26));
        Object[] options = new Object[] {adminPassword, btnValidate};
        JOptionPane jop = new JOptionPane("Introduce la contraseña"
                , JOptionPane.WARNING_MESSAGE, JOptionPane.YES_NO_OPTION, null, options);

        adminPasswordDialog = new JDialog(frame,"Opciones",true);
        adminPasswordDialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
        adminPasswordDialog.setContentPane(jop);
        adminPasswordDialog.pack();
        adminPasswordDialog.setLocationRelativeTo(frame);
    }
}



