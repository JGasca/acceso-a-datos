package com.javiergasca.practica2UD2.gui;

import Encriptacion.Encriptar;
import com.javiergasca.practica2UD2.Util.Util;

import javax.swing.*;
import java.io.*;
import java.sql.*;
import java.time.LocalDateTime;
import java.util.Properties;

/**
 * Clase Modelo
 */
public class Modelo {
    private Connection conexion;
    private String ip;
    private String user;
    private String password;
    private String adminPassword;
    private static String nombre_BBDD = "recambios_informatica2";

    /**
     * Construcotor de la clase Modelo, carga el archivo de configuración
     */
    public Modelo(){
        getPropValues();
    }

    /**
     * Metodo que devuelve la ip
     * @return String ip
     */
    String getIP() {
        return ip;
    }

    /**
     * Metodo que develve el usuario de la base de datos
     * @return Usuario
     */
    String getUser() {
        return user;
    }

    /**
     * Metodo que devuelve la contraseña de la base de datos
     * @return password
     */
    String getPassword() {
        return password;
    }

    /**
     * Metodo que devuelve la contraseña del administrador
     * @return adminpassword
     */
    String getAdminPassword() {
        return adminPassword;
    }

    /**
     * Metodo que crea la conexión con la base de datos
     * @throws SQLException
     */
    public void conectar() throws SQLException {
        conexion=null;
        conexion= DriverManager.getConnection("jdbc:mysql://"+ ip +":3306/" + nombre_BBDD,user,password);
    }

    /**
     * Metodo que cierra la conexión con la base de datos
     * @throws SQLException
     */
    public void desconectar() throws SQLException {
        conexion.close();
        conexion=null;
    }

    /**
     * Metodo que añade un recambio a la base de datos
     * @param nombre String Nombre del recambio
     * @param cantidad String Cantidad del recambio
     * @param precio String Precio del recambio
     * @param fechaExpi Fecha de expiración de la oferta
     * @param tipo Tipo de maquina a la que va dirigido
     * @return Devuelve un numero que corresponde con un error
     * @throws SQLException
     */
    public int addRecambio(String nombre, String cantidad, String precio, LocalDateTime fechaExpi, String tipo) throws SQLException {
        if (conexion==null){
            Util.showErrorAlert("Error, no hay conexión con la base de datos");
            return -1;
        }
        if (conexion.isClosed())
            return -2;

        String consulta="INSERT INTO recambio(nombre, cantidad, precio, fechaexpira, tipo)"+ "VALUES (?,?,?,?,?)";

        PreparedStatement sentencia=null;

        sentencia=conexion.prepareStatement(consulta);

        sentencia.setString(1,nombre);
        sentencia.setString(2,cantidad);
        sentencia.setString(3,precio);
        sentencia.setTimestamp(4,Timestamp.valueOf(fechaExpi));
        sentencia.setString(5,tipo);

        int numeroRegistros=sentencia.executeUpdate();

        if (sentencia!=null) {
            sentencia.close();
        }

        return numeroRegistros;

    }

    /**
     * Metodo que añade una tienda a la base de datos
     * @param nombre String Nombre de la tienda
     * @param direccion String Direccion de la tienda
     * @param telefono String Telefono de la tienda
     * @return Devuelve un numero que corresponde con un error
     * @throws SQLException
     */
    public int addTienda(String nombre, String direccion, String telefono) throws SQLException {
        if (conexion==null){
            Util.showErrorAlert("Error, no hay conexión con la base de datos");
            return -1;
        }
        if (conexion.isClosed())
            return -2;

        String consulta="INSERT INTO tienda(nombre, direccion, telefono)"+ "VALUES (?,?,?)";

        PreparedStatement sentencia=null;

        sentencia=conexion.prepareStatement(consulta);

        sentencia.setString(1,nombre);
        sentencia.setString(2,direccion);
        sentencia.setString(3,telefono);

        int numeroRegistros=sentencia.executeUpdate();

        if (sentencia!=null) {
            sentencia.close();
        }

        return numeroRegistros;

    }

    /**
     * Metodo que añade un cliente a la base de datos
     * @param nombre String Nombre del cliente
     * @param apellidos String apellidos del cliente
     * @param direccion String dirección del cliente
     * @param telefono String telefono del cliente
     * @param edad String edad del cliente
     * @return Devuelve un numero que corresponde con un error
     * @throws SQLException
     */
    public int addcliente(String nombre, String apellidos, String direccion, String telefono, String edad) throws SQLException {
        if (conexion==null){
            Util.showErrorAlert("Error, no hay conexión con la base de datos");
            return -1;
        }
        if (conexion.isClosed())
            return -2;

        String consulta="INSERT INTO cliente(nombre, apellidos, direccion, telefono, edad)"+ "VALUES (?,?,?,?,?)";

        PreparedStatement sentencia=null;

        sentencia=conexion.prepareStatement(consulta);
        sentencia.setString(1,nombre);
        sentencia.setString(2,apellidos);
        sentencia.setString(3,direccion);
        sentencia.setString(4,telefono);
        sentencia.setString(5,edad);

        int numeroRegistros=sentencia.executeUpdate();

        if (sentencia!=null) {
            sentencia.close();
        }

        return numeroRegistros;

    }

    /**
     * Metodo que obtiene todos los datos de la tabla que se pase por parametro
     * @param tabla Nombre de la tabla de la que queremos obtener los datos
     * @return Devuelve un ResultSet con todos los datos
     * @throws SQLException
     */
    public ResultSet obtenerDatos(String tabla) throws SQLException {
        if (conexion==null) {
            Util.showErrorAlert("Error al cargar los datos");
            return null;
        }
        if (conexion.isClosed()) {
            return null;
        }
        String consulta = "";
        switch (tabla){
            case "recambio":
                consulta ="SELECT * FROM recambio";
                break;
            case "cliente":
                consulta ="SELECT * FROM cliente";
                break;
            case "tienda":
                consulta ="SELECT * FROM tienda";
                break;
        }
        //String consulta ="SELECT * FROM recambio";
        PreparedStatement sentencia = null;
        sentencia=conexion.prepareStatement(consulta);
        ResultSet resultado=sentencia.executeQuery();
        return resultado;
    }

    /**
     * Metodo que modifica un Recambio en la base de datos
     * @param id id del recambio
     * @param nombre String Nombre del recambio
     * @param cantidad String Cantidad del recambio
     * @param precio String Precio del recambio
     * @param fechaExpi Fecha de expiración de la oferta
     * @param tipo Tipo de maquina a la que va dirigido
     * @return Devuelve un numero que corresponde a un codigo de error
     * @throws SQLException
     */
    public int modificarRecambio(int id, String nombre, String cantidad, String precio, LocalDateTime fechaExpi, String tipo) throws SQLException {
        if (conexion==null){
            Util.showErrorAlert("Error, no hay conexión con la base de datos");
            return -1;
        }
        if (conexion.isClosed())
            return -2;

        String consulta="UPDATE recambio SET nombre=?,cantidad = ? , precio = ?, fechaexpira = ?, tipo = ? WHERE id=?";

        PreparedStatement sentencia=null;

        sentencia=conexion.prepareStatement(consulta);

        sentencia.setString(1,nombre);
        sentencia.setString(2,cantidad);
        sentencia.setString(3,precio);
        sentencia.setTimestamp(4,Timestamp.valueOf(fechaExpi));
        sentencia.setString(5,tipo);
        sentencia.setInt(6,id);

        int numeroRegistros=sentencia.executeUpdate();

        if (sentencia!=null) {
            sentencia.close();
        }

        return 0;
    }

    /**
     * Metodo que modifica una tienda
     * @param id id de la tienda a modificar
     * @param nombre Nombre de la tienda a modificar
     * @param direccion Dirección de la tienda a modificar
     * @param telefono Telefono de la tienda a modificar
     * @return Devuelve un numero que corresponde a un codigo de error
     * @throws SQLException
     */
    public int modificarTienda(int id, String nombre, String direccion, String telefono) throws SQLException {
        if (conexion==null){
            Util.showErrorAlert("Error, no hay conexión con la base de datos");
            return -1;
        }
        if (conexion.isClosed())
            return -2;

        String consulta="UPDATE tienda SET nombre=?,direccion = ? , telefono = ? WHERE id=?";

        PreparedStatement sentencia=null;

        sentencia=conexion.prepareStatement(consulta);

        sentencia.setString(1,nombre);
        sentencia.setString(2,direccion);
        sentencia.setString(3,telefono);
        sentencia.setInt(4,id);

        int numeroRegistros=sentencia.executeUpdate();

        if (sentencia!=null) {
            sentencia.close();
        }

        return 0;
    }

    /**
     * Metodo que modifica un cliente
     * @param id id del cliente a modificar
     * @param nombre Nombre del cliente a modificar
     * @param apellidos Apellidos del cliente a modificar
     * @param direccion Dirección del cliente a modificar
     * @param telefono Telefono del cliente a modificar
     * @param edad Edad del cliente a modificar
     * @return Devuelve un numero que corresponde a un codigo de error
     * @throws SQLException
     */
    public int modificarCliente(int id, String nombre, String apellidos,String direccion ,String telefono, String edad) throws SQLException {
        if (conexion==null){
            Util.showErrorAlert("Error, no hay conexión con la base de datos");
            return -1;
        }
        if (conexion.isClosed())
            return -2;

        String consulta="UPDATE cliente SET nombre=?,apellidos = ?,direccion = ? , telefono = ?,edad = ? WHERE id=?";

        PreparedStatement sentencia=null;

        sentencia=conexion.prepareStatement(consulta);

        sentencia.setString(1,nombre);
        sentencia.setString(2,apellidos);
        sentencia.setString(3,direccion);
        sentencia.setString(4,telefono);
        sentencia.setString(5,edad);
        sentencia.setInt(6,id);

        int numeroRegistros=sentencia.executeUpdate();

        if (sentencia!=null) {
            sentencia.close();
        }

        return 0;
    }

    /**
     * Metodo que elmimina un recambio
     * @param id id del recambio que queremos eliminar
     * @return Devuelve un numero que corresponde a un codigo de error
     * @throws SQLException
     */
    public int eliminarRecambio(int id) throws SQLException {
        if (conexion == null){
            return -1;
        }
        if (conexion.isClosed()){
            return -2;
        }

        String consulta = "DELETE FROM recambio WHERE id=?";
        PreparedStatement sentencia = null;

        sentencia=conexion.prepareStatement(consulta);
        sentencia.setInt(1,id);

        int resultado = sentencia.executeUpdate();

        if (sentencia != null){
            sentencia.close();
        }

        return resultado;
    }

    /**
     * Metodo que elimina una tienda
     * @param id id de la tienda que queremos eliminar
     * @return Devuelve un numero que corresponde a un codigo de error
     * @throws SQLException
     */
    public int eliminarTienda(int id) throws SQLException {
        if (conexion == null){
            return -1;
        }
        if (conexion.isClosed()){
            return -2;
        }

        String consulta = "DELETE FROM tienda WHERE id=?";
        PreparedStatement sentencia = null;

        sentencia=conexion.prepareStatement(consulta);
        sentencia.setInt(1,id);

        int resultado = sentencia.executeUpdate();

        if (sentencia != null){
            sentencia.close();
        }

        return resultado;
    }

    /**
     * Metodo que elimina un cliente
     * @param id id del cliente que queremos eliminar
     * @return Devuelve un numero que corresponde a un codigo de error
     * @throws SQLException
     */
    public int eliminarCliente(int id) throws SQLException {
        if (conexion == null){
            return -1;
        }
        if (conexion.isClosed()){
            return -2;
        }

        String consulta = "DELETE FROM cliente WHERE id=?";
        PreparedStatement sentencia = null;

        sentencia=conexion.prepareStatement(consulta);
        sentencia.setInt(1,id);

        int resultado = sentencia.executeUpdate();

        if (sentencia != null){
            sentencia.close();
        }

        return resultado;
    }

    /**
     * Metodo que devuelve un ResultSet con todos los resultados segun el tipo de maquina introducida por parametro
     * @param tipo String tipo de maquina
     * @return Devuelve un Resultset
     * @throws SQLException
     */
    public ResultSet buscarTipo(String tipo) throws SQLException {
        if (conexion == null){
            return null;
        }
        if (conexion.isClosed()){
            return null;
        }
        String consulta = "";
        if (tipo.equals("Todos")){
            consulta = "SELECT * FROM recambio";
        }else{
            tipo = "'" + tipo + "'";
            consulta ="SELECT * FROM recambio WHERE tipo=" + tipo;
        }

        PreparedStatement sentencia = null;
        sentencia=conexion.prepareStatement(consulta);
        ResultSet resultado=sentencia.executeQuery();
        return resultado;
    }

    /**
     * Actualiza el archivo de configuración
     * @param ip String ip de la BBDD
     * @param user String usuario de la BBDD
     * @param pass String contraseña de la BBDD
     * @param adminPass String contraseña del administrador
     */
    void setPropValues(String ip, String user, String pass, String adminPass) {
        Boolean contra = false;
        Boolean contraAdmin = false;

        if (adminPass.equals(adminPassword)){
            contraAdmin = true;
        }
        if (pass.equals(password) && !pass.equals("")){
            contra = true;
        }
        /* Encripto la ip y el usuario, si la contraseña de la BBDD o la contraseña del admin no se han cambiado
            no se vuelven a encriptar.
        */
        try {
            ip = Encriptar.encriptar(ip);
            user = Encriptar.encriptar(user);
            if (!contra){
                pass = Encriptar.encriptar(pass);
            }
            if (!contraAdmin){
                adminPass = Encriptar.hasear(adminPass);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            Properties prop = new Properties();
            prop.setProperty("ip", ip);
            prop.setProperty("user", user);
            if (contra) {
                prop.setProperty("pass", Encriptar.encriptar(getPassword()));
            } else {
                prop.setProperty("pass", pass);
            }
            prop.setProperty("admin", adminPass);

            OutputStream out = new FileOutputStream("config.properties");
            prop.store(out, null);

        } catch (IOException ex) {
            ex.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        this.ip = ip;
        this.user = user;
        this.password = pass;
        this.adminPassword = adminPass;
    }

    /**
     * Metodo que crea el archivo config.properties y le pasa los datos de la base de datos
     * Este metodo solo sirve en el caso que que no exista el archivo.
     * @param ip String ip de la BBDD
     * @param user String usuario de la BBDD
     * @param pass String contraseña de la BBDD
     * @param adminPass String contraseña del administrador
     */
   void setPropValuesSinEncrip(String ip, String user, String pass, String adminPass) {
        try {
            Properties prop = new Properties();
            prop.setProperty("ip", ip);
            prop.setProperty("user", user);
            prop.setProperty("pass", pass);
            prop.setProperty("admin", adminPass);

            OutputStream out = new FileOutputStream("config.properties");
            prop.store(out, null);

            getPropValues();

        } catch (IOException ex) {
            ex.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Lee el archivo de configuración y carga los atributos
     */
    private void getPropValues() {
        InputStream inputStream = null;
        try {
            Properties prop = new Properties();
            String propFileName = "config.properties";

            inputStream = new FileInputStream(propFileName);

            prop.load(inputStream);
            ip = Encriptar.desencriptar(prop.getProperty("ip"));
            user = Encriptar.desencriptar(prop.getProperty("user"));
            password = Encriptar.desencriptar(prop.getProperty("pass"));
            adminPassword = prop.getProperty("admin");
        } catch (Exception e) {
            System.out.println("Exception: " + e);
        } finally {
            try {
                if (inputStream != null) inputStream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Metodo que crea la tabla recambio si no esta creada
     * @throws SQLException
     */
    public void crearTablaRecambios() throws SQLException {
        conectar();

        String sentenciaSql="call crearTablaRecambio()";
        CallableStatement procedimiento=null;
        procedimiento=conexion.prepareCall(sentenciaSql);
        procedimiento.execute();

        desconectar();
    }

    /**
     * Metodo que resta uno a la cantidad de un recambio con un procedmiento almacenado
     * @param id_recambio id del recambio que queremos restar uno
     * @throws SQLException
     */
    public void restarUnRecambio(int id_recambio) throws SQLException {
        String sentenciaSql="call restarUno("+ id_recambio +")";
        CallableStatement procedimiento=null;
        procedimiento=conexion.prepareCall(sentenciaSql);
        procedimiento.execute();
    }

    /**
     * Metodo que resta uno al recambio con el id que hemos introducido
     * @param id int id del recambio que queremos restar
     * @return Devuelve un numero que corresponde a un codigo de error
     * @throws SQLException
     */
    public int restarUnRecambio2(int id) throws SQLException {
        if (conexion==null){
            Util.showErrorAlert("Error, no hay conexión con la base de datos");
            return -1;
        }
        if (conexion.isClosed())
            return -2;

        String consulta="UPDATE recambio SET cantidad= cantidad - 1 WHERE id=?";

        PreparedStatement sentencia=null;

        sentencia=conexion.prepareStatement(consulta);

        sentencia.setInt(1,id);

        int numeroRegistros=sentencia.executeUpdate();

        if (sentencia!=null) {
            sentencia.close();
        }

        return 0;
    }

    /**
     * Metodo que rellena un comboBox introducido por parametro con el resultado de una consulta
     * @param resultado Resultado de una consulta sql
     * @param combo JComboBox al que se lo queremos añadir
     */
    public void setComboBoxAsignar(ResultSet resultado, JComboBox combo){
        int count=0;
        try {
            while (resultado.next()){
                int id = (int) resultado.getObject(1);
                String nombre = (String) resultado.getObject(2);
                String extra = (String) resultado.getObject(3);
                combo.addItem(id+"-"+nombre +"-"+extra);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        combo.setSelectedIndex(-1);
    }

    /**
     * Metodo que duelve el nombre y el apellido del cliente, el nombre del recambio y el nombre y la dirección de la tienda
     * @return Devuelve un Resultset con todos los datos
     * @throws SQLException
     */

    public ResultSet obtenerClientesTiendasRecambio() throws SQLException {
        if (conexion == null){
            return null;
        }
        if (conexion.isClosed()){
            return null;
        }

        String consulta = "";
        consulta = "SELECT c.nombre, c.apellidos,r.nombre,t.nombre,t.direccion, recambio_cliente_tienda.fecha " +
                "FROM recambio_cliente_tienda " +
                "INNER JOIN cliente c on recambio_cliente_tienda.id_cliente = c.id " +
                "INNER JOIN recambio r on recambio_cliente_tienda.id_recambio = r.id " +
                "INNER JOIN tienda t on recambio_cliente_tienda.id_tienda = t.id";

        PreparedStatement sentencia = null;
        sentencia=conexion.prepareStatement(consulta);
        ResultSet resultado=sentencia.executeQuery();
        return resultado;


    }

    /**
     * Meotodo que inserta una relacion en la tabla recambio_cliente_tienda
     * @param id_recambio id del recambio correspondiente
     * @param id_cliente id del cliente correspondiente
     * @param id_tienda id de la tienda correspondiente
     * @return Devuelve un numero que corresponde a un codigo de error
     * @throws SQLException
     */
    public int insertarClienteTiendaRecambio(int id_recambio, int id_cliente, int id_tienda) throws SQLException {
        if (conexion==null){
            Util.showErrorAlert("Error, no hay conexión con la base de datos");
            return -1;
        }
        if (conexion.isClosed())
            return -2;

        String consulta="INSERT INTO recambio_cliente_tienda(id_cliente, id_tienda, id_recambio)"+ "VALUES (?,?,?)";

        PreparedStatement sentencia=null;

        sentencia=conexion.prepareStatement(consulta);
        sentencia.setInt(1,id_cliente);
        sentencia.setInt(2,id_tienda);
        sentencia.setInt(3,id_recambio);

        int numeroRegistros=sentencia.executeUpdate();

        if (sentencia!=null) {
            sentencia.close();
        }

        return numeroRegistros;
    }
}
