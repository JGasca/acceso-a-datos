package com.javiergasca.practica2UD2.emun;

/**
 * Clase que contiene los tipos que maquinas que podemos seleccionar
 */
public enum TipoMaquina {
    PHONE("Iphone"),
    MOVILANDROID("Movil Android"),
    PORTATIL("Portatil"),
    SOBREMESA("Sobremesa"),
    MONITOR("Monitor"),
    CONSOLA("Consola"),
    TELEVISION("Television");

    private String valor;

    /**
     * Metodo que establece un nuevo tipo de recambio
     * @param valor Nombre del nuevo tipo de recambio
     */
    TipoMaquina(String valor) {

        this.valor = valor;
    }

    /**
     * Metodo que obtiene los valores
     * @return Los valores que contiene la clase
     */
    public String getValor() {
        return valor;
    }
}
