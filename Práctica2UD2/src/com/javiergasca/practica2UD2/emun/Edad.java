package com.javiergasca.practica2UD2.emun;

/**
 * Clase que contiene los tipos que maquinas que podemos seleccionar
 */
public enum Edad {
    EDAD15_20("15 a 20"),
    EDAD21_25("21 a 25"),
    EDAD26_35("26 a 30"),
    EDAD31_40("31 a 40"),
    EDAD41_60("41 a 60"),
    EDAD61_80("61 a 80"),
    EDAD81_100("81 a 100");


    private String valor;

    /**
     * Metodo que establece un nuevo tipo de edad
     * @param valor Nombre del nuevo tipo de edad
     */
    Edad(String valor) {

        this.valor = valor;
    }

    /**
     * Metodo que obtiene los valores
     * @return Los valores que contiene la clase
     */
    public String getValor() {
        return valor;
    }
}
