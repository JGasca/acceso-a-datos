package com.javiergasca.EstructurasDatos;

import java.util.*;

public class EjercicioHashMap1 {
    public static void main(String[] args) {
        /*
        System.out.println("Añadir un elemento (clave-valor)");
        HashMap<String,String> mapLibros = new HashMap<String, String>();
        System.out.println("Insertar un dato");
        Libro libro = new Libro();
        libro.setTitulo("Secuestrado");
        libro.setAutor("Robert Louis");
        mapLibros.put(libro.getTitulo(),"Robert");

        System.out.println("Mostramos un valor dado el titulo");
        String tituloLibro = "Secuestrado";
        System.out.println(mapLibros.get(tituloLibro));

        System.out.println("Insertamos dos datos mas ");
        Libro libro1 = new Libro("El tiempo en paris", "Perico");
        mapLibros.put(libro1.getTitulo(),libro1.getAutor());

        Libro libro2 = new Libro("Don quijote", "josecho");
        mapLibros.put(libro2.getTitulo(), libro2.getAutor());

        System.out.println("Mostramos valores con un for each");
        String autor;
        for (String titulo : mapLibros.keySet()) {
            autor = mapLibros.get(titulo);
            System.out.println(titulo + " - " + autor);
        }

        System.out.println("Obetener un elemento");
        String titulo = "Secuestrado";
        System.out.println(mapLibros.get(titulo));

        System.out.println("Imprimimos el mao con iterator");
        String titulo2;
        Iterator<String> it = mapLibros.keySet().iterator();
        while (it.hasNext()){
            titulo2 = it.next();
            autor = mapLibros.get(titulo2);
            System.out.println(titulo2 + "-" + autor);
        }

        System.out.println("Obetener una coleccion con todos los valores");
        Collection<String> coleccionLibros = mapLibros.values();

        System.out.println("Mostramos valores de los autores");
        for (Object elemento : coleccionLibros) {
            System.out.println(elemento + " ");
        }

        System.out.println("Obetener un set con todas las claves");
        Set<String> titulos = mapLibros.keySet();

        System.out.println("Mostramos todos los titulos");
        for (Object elemento : titulos) {
            System.out.println(elemento + " ");
        }

        System.out.println("*** Obetener una coleccion con todos los valores ***");
        System.out.println("Mostramos los valores");
        Collection<Libro> coleccionLibros = mapLibros.values();
        for (Libro elemento: coleccionLibros) {
            System.out.println(elemento + " ");
        }
        System.out.println();

        System.out.println("Comprobamos si existe una clave");
        String titulo = "Secuestrado";
        if (mapLibros.containsKey(titulo)){
            System.out.println("El libor con el titulo " + titulo + " existe en la coleccion");
        }

        System.out.println("Obtener un set con todas las claves");
        Set<String> titulos  = mapLibros.keySet();

        System.out.println("Comprobamos el tamaño del map");
        System.out.println("Tienes " + mapLibros.size() + " libros en tu coleccion");

        System.out.println("Concatenar todos los elementos de otro map");
        Map<String,Libro> maslibros = new HashMap<String, Libro>();
        Libro maslibros1 = new Libro("La montaña magica", "Tomas");
        maslibros.put(libro.getTitulo(),maslibros1);
        Libro maslibros2 = new Libro("Oso2", "Tomaso");
        maslibros.put(libro.getTitulo(),maslibros2);
        Libro maslibros3 = new Libro("Chill chill", "Roberto");
        maslibros.put(libro.getTitulo(),maslibros3);


        //Cambio los datos de mapLibros correspondiente a libro
        /*
        mapLibros.putAll(maslibros);

        System.out.println("Mostramos los valores");
        for (Libro elemento: coleccionLibros) {
            System.out.println(elemento + " ");
        }

        System.out.println("Eliminar un elemento por clave");
        titulo = "Secuestrado";
        mapLibros.remove(titulo);

        System.out.println("Mostramos los valores");
        for (Libro elemento: coleccionLibros) {
            System.out.println(elemento + " ");
        }


        System.out.println("Eliminar todos los elementos");
        mapLibros.clear();
        System.out.println("Comprobar si esta vacio");
        if (mapLibros.isEmpty()){
            System.out.println("La colección de libros esta vacia");
        }

         */
    }
}
