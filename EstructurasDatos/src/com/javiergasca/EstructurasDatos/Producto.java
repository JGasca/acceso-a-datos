package com.javiergasca.EstructurasDatos;

/**
 *
 */
public class Producto implements Comparable{
    private String nombre;
    private int cantidad;

    public Producto(String nombre, int cantidad) {
        this.nombre = nombre;
        this.cantidad = cantidad;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    /**
     *
     * @param objeto
     * @return
     */
    public boolean equals(Object objeto){
        //Indicas en base a que atributos se iguala el objeto
        if (objeto==null) return false;
        Producto producto = (Producto)objeto;
        if (this.getNombre().equals(producto.getNombre())) return true;

        return false;
    }

    /**
     *
     * @return
     */
    public int hascode(){
        //Devuelve un identificador unico del objeto
        return this.getNombre().hashCode();
    }

    public int compareTo(Object objeto){
       //indica en base a que atributo se comapra el objeto
       //devuelve 0 si son iguales,
       Producto producto = (Producto) objeto;
       String nombreObjeto = producto.getNombre().toLowerCase();
       String nombreThis = this.getNombre().toLowerCase();

       return (nombreThis.compareTo(nombreObjeto));
    }

    @Override
    public String toString() {
        return "Producto{" +
                "nombre='" + nombre + '\'' +
                ", cantidad=" + cantidad +
                '}';
    }
}
