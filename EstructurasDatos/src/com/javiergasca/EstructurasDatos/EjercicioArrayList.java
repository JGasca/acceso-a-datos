package com.javiergasca.EstructurasDatos;

import java.util.ArrayList;
import java.util.List;

public class EjercicioArrayList {
    public static void main(String[] args) {
        System.out.println("--- Añadir elementos ---");
        List<String> listaCadenas = new ArrayList<>();
        String nuevaCadena1 = "Esto es una cadena1";
        String nuevaCadena2 = "Esto es una cadena2";
        String nuevaCadena3 = "Esto es una cadena3";

        listaCadenas.add(nuevaCadena1);
        listaCadenas.add(nuevaCadena2);
        listaCadenas.add(nuevaCadena3);

        System.out.println("--- Muestro datos ---");
        for (String elemento: listaCadenas){
            System.out.println(elemento);
        }

        System.out.println("--- Añadimmos un elemento en una posicion determinada ---");

        int posicion = 3;
        listaCadenas.add(posicion,"Esto es una cadena3");

        System.out.println("------ Muestro los datos ------");
        for (String elemento: listaCadenas){
            System.out.println(elemento);
        }

        System.out.println("--- Reemplazar un elemento en una posicion determinada ---");
        String otraCadena = "Esto es una cadena diferente";
        listaCadenas.set(3,otraCadena);

        System.out.println("------ Muestro los datos ------");
        for (String elemento: listaCadenas){
            System.out.println(elemento);
        }

        System.out.println("--- Eliminar de una posicion ---");
        listaCadenas.remove(0);

        System.out.println("------ Muestro los datos ------");
        for (String elemento: listaCadenas){
            System.out.println(elemento);
        }

        System.out.println("Obtener la referencia a un elemento");
        String unaCadena = listaCadenas.get(2);
        System.out.println("Elemento2 " + unaCadena);

        System.out.println("Conocer el numero de elementos");
        System.out.println("El arraylist tiene " + listaCadenas.size());

        System.out.println("Eliminar un elemento concreto de la lista");
        listaCadenas.remove(unaCadena);

        System.out.println("------ Muestro los datos ------");
        for (String elemento: listaCadenas){
            System.out.println(elemento);
        }

        System.out.println("Eliminar todos los elementos de la lista");
        listaCadenas.clear();

        System.out.println("Comprobar si esta vacio");
        if (listaCadenas.isEmpty()){
            System.out.println("La lista no tiene elementos");
        }

        System.out.println("Añadir todos los elementos de otra lista");
        List<String> otraListaCadenas = new ArrayList<>();
        String otraNuevaCadena = "cadena";
        String otraNuevaCadena1 = "cadena1";
        String otraNuevaCadena2 = "cadena2";
        otraListaCadenas.add(otraNuevaCadena);
        otraListaCadenas.add(otraNuevaCadena1);
        otraListaCadenas.add(otraNuevaCadena2);

        System.out.println("Se puede pasar como aprametro un objero que implementa la intefaz Collection");
        listaCadenas.addAll(otraListaCadenas);

        System.out.println("------ Muestro los datos ------");
        for (String elemento: listaCadenas){
            System.out.println(elemento);
        }

        System.out.println("Obetener un array (estatico) con todos los elementos de la lista");
        Object[] arrayCadena = listaCadenas.toArray();
        for (int i = 0; i < arrayCadena.length; i++) {
            System.out.println(arrayCadena[i]);
        }



    }
}
