package com.javiergasca.EstructurasDatos;

import java.util.Collection;
import java.util.HashMap;
import java.util.Set;
import java.util.TreeMap;

public class EjercicioTreeMap {
    public static void main(String[] args) {
        System.out.println("*** Añadir un elemento (clave-valor) ***");
        TreeMap<String,Libro> libroTreeMap = new TreeMap<String, Libro>();

        System.out.println("*** Añadimos dos libros ***");
        Libro libro = new Libro();
        libro.setTitulo("Secuestrado");
        libro.setAutor("Pepito");

        libroTreeMap.put(libro.getTitulo(), libro);

        Libro libro2 = new Libro();
        libro2.setTitulo("Coche del infierno");
        libro2.setAutor("Roberto");

        libroTreeMap.put(libro2.getTitulo(), libro2);

        System.out.println("*** Mostramos los datos ***");
        Collection<Libro> collectionLibros = libroTreeMap.values();

        for (Libro lib : collectionLibros) {
            System.out.println(lib.getTitulo() + "-" + lib.getAutor());
        }

        System.out.println("*** Obetenemos un elemento ***");
        String tituloLibro = "Secuestrado";
        libroTreeMap.get(tituloLibro);

        System.out.println("*** Comprobar si una clave existe ***");
        String titulo = "Secuestrado";
        if (libroTreeMap.containsKey(titulo)){
            System.out.println("El libro existe en la coleccion");
        } else System.out.println("No existe el liborn");

        System.out.println("*** Obtener un set con todas las claves ***");
        Set<String> titulos = libroTreeMap.keySet();

        System.out.println("*** Mostramos los elementos (titulos)");
        for (Object elemento : titulos) {
            System.out.println(elemento + " ");
        }

        System.out.println("*** Comprobar el tamaño del map ***");
        System.out.println("Tienes " + libroTreeMap.size() + " libros en tu colección");

        System.out.println("*** Concatenamos lso elementos de otro map ***");
        TreeMap<String, Libro> masLibros = new TreeMap<String, Libro>();

        Libro libro3 = new Libro();
        libro3.setAutor("Javier");
        libro3.setTitulo("Las sombras del infierno");

        masLibros.put(libro3.getTitulo(),libro3);

        libroTreeMap.putAll(masLibros);

        System.out.println("*** Eliminar un elemento (clave) ***");
        titulo  = "Secuestrado";
        libroTreeMap.remove(titulo);

        System.out.println("*** Mostramos los datos ***");
        for (Libro lib : collectionLibros) {
            System.out.println(lib.getTitulo() + "-" + lib.getAutor());
        }

        System.out.println("*** Vaciamos ***");
        libroTreeMap.clear();

        System.out.println("*** Comprobar si está vacio ***");
        if (libroTreeMap.isEmpty()){
            System.out.println("Tu colección esta vacia ");
        }


    }
}
