package com.javiergasca.EstructurasDatos;

import com.sun.xml.internal.ws.api.model.wsdl.WSDLOutput;
import org.w3c.dom.ls.LSOutput;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

public class EjercicioTreeSet {
    public static void main(String[] args) {
        System.out.println("*** Creamos 4 productos y añadimos el treeSet");

        String producto1 = "Pan";
        String producto2 = "Manzanas";
        String producto3 = "Brocoli";
        String producto4 = "Carne";

        //Definimos el TreeSet
        Set<String> listaTreeSet = new TreeSet<>();
        listaTreeSet.add(producto1);
        listaTreeSet.add(producto2);
        listaTreeSet.add(producto3);
        listaTreeSet.add(producto4);

        System.out.println("*** Mostramos los elementos ***");
        for (Object elemento :
                listaTreeSet) {
            System.out.println(elemento + " ");
        }
        System.out.println();

        System.out.println("*** Añadimos un elemento ***");
        listaTreeSet.add("Lechuga");

        System.out.println("*** Mostramos los elementos ***");
        for (Object elemento :
                listaTreeSet) {
            System.out.println(elemento + " ");
        }
        System.out.println();

        System.out.println("*** Añadimos los elementos de otra colección ***");
        List<String> listaArraylist = new ArrayList<>();
        listaArraylist.add("Lentejas");
        listaArraylist.add("Garbanzos");
        listaArraylist.add("Judias");

        listaTreeSet.addAll(listaArraylist);

        System.out.println("*** Mostramos los elementos ***");
        for (Object elemento :
                listaTreeSet) {
            System.out.println(elemento + " ");
        }
        System.out.println();

        System.out.println("*** Comprobamos is existe un elemento en la lista ***");
        String texto = "Lentejas";
        if (listaTreeSet.contains(texto)){
            System.out.println("El elemento " + texto + " existe");
        }

        System.out.println("*** Mostramos el primer elemento ***");
        System.out.println(((TreeSet<String>) listaTreeSet).first() );

        ((TreeSet<String>) listaTreeSet).pollFirst();

        System.out.println("*** Numero de elementos ***");
        System.out.println("Tienes " + listaTreeSet.size() + " elementos");

        System.out.println("*** Obetener la parte de un set cuyos valores son menores ***");
        String cadena = "carne";
        System.out.println(((TreeSet<String>) listaTreeSet).headSet("Judias"));

        System.out.println("*** Obtener un set entre dos valores ***");
        String cadenaMenor = "Pan";
        String cadenaMayor = "Lechuga";
        System.out.println(((TreeSet<String>) listaTreeSet).subSet(cadenaMayor,cadenaMenor));

        System.out.println("*** Vaciamos ***");
        listaTreeSet.clear();

        System.out.println("*** Comprobar si está vacio ***");
        if ( listaTreeSet.isEmpty()){
            System.out.println("Tu colección esta vacia ");
        }



    }
}
