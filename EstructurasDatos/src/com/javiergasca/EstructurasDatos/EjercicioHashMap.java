package com.javiergasca.EstructurasDatos;

import java.util.*;

public class EjercicioHashMap {
    public static void main(String[] args) {
        HashMap<String,Libro> mapLibros = new HashMap<String, Libro>();
        System.out.println("Insertar un dato");
        Libro libro = new Libro();
        libro.setTitulo("Secuestrado");
        libro.setAutor("Robert Louis");
        mapLibros.put(libro.getTitulo(),libro);

        System.out.println("Mostramos un valor dado el titulo");
        String tituloLibro = "Secuestrado";
        System.out.println(mapLibros.get(tituloLibro));

        System.out.println("Insertamos dos datos mas ");
        Libro libro1 = new Libro("El tiempo en paris", "Perico");
        mapLibros.put(libro1.getTitulo(),libro1);

        Libro libro2 = new Libro("Don quijote", "josecho");
        mapLibros.put(libro2.getTitulo(), libro2);

        System.out.println("*** Obetener una coleccion con todos los valores ***");
        System.out.println("Mostramos los valores");
        Collection<Libro> coleccionLibros = mapLibros.values();
        for (Libro elemento: coleccionLibros) {
            System.out.println(elemento + " ");
        }
        System.out.println();

        System.out.println("Comprobamos si existe una clave");
        String titulo = "Secuestrado";
        if (mapLibros.containsKey(titulo)){
            System.out.println("El libor con el titulo " + titulo + " existe en la coleccion");
        }

        System.out.println("Obtener un set con todas las claves");
        Set<String> titulos  = mapLibros.keySet();

        System.out.println("Comprobamos el tamaño del map");
        System.out.println("Tienes " + mapLibros.size() + " libros en tu coleccion");

        System.out.println("Concatenar todos los elementos de otro map");
        Map<String,Libro> maslibros = new HashMap<String, Libro>();
        Libro maslibros1 = new Libro("La montaña magica", "Tomas");
        maslibros.put(libro.getTitulo(),maslibros1);
        Libro maslibros2 = new Libro("Oso2", "Tomaso");
        maslibros.put(libro.getTitulo(),maslibros2);
        Libro maslibros3 = new Libro("Chill chill", "Roberto");
        maslibros.put(libro.getTitulo(),maslibros3);


        //Cambio los datos de mapLibros correspondiente a libro
        mapLibros.putAll(maslibros);

        System.out.println("Mostramos los valores");
        for (Libro elemento: coleccionLibros) {
            System.out.println(elemento + " ");
        }

        System.out.println("Eliminar un elemento por clave");
        titulo = "Secuestrado";
        mapLibros.remove(titulo);

        System.out.println("Mostramos los valores");
        for (Libro elemento: coleccionLibros) {
            System.out.println(elemento + " ");
        }


        System.out.println("Eliminar todos los elementos");
        mapLibros.clear();
        System.out.println("Comprobar si esta vacio");
        if (mapLibros.isEmpty()){
            System.out.println("La colección de libros esta vacia");
        }
    }
}
