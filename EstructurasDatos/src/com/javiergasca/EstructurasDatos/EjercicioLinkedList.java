package com.javiergasca.EstructurasDatos;

import javax.swing.text.html.HTMLDocument;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

public class EjercicioLinkedList {
    public static void main(String[] args) {
        List<Libro> listaLinkedLibros = new LinkedList<>();
        Libro libro = new Libro();
        libro.setTitulo("Secuestrado");
        libro.setAutor("Perico");

        listaLinkedLibros.add(libro);

        System.out.println("Mostramos elementos");
        Iterator it = listaLinkedLibros.iterator();
        while (it.hasNext()){
            System.out.println(it.next().toString());
        }

        System.out.println("*** Añadir un elemento al principio ***");
        Libro libro1 = new Libro();
        libro1.setTitulo("La montaña magica");
        libro1.setAutor("Adolfo");

        ((LinkedList<Libro>)listaLinkedLibros).addFirst(libro1);

        System.out.println("Mostramos elementos");
        Iterator it1 = listaLinkedLibros.iterator();
        while (it1.hasNext()){
            System.out.println(it1.next().toString());
        }

        Libro libro2 = new Libro();
        libro2.setTitulo("La torre");
        libro2.setAutor("Javier");

        ((LinkedList<Libro>) listaLinkedLibros).addLast(libro2);

        System.out.println("Mostramos elementos");
        Iterator it2 = listaLinkedLibros.iterator();
        while (it2.hasNext()){
            System.out.println(it2.next().toString());
        }

        System.out.println("Añadir toda la colección al final");
        List<Libro> otraListaDeLibros = new ArrayList<>();

        Libro libro3 = new Libro();
        libro3.setTitulo("La torre2");
        libro3.setAutor("Perico palotes");
        otraListaDeLibros.add(libro3);

        Libro libro4 = new Libro();
        libro4.setTitulo("La 52");
        libro4.setAutor("Fernando");
        otraListaDeLibros.add(libro4);

        listaLinkedLibros.addAll(otraListaDeLibros);

        System.out.println("Mostramos elementos");
        Iterator it3 = listaLinkedLibros.iterator();
        while (it3.hasNext()){
            System.out.println(it3.next().toString());
        }

        System.out.println("Obetener un elemento");
        System.out.println(listaLinkedLibros.get(4));

        System.out.println("Obtener el primer elemento");
        System.out.println(((LinkedList<Libro>) listaLinkedLibros).getFirst());

        System.out.println("Mostrar longitud");
        System.out.println("Esta lista tiene " + listaLinkedLibros.size() + " elementos");

        System.out.println("Elimiar y obtener un elemento");
        System.out.println(listaLinkedLibros.remove(3));

        System.out.println("Eliminar y obetener primer elemento");
        System.out.println(((LinkedList<Libro>) listaLinkedLibros).removeFirst());

        System.out.println("Eliminar y obtener ultimo elemento");
        System.out.println(((LinkedList<Libro>) listaLinkedLibros).removeLast());

        System.out.println("ELiminar todos los elementos");
        listaLinkedLibros.clear();

    }

}
