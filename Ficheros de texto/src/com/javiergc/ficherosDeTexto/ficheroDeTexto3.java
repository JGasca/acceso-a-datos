package com.javiergc.ficherosDeTexto;

import javax.xml.transform.sax.SAXSource;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

public class ficheroDeTexto3 {
    public static void main(String[] args) throws IOException {
        FileWriter writer = new FileWriter("fichero3.txt", true);
        writer.write("traspaso de datos\n");
        writer.write("traspaso de datos\n");
        writer.write("traspaso de datos\n");
        writer.write("traspaso de datos\n");
        writer.close();
        //Leer fichero de texto plano
        File fichero = new File("fichero3.txt");
        Scanner lector = null;
        lector = new Scanner(fichero);
        while (lector.hasNextLine()) {
            System.out.println(lector.nextLine());
        }
        lector.close();
    }
}
