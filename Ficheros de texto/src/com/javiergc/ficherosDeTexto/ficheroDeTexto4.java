package com.javiergc.ficherosDeTexto;

import java.io.*;

public class ficheroDeTexto4 {
    public static void main(String[] args) throws IOException {
        //Con BufferedWriter / reader
        //Escritura de fichero
        FileWriter fw = new FileWriter("fichero4.txt");
        BufferedWriter writer = new BufferedWriter(fw);
        writer.write("Texto para escribir\n");
        writer.close();

        //Lectura del fichero
        FileReader fr = new FileReader("fichero4.txt");
        BufferedReader reader = new BufferedReader(fr);
        String linea;
        while ((linea = reader.readLine()) != null){
            System.out.println(linea);
        }
        reader.close();
    }
}
