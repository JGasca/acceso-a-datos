package com.javiergc.ficherosDeTexto;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

public class FicheroDeTexto2<lector> {
    public static void main(String[] args) throws IOException {
        //Escribir con FileWriter
        FileWriter writer = new FileWriter("fichero2.txt");
        writer.write("Traspasamos los datos al fichero \nEsto es todo normal");


        //Leer fichero de texto plano
        File fichero = new File("fichero1.txt");
        Scanner lector = null;

        lector =new Scanner(fichero);
        while (lector.hasNextLine()){
            System.out.println(lector.nextLine());
        }
    }

}
