package com.javiergc.ficherosDeTexto;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.Scanner;

public class ficheroDeTexto1 {
    public static void main(String[] args) {
        //Escribir ficheros de texto plano
        PrintWriter escritor = null;

        try {
            escritor = new PrintWriter("fichero1.txt");
            escritor.println("Hola me llamo Javier Gasca Carilla \nEsto es una prueba de escritura y lectura de un fichero txt");
            escritor.close();

            //Leer ficheros de texto plano
            File fichero = new File("fichero1.txt");
            Scanner lector = null;


            lector = new Scanner(fichero);
            while (lector.hasNextLine()){
                System.out.println(lector.nextLine());
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

}
