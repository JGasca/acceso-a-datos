package com.javiergc.propiedadesdelsistema;

        import java.io.File;

public class PropiedadesSitema {

    public static void main(String[] args) {
        System.out.println("Caracter separador de rutas");
        System.out.println(File.separator);
        //ALT+INTRO para importar la clase

        System.out.println("Carpeta personal del usuario");
        System.out.println(System.getProperty("user.home"));
        System.out.println("Ruta en la que se encuentra elsusuario");
        System.out.println(System.getProperty("user.dir"));

    }
}
