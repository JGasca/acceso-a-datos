package com.clases.ejerciciosbasedatos;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public class TrabajandoConDatos {
	private Connection conexion = null;
	PreparedStatement sentencia = null;
	
	public void conectar() throws SQLException {
		String servidor = "jdbc:mysql://localhost:3306/";
		String bbdd = "videojuegos";
		String user = "root";
		String password = "";
		
		conexion = DriverManager.getConnection(servidor+bbdd,user,password);
	}
	
	public void seleccionar() {
		String sentenciaSql ="SELECT * FROM videojuegos";
		try {
			sentencia =  conexion.prepareStatement(sentenciaSql);
			ResultSet resultao = sentencia.executeQuery();
			
			//mostramos los datos
			while(resultao.next()) {
				System.out.println(resultao.getString(1) + "," + resultao.getString(2)  + "," + resultao.getString(3)  + "," + resultao.getString(4) + "," + resultao.getString(5));
			}
		} catch (SQLException e) {
			System.out.println("Error al ejecutar la sentencia");
			e.printStackTrace();
		}
		
	}
	
	public void insertar(String nombre, String plataforma, String genero, float precio) throws SQLException {
		String sentenciaSql = "INSERT INTO videojuegos(nombre, plataforma, genero, precio)" + "VALUES(?,?,?,?);";
		sentencia = conexion.prepareStatement(sentenciaSql);
		sentencia.setString(1, nombre);
		sentencia.setString(2, plataforma);
		sentencia.setString(3, genero);
		sentencia.setFloat(4, precio);
		sentencia.executeUpdate();
	}
	
	public void actualizar(String nombre, String plataforma, String genero, float precio) throws SQLException {
		String sentenciaSql = "UPDATE videojuegos SET plataforma=?, genero=?, precio=? WHERE nombre=?;";
		sentencia = conexion.prepareStatement(sentenciaSql);
		sentencia.setString(1, plataforma);
		sentencia.setString(2, genero);
		sentencia.setFloat(3, precio);
		sentencia.setString(4, nombre);
		sentencia.executeUpdate();
	}
	
	public void eliminar(String nombre) throws SQLException {
		String sentenciaSql = "DELETE FROM videojuegos WHERE nombre=?";
		sentencia = conexion.prepareStatement(sentenciaSql);
		sentencia.setString(1, nombre);
		sentencia.executeUpdate();
	}
	
	public void desconectar() {
		try {
			sentencia.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
