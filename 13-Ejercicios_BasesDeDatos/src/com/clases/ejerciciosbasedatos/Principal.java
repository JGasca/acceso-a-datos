package com.clases.ejerciciosbasedatos;

import java.sql.SQLException;
import java.util.Scanner;

public class Principal {

	public static void main(String[] args) throws SQLException {
		TrabajandoConDatos misDatos = new TrabajandoConDatos();
		Scanner in = new Scanner(System.in);
		System.out.println("Conectamos");
		try {
			misDatos.conectar();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		System.out.println("Seleccionamos datos");
		misDatos.seleccionar();
		
		System.out.println("Insertamos datos");
		System.out.println("Dame el nombre del videojuego");
		String nombre = in.nextLine();
		System.out.println("Dame la plataforma");
		String plataforma = in.nextLine();
		System.out.println("Dame le genero");
		String genero = in.nextLine();
		System.out.println("Dame el precio");
		Float precio = in.nextFloat();
		misDatos.insertar(nombre, plataforma, genero, precio);
		in.nextLine();
		
		System.out.println("Actualizamos datos ");
		System.out.println("Dame el nombre del videojuego");
		nombre = in.nextLine();
		System.out.println("Dame la plataforma");
		plataforma = in.nextLine();
		System.out.println("Dame le genero");
		genero = in.nextLine();
		System.out.println("Dame el precio");
		precio = in.nextFloat();
		misDatos.actualizar(nombre, plataforma, genero, precio);
		
		
		misDatos.seleccionar();
		in.nextLine();
		
		System.out.println("Eliminar un registro");
		System.out.println("Dame el nombre");
		nombre = in.nextLine();
		misDatos.eliminar(nombre);
		
		misDatos.desconectar();

	}

}
