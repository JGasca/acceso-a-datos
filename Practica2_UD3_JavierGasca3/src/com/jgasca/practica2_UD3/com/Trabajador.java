package com.jgasca.practica2_UD3.com;

import javax.persistence.*;
import java.util.Objects;

/**
 * Clase Trabajador
 */
@Entity
public class Trabajador {
    private int id;
    private String nombre;
    private String apellidos;
    private String telefono;
    private Tienda trabajador;

    /**
     * Metodo que devuelve el id del trabajador
     * @return id del trabajador
     */
    @Id
    @Column(name = "id")
    public int getId() {
        return id;
    }

    /**
     * Metodo que establece el id del trabajador
     * @param id id del trabajador
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * Metodo que devuelve el nombre del trabajador
     * @return Nombre del trabajador
     */
    @Basic
    @Column(name = "nombre")
    public String getNombre() {
        return nombre;
    }

    /**
     * Metodo que establece el nombre del trabajador
     * @param nombre Nombre del trabajador
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    /**
     * Meotodo que devuelve los apellidos del trabajador
     * @return Apellidos del trabajador
     */
    @Basic
    @Column(name = "apellidos")
    public String getApellidos() {
        return apellidos;
    }

    /**
     * Metodo que establece los apellidos del trabajador
     * @param apellidos Apellidos del trabajador
     */
    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    /**
     * Metodo que devuelve el telefono del trabajador
     * @return Telefono del trabajador
     */
    @Basic
    @Column(name = "telefono")
    public String getTelefono() {
        return telefono;
    }

    /**
     * Metodo que establece el telefono del trabajador
     * @param telefono Telefono del trabajador
     */
    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    /**
     * Metodo que compara dos Objetos de la clase Trabajador
     * @param o Objeto de la clase trabajador
     * @return true o false
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Trabajador that = (Trabajador) o;
        return id == that.id &&
                Objects.equals(nombre, that.nombre) &&
                Objects.equals(apellidos, that.apellidos) &&
                Objects.equals(telefono, that.telefono);
    }

    /**
     * Metodo hasCode
     * @return hash
     */
    @Override
    public int hashCode() {
        return Objects.hash(id, nombre, apellidos, telefono);
    }

    /**
     * Metodo que devuelve la tienda del trabajador
     * @return Objeto de la clase trabajador
     */
    @ManyToOne
    @JoinColumn(name = "id_tienda", referencedColumnName = "id", nullable = false)
    public Tienda getTrabajador() {
        return trabajador;
    }

    /**
     * Metodo que establece la tienda del trabajador
     * @param trabajador Tienda del trabajador
     */
    public void setTrabajador(Tienda trabajador) {
        this.trabajador = trabajador;
    }

    /**
     * Metodo que pasa a String los atributos de la clase
     * @return String con los atributos de la clase
     */
    @Override
    public String toString() {
        return nombre + "-" + apellidos + "-" + telefono + "-" + trabajador.getCodigo();
    }
}
