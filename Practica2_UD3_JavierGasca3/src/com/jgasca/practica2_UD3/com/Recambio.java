package com.jgasca.practica2_UD3.com;

import javax.persistence.*;
import java.sql.Date;
import java.util.Objects;

/**
 * Clase Recambio
 */
@Entity
public class Recambio {
    private int id;
    private String nombre;
    private String cantidad;
    private double precio;
    private Date fecha;
    private Tienda tienda;

    /**
     * Metodo que devuelve el id del recambio
     * @return id del recambio
     */
    @Id
    @Column(name = "id")
    public int getId() {
        return id;
    }

    /**
     * Metodo que establece el id del recambio
     * @param id id del recambio
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * Metodo que devuelve el nombre del recambio
     * @return Nombre del recambio
     */
    @Basic
    @Column(name = "nombre")
    public String getNombre() {
        return nombre;
    }

    /**
     * Metodo que establece el nombre del recambio
     * @param nombre Nombre del recambio
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    /**
     * Metodo que devuelve la cantidad del recambio
     * @return Cantidad del recambio
     */
    @Basic
    @Column(name = "cantidad")
    public String getCantidad() {
        return cantidad;
    }

    /**
     * Meotodo que etsablece la cantidad de recambios
     * @param cantidad Cantidad de recambios
     */
    public void setCantidad(String cantidad) {
        this.cantidad = cantidad;
    }

    /**
     * Metodo que devuelve el preciodel recambio
     * @return Precio del recambio
     */
    @Basic
    @Column(name = "precio")
    public double getPrecio() {
        return precio;
    }

    /**
     * Metodo que establece el precio del recambio
     * @param precio Precio del recambio
     */
    public void setPrecio(double precio) {
        this.precio = precio;
    }

    /**
     * Metodo que devuelve la fecha del recambio
     * @return Fecha del recambio
     */
    @Basic
    @Column(name = "fecha")
    public Date getFecha() {
        return fecha;
    }

    /**
     * Metodo que establece la fecha del recambio
     * @param fecha fecha del recambio
     */
    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    /**
     * Metodo que compara dos objetos de la clase recambio
     * @param o Objeto de la clase Recambio
     * @return true o false
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Recambio recambio = (Recambio) o;
        return id == recambio.id &&
                Double.compare(recambio.precio, precio) == 0 &&
                Objects.equals(nombre, recambio.nombre) &&
                Objects.equals(cantidad, recambio.cantidad) &&
                Objects.equals(fecha, recambio.fecha);
    }

    /**
     * hasCode
     * @return hash
     */
    @Override
    public int hashCode() {
        return Objects.hash(id, nombre, cantidad, precio, fecha);
    }

    /**
     * Metodo que devuelve la tienda a la que pertenece el recambio
     * @return Tienda a la que pertenece el recambio
     */
    @ManyToOne
    @JoinColumn(name = "id_tienda", referencedColumnName = "id", nullable = false)
    public Tienda getTienda() {
        return tienda;
    }

    /**
     * Metodo que establece una tienda a la que pertenece un recambio
     * @param tienda Objeto de la clase tienda
     */
    public void setTienda(Tienda tienda) {
        this.tienda = tienda;
    }

    /**
     * Metodo que paso los atributos a String y lo devuelve en una cadena
     * @return Los atributos en una String
     */
    @Override
    public String toString() {
        return nombre + "-" + cantidad + "-" + precio + "€-" + fecha;
    }
}
