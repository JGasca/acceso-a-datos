package com.jgasca.practica2_UD3.com;

import javax.persistence.*;
import java.util.Objects;

/**
 * clase Encargado
 */
@Entity
public class Encargado {
    private int id;
    private String nombre;
    private String apellidos;
    private String telefono;
    private Tienda tienda;

    /**
     * Metodo que devuelve el id del encargado
     * @return id del encargado
     */
    @Id
    @Column(name = "id")
    public int getId() {
        return id;
    }

    /**
     * Metodo que establece el id del encargado
     * @param id id del encargado
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * Metodo que devuelve el nombre del encargado
     * @return Nombre del encargado
     */
    @Basic
    @Column(name = "nombre")
    public String getNombre() {
        return nombre;
    }

    /**
     * Metodo que establece el nombre del encargado
     * @param nombre Nombre del encargado
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    /**
     * Metodo que devuelve los apellidos del encargado
     * @return Apellidos de los encargados
     */
    @Basic
    @Column(name = "apellidos")
    public String getApellidos() {
        return apellidos;
    }

    /**
     * Metodo que establece los apellidos del encargado
     * @param apellidos Apellidos del encargado
     */
    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    /**
     * Metodo que devuelve el telefono del encargado
     * @return Telefono del encargado
     */
    @Basic
    @Column(name = "telefono")
    public String getTelefono() {
        return telefono;
    }

    /**
     * Metodo que establece el telefono del encargado
     * @param telefono Telefono del encargado
     */
    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    /**
     * Metodo que compara dos encargados
     * @param o Objeto de la clase encargado
     * @return true o false
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Encargado encargado = (Encargado) o;
        return id == encargado.id &&
                Objects.equals(nombre, encargado.nombre) &&
                Objects.equals(apellidos, encargado.apellidos) &&
                Objects.equals(telefono, encargado.telefono);
    }

    /**
     * Metodo hasCode
     * @return hash
     */
    @Override
    public int hashCode() {
        return Objects.hash(id, nombre, apellidos, telefono);
    }

    /**
     * Metodo que devuelve la tienda a la que pertenece el encargado
     * @return Tienda a la que pertenece el encargado
     */
    @OneToOne(mappedBy = "encargado")
    public Tienda getTienda() {
        return tienda;
    }

    /**
     * Metodo que establece la tienda a la que pertence un encargado
     * @param tienda Objeto de la clase Tienda
     */
    public void setTienda(Tienda tienda) {
        this.tienda = tienda;
    }

    /**
     * Metodo que paso los atributos de la clase a un String
     * @return Un String con los atributos
     */
    @Override
    public String toString() {
        if (tienda != null){
            return nombre + "-" + apellidos + "-" + telefono + "-" + tienda.getCodigo();
        }else {
            return nombre + "-" + apellidos + "-" + telefono;
        }

    }
}
