package com.jgasca.practica2_UD3.com;

import javax.persistence.*;
import java.util.List;
import java.util.Objects;

@Entity
/**
 * CLase Tienda
 */
public class Tienda {
    private int id;
    private String codigo;
    private String direccion;
    private String telefono;
    private List<Recambio> recambio;
    private Encargado encargado;
    private List<Proveedor> proveedor;
    private List<Trabajador> tienda;

    @Id
    @Column(name = "id")
    /**
     * Metodo que devuelve el id
     */
    public int getId() {
        return id;
    }

    /**
     * Metodo que establece el id de  la tienda
     * @param id id de la tienda
     */
    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "codigo")
    /**
     * Metodo que devuelve el codigo de la tienda
     */
    public String getCodigo() {
        return codigo;
    }

    /**
     * Metodo que establece elcodigo de la tienda
     * @param codigo codigo de la tienda
     */
    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    @Basic
    @Column(name = "direccion")
    /**
     * Metodo que devuelve la direccion de la tienda
     */
    public String getDireccion() {
        return direccion;
    }

    /**
     * Metodo que establece la dirección de la tienda
     * @param direccion Dirección de la tienda
     */
    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    @Basic
    @Column(name = "telefono")
    /**
     * Metodo que devuelve el telefono de la tienda
     */
    public String getTelefono() {
        return telefono;
    }

    /**
     * Metodo que establece el teleofono dela tienda
     * @param telefono Telefono de la tienda
     */
    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    /**
     * Metodo que compara dos objetos de la calse Tienda
     * @param o Objeto de la clase Tienda
     * @return True o false
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Tienda tienda = (Tienda) o;
        return id == tienda.id &&
                Objects.equals(codigo, tienda.codigo) &&
                Objects.equals(direccion, tienda.direccion) &&
                Objects.equals(telefono, tienda.telefono);
    }

    /**
     * Metodo hasCode
     * @return Lista de objetos
     */
    @Override
    public int hashCode() {
        return Objects.hash(id, codigo, direccion, telefono);
    }

    /**
     * Meotodo que devuelve un List de recambios
     * @return list de recambios
     */
    @OneToMany(mappedBy = "tienda")
    public List<Recambio> getRecambio() {
        return recambio;
    }

    /**
     * Metodo que establece un list de recambios
     * @param recambio List de recambio
     */
    public void setRecambio(List<Recambio> recambio) {
        this.recambio = recambio;
    }

    /**
     * Metodo que devuelve un encargado
     * @return Objeto de la clase Encargado
     */
    @OneToOne
    @JoinColumn(name = "id_encargado", referencedColumnName = "id")
    public Encargado getEncargado() {
        return encargado;
    }

    /**
     * Metodo que establece un encargado
     * @param encargado objeto de la clase Encargado
     */
    public void setEncargado(Encargado encargado) {
        this.encargado = encargado;
    }

    /**
     * Metodo que devuelve un list de proveedores
     * @return List de proveedores
     */
    @ManyToMany(mappedBy = "tienda",fetch=FetchType.EAGER)
    public List<Proveedor> getProveedor() {
        return proveedor;
    }

    /**
     * Metodo que establece un list de proveedores
     * @param proveedor list de proveedores
     */
    public void setProveedor(List<Proveedor> proveedor) {
        this.proveedor = proveedor;
    }

    /**
     * Metodo que devuelve un list de tiendas
     * @return List de tiendas
     */
    @OneToMany(mappedBy = "trabajador")
    public List<Trabajador> getTienda() {
        return tienda;
    }

    /**
     * Metodo que establece un list de tiendas
     * @param tienda List de tiendas
     */
    public void setTienda(List<Trabajador> tienda) {
        this.tienda = tienda;
    }

    /**
     * Metodo que devuelve los atributos un forma de string en una cadena
     * @return Un string
     */
    @Override
    public String toString() {
        if (encargado != null){
            return codigo + "-" + direccion + "-" + telefono + "-" + encargado.getNombre() + " " + encargado.getApellidos();
        }else {
            return codigo + "-" + direccion + "-" + telefono;
        }
    }
}
