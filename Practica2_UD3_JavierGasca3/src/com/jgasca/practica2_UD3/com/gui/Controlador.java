package com.jgasca.practica2_UD3.com.gui;

import com.jgasca.practica2_UD3.com.*;
import org.omg.CORBA.portable.ValueInputStream;

import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

/**
 * Clase Controlador
 */
public class Controlador implements ActionListener, ListSelectionListener {
    private Vista vista;
    private Modelo modelo;

    /**
     * Construtor de la clase Controlador
     * @param vista Objeto de la clase Vista
     * @param modelo Objeto de la clase modelo
     */
    public Controlador(Vista vista, Modelo modelo) {
        this.vista = vista;
        this.modelo = modelo;

        addActionListeners(this);
        addListSelectionListener(this);
    }

    /**
     * Metodo que esta ala ecucha e los eventos de boton
     * @param listener Listener de la clase
     */
    public void addActionListeners(ActionListener listener){
        //Conectar
        vista.itemConectar.addActionListener(listener);
        //Boton Nuevo
        vista.btnNuevoRecambio.addActionListener(listener);
        vista.btnNuevoEncargado.addActionListener(listener);
        vista.btnNuevoTienda.addActionListener(listener);
        vista.btnNuevoTrabajador.addActionListener(listener);
        vista.btnNuevoProveedor.addActionListener(listener);
        //Boton Eliminar
        vista.btnEliminarRecambio.addActionListener(listener);
        vista.btnEliminarEncargado.addActionListener(listener);
        vista.btnEliminarTienda.addActionListener(listener);
        vista.btnEliminarTrabajador.addActionListener(listener);
        vista.btnEliminarProveedor.addActionListener(listener);
        //Boton Modificar
        vista.btnModificarRecambio.addActionListener(listener);
        vista.btnModificarEncargado.addActionListener(listener);
        vista.btnModificarTienda.addActionListener(listener);
        vista.btnModificarTrabajador.addActionListener(listener);
        vista.btnModificarProveedor.addActionListener(listener);
        //Boton Listar
        vista.btnListarTrabajadores.addActionListener(listener);
        vista.btnlistarTiendasDelProveedor.addActionListener(listener);
        //Boton asignar
        vista.btasignarTiendaProveedor.addActionListener(listener);
    }

    /**
     * Metodo que esta a la escuha  de los eventos en los Jlist
     * @param listener Listener de la clase
     */
    public void addListSelectionListener(ListSelectionListener listener){
        vista.listEncargado.addListSelectionListener(listener);
        vista.listRecambios.addListSelectionListener(listener);
        vista.listTienda.addListSelectionListener(listener);
        vista.listTrabajador.addListSelectionListener(listener);
        vista.listProveedor.addListSelectionListener(listener);
        vista.listTiendaProveedor.addListSelectionListener(listener);

    }

    /**
     * Metodo que ejecuta funciones en funcion del boton pulsado
     * @param e Objeto de la clase ActionEvent
     */
    @Override
    public void actionPerformed(ActionEvent e) {
        String comando = e.getActionCommand();

        switch (comando){
            case "Conectar":
                modelo.conectar();
                vista.itemConectar.setEnabled(false);
                break;
            case "NuevoRecambio":
                if (!vista.txtNombreRecambio.getText().equals("") && !vista.txtCantidadRecambio.getText().equals("") && !vista.txtPrecioRecambio.getText().equals("") && vista.comboTiendaRecambio.getSelectedItem() != null && isNumeric(vista.txtPrecioRecambio.getText()) && isNumeric(vista.txtCantidadRecambio.getText())){
                    Recambio recambio = new Recambio();
                    System.out.println("Nombre: " + vista.txtNombreRecambio.getText());
                    System.out.println("Cantidad: " + vista.txtCantidadRecambio.getText());
                    System.out.println("Precio: " + vista.txtPrecioRecambio.getText());

                    recambio.setNombre(vista.txtNombreRecambio.getText());
                    recambio.setCantidad(vista.txtCantidadRecambio.getText());
                    recambio.setPrecio(Double.parseDouble(vista.txtPrecioRecambio.getText()));
                    recambio.setTienda((Tienda) vista.comboTiendaRecambio.getSelectedItem());
                    modelo.altaRecambio(recambio);
                    resetRecambio();
                }else {
                    if (!isNumeric(vista.txtPrecioRecambio.getText()) || !isNumeric(vista.txtCantidadRecambio.getText())){
                        JOptionPane.showMessageDialog(null,"El precio o la cantidad no es numerico","No numerico",JOptionPane.WARNING_MESSAGE);
                    }else{
                        JOptionPane.showMessageDialog(null,"Algun campo esta vacio","Compos vacios",JOptionPane.WARNING_MESSAGE);
                    }
                }
                break;
            case "EliminarRecambio":
                if (vista.listRecambios.getSelectedValue() != null){
                    Recambio recambio2 = (Recambio) vista.listRecambios.getSelectedValue();
                    modelo.borrarRecambio(recambio2);
                    resetRecambio();
                }else {
                    JOptionPane.showMessageDialog(null,"No has seleccionado un recambio","Ninguna selección",JOptionPane.WARNING_MESSAGE);
                }

                break;
            case "ModificarRecambio":
                if (vista.listRecambios.getSelectedValue() != null){
                    Recambio recambio = (Recambio) vista.listRecambios.getSelectedValue();
                    recambio.setNombre(vista.txtNombreRecambio.getText());
                    recambio.setCantidad(vista.txtCantidadRecambio.getText());
                    recambio.setPrecio(Double.parseDouble(vista.txtPrecioRecambio.getText()));
                    recambio.setTienda((Tienda) vista.comboTiendaRecambio.getSelectedItem());
                    //Falta el asignarle la tienda
                    modelo.modificarRecambio(recambio);
                    resetRecambio();
                }else {
                    JOptionPane.showMessageDialog(null,"No has seleccionado ningun remcabio","Ninguna selección",JOptionPane.WARNING_MESSAGE);
                }

                break;
            case "NuevoTienda":
                if (!vista.txtCodigoTienda.getText().equals("") && !vista.txtDireccionTienda.getText().equals("") && !vista.txtTelefonoTienda.getText().equals("") && vista.comboTienda.getSelectedItem() != null){
                    Tienda tienda = new Tienda();
                    tienda.setCodigo(vista.txtCodigoTienda.getText());
                    tienda.setDireccion(vista.txtDireccionTienda.getText());
                    tienda.setTelefono(vista.txtTelefonoTienda.getText());
                    System.out.println(vista.comboTienda.getSelectedItem() + " Encargado01");
                    tienda.setEncargado((Encargado) vista.comboTienda.getSelectedItem());
                    modelo.altaTienda(tienda);

                    for (Tienda tienda1:modelo.getTiendas()) {
                        System.out.println(tienda.getEncargado() + "     Encargado *****************************");
                    }
                    resetTienda();
                }else {
                    JOptionPane.showMessageDialog(null,"Algun campo esta vacio","Compos vacios",JOptionPane.WARNING_MESSAGE);
                }
                break;
            case "EliminarTienda":
                if (vista.listTienda.getSelectedValue() != null){
                    Tienda Tienda2 = (Tienda) vista.listTienda.getSelectedValue();
                    modelo.borrarTienda(Tienda2);
                    resetTienda();
                }else {
                    JOptionPane.showMessageDialog(null,"No has seleccionado una tienda","Ninguna selección",JOptionPane.WARNING_MESSAGE);
                }

                break;
            case "ModificarTienda":
                System.out.println("ENTRA EN MODIFICAR TIENDA");
                if (vista.listTienda.getSelectedValue() != null){
                    Tienda tienda = (Tienda)vista.listTienda.getSelectedValue();
                    tienda.setCodigo(vista.txtCodigoTienda.getText());
                    tienda.setDireccion(vista.txtDireccionTienda.getText());
                    tienda.setTelefono(vista.txtTelefonoTienda.getText());
                    modelo.modificarTienda(tienda);
                    resetTienda();
                }else {
                    JOptionPane.showMessageDialog(null,"No has seleccionado ninguna tienda","Ninguna selección",JOptionPane.WARNING_MESSAGE);
                }
                break;
            case "NuevoEncargado":
                if (!vista.txtNombreEncargado.getText().equals("") && !vista.txtApellidosEncargado.getText().equals("") && !vista.txtTelefonoEncargado.getText().equals("") ){
                    Encargado encargado = new Encargado();
                    encargado.setNombre(vista.txtNombreEncargado.getText());
                    encargado.setApellidos(vista.txtApellidosEncargado.getText());
                    encargado.setTelefono(vista.txtTelefonoEncargado.getText());
                    modelo.altaEncargado(encargado);
                    //Encargado encargado2 = modelo.getEncargado(encargado);
                    //System.out.println(encargado2.getId() + "  Id del encargado");
                    //tiendaAsignar.setEncargado(encargado2);
                    //modelo.modificarTienda(tiendaAsignar);
                    resetEncargado();
                }else {
                    JOptionPane.showMessageDialog(null,"Algun campo esta vacio","Compos vacios",JOptionPane.WARNING_MESSAGE);
                }

                break;
            case "EliminarEncargado":
                if (vista.listEncargado.getSelectedValue() != null){
                    Encargado encargado2 = (Encargado) vista.listEncargado.getSelectedValue();
                    modelo.borrarEncargado(encargado2);
                    resetEncargado();
                }else {
                    JOptionPane.showMessageDialog(null,"No has seleccionado un encargado","Ninguna selección",JOptionPane.WARNING_MESSAGE);
                }

                break;
            case "ModificarEncargado":
                if (vista.listEncargado.getSelectedValue() != null){
                    Encargado encargado = (Encargado)vista.listEncargado.getSelectedValue();
                    encargado.setNombre(vista.txtNombreEncargado.getText());
                    encargado.setApellidos(vista.txtApellidosEncargado.getText());
                    encargado.setTelefono(vista.txtTelefonoEncargado.getText());
                    //Falta el asignarle la tienda
                    modelo.modificarEncargado(encargado);
                    resetEncargado();
                }else {
                    JOptionPane.showMessageDialog(null,"No has seleccionado ningun encargado","Ninguna selección",JOptionPane.WARNING_MESSAGE);
                }
                break;
            case "NuevoTrabajador":
                if (!vista.txtNombreTrabajador.getText().equals("") && !vista.txtApellidosTrabajador.getText().equals("") && !vista.txtTelefonoTrabajador.getText().equals("") && vista.comboTrabajadorTienda.getSelectedItem() != null){
                    Trabajador trabajador = new Trabajador();
                    trabajador.setNombre(vista.txtNombreTrabajador.getText());
                    trabajador.setApellidos(vista.txtApellidosTrabajador.getText());
                    trabajador.setTelefono(vista.txtTelefonoTrabajador.getText());
                    trabajador.setTrabajador((Tienda) vista.comboTrabajadorTienda.getSelectedItem());
                    modelo.altaTrabajador(trabajador);
                    resetTrabajador();
                }else {
                    JOptionPane.showMessageDialog(null,"Algun campo esta vacio","Compos vacios",JOptionPane.WARNING_MESSAGE);
                }

                break;
            case "EliminarTrabajador":
                if (vista.listTrabajador.getSelectedValue() != null){
                    Trabajador trabajador2 = (Trabajador) vista.listTrabajador.getSelectedValue();
                    modelo.borrarTrabajador(trabajador2);
                    resetTrabajador();
                }else {
                    JOptionPane.showMessageDialog(null,"No has seleccionado un trabajador","Ninguna selección",JOptionPane.WARNING_MESSAGE);
                }

                break;
            case "ModificarTrabajador":
                if (vista.listTrabajador.getSelectedValue() != null){
                    Trabajador trabajador = (Trabajador)vista.listTrabajador.getSelectedValue();
                    trabajador.setNombre(vista.txtNombreTrabajador.getText());
                    trabajador.setApellidos(vista.txtApellidosTrabajador.getText());
                    trabajador.setTelefono(vista.txtTelefonoTrabajador.getText());
                    modelo.modificarTrabajador(trabajador);
                    resetTrabajador();
                }else {
                    JOptionPane.showMessageDialog(null,"No has seleccionado ningun trabajador","Ninguna selección",JOptionPane.WARNING_MESSAGE);
                }
                break;
            case "NuevoProveedor":
                if (!vista.txtNombreProveedor.getText().equals("") && !vista.txtDireccionProveedor.getText().equals("") && !vista.txtTelefonoProveedor.getText().equals("")){
                    Proveedor proveedor = new Proveedor();
                    proveedor.setNombre(vista.txtNombreProveedor.getText());
                    proveedor.setDireccion(vista.txtDireccionProveedor.getText());
                    proveedor.setTelefono(vista.txtTelefonoProveedor.getText());
                    modelo.altaProveedor(proveedor);
                    resetProveedor();
                }else {
                    JOptionPane.showMessageDialog(null,"Algun campo esta vacio","Compos vacios",JOptionPane.WARNING_MESSAGE);
                }
                break;
            case "EliminarProveedor":
                if (vista.listProveedor.getSelectedValue() != null){
                    Proveedor proveedor2 = (Proveedor) vista.listProveedor.getSelectedValue();
                    modelo.borrarProveedor(proveedor2);
                    resetProveedor();
                }else {
                    JOptionPane.showMessageDialog(null,"No has seleccionado un trabajador","Ninguna selección",JOptionPane.WARNING_MESSAGE);
                }
                break;

            case "ModificarProveedor":
                if (vista.listProveedor.getSelectedValue() != null){
                    Proveedor proveedor = (Proveedor) vista.listProveedor.getSelectedValue();
                    proveedor.setNombre(vista.txtNombreProveedor.getText());
                    proveedor.setDireccion(vista.txtDireccionProveedor.getText());
                    proveedor.setTelefono(vista.txtTelefonoProveedor.getText());
                    modelo.modificarProveedor(proveedor);
                    resetProveedor();
                }else {
                    JOptionPane.showMessageDialog(null,"No has seleccionado ningun trabajador","Ninguna selección",JOptionPane.WARNING_MESSAGE);
                }
                break;
            case "ListarTrabajadores":
                if (vista.listTienda.getSelectedValue() != null){
                    Tienda tienda = (Tienda) vista.listTienda.getSelectedValue();
                    listarTrabajadoresTienda(modelo.getTrabajadoresTienda(tienda));
                    resetTienda();
                }else {
                    JOptionPane.showMessageDialog(null,"No has seleccionado ninguna tienda","Ninguna selección",JOptionPane.WARNING_MESSAGE);
                }
                break;
            case "AsignarTiendaProveedor":
                if (vista.listTienda.getSelectedValue() != null && vista.listTiendaProveedor.getSelectedValue() != null){
                    Tienda tienda = (Tienda) vista.listTienda.getSelectedValue();
                    Proveedor proveedor = (Proveedor)vista.listTiendaProveedor.getSelectedValue();
                    tienda.getProveedor().add(proveedor);
                    proveedor.getTienda().add(tienda);
                    modelo.modificarProveedor(proveedor);
                    modelo.modificarTienda(tienda);
                    //tienda.getProveedor().add(proveedor);
                    //proveedor.getTienda().add(tienda);
                    vista.dlmTiendaProveedor.clear();
                    resetTienda();
                }else {
                    JOptionPane.showMessageDialog(null,"Algun campo te falta por seleccionar","Compos vacios",JOptionPane.WARNING_MESSAGE);
                }
                break;
            case "listarTiendasDelProveedor":
                if (vista.listProveedor.getSelectedValue() != null){
                    listarTiendasProveedor();
                }else{
                    JOptionPane.showMessageDialog(null,"No has seleccionado ningun proveedor","Ninguna selección",JOptionPane.WARNING_MESSAGE);
                }
                break;
        }
        rellenarCombos();
        listarEncargados(modelo.getEncargados());
        listarTiendas(modelo.getTiendas());
        listarRecambios(modelo.getRecambios());
        listarTrabajadores(modelo.getTrabajadores());
        listarProveedor(modelo.getProveedores());
    }

    /**
     * Metodo que lista las tiendas en el Jlist de tienda
     * @param lista Lista de tiendas en Arraylist
     */
    public void listarTiendas(ArrayList<Tienda> lista){
        vista.dlmTienda.clear();
        for(Tienda tienda : lista){
            vista.dlmTienda.addElement(tienda);
        }
    }

    /**
     * Metodo que lista los recambios en el Jlist de recambio
     * @param lista Lista de recambios en Arraylist
     */
    public void listarRecambios(ArrayList<Recambio> lista){
        vista.dlmRecambio.clear();
        for(Recambio recambio : lista){
            vista.dlmRecambio.addElement(recambio);
        }
    }

    /**
     * Metodo que lista las tiendas en el Jlist de tienda
     * @param lista Lista de tiendas en Arraylist
     */
    public void listarTrabajadores(ArrayList<Trabajador> lista){
        vista.dlmTrabajador.clear();
        for(Trabajador trabajador : lista){
            vista.dlmTrabajador.addElement(trabajador);
        }
    }

    /**
     * Metodo que lista los encargados en el Jlist de encargados
     * @param lista Lista de encargados en Arraylist
     */
    public void listarEncargados(ArrayList<Encargado> lista){
        vista.dlmEncaragado.clear();
        for(Encargado encargado : lista){
            vista.dlmEncaragado.addElement(encargado);
        }
    }

    /**
     * Metodo que lista las tiendas en el Jlist de trabajadoresTienda
     * @param lista Lista de tiendas en Arraylist
     */
    public void listarTrabajadoresTienda(ArrayList<Trabajador> lista){
        vista.dlmTrabajadorTienda.clear();
        for(Trabajador trabajador : lista){
            vista.dlmTrabajadorTienda.addElement(trabajador);
        }
    }

    /**
     * Metodo que lista los proveedores en el Jlist de preveedores
     * @param lista Lista de preveedores en Arraylist
     */
    public void listarProveedor(ArrayList<Proveedor> lista){
        vista.dlmProveedor.clear();
        for(Proveedor proveedor : lista){
            vista.dlmProveedor.addElement(proveedor);
        }
    }

    /**
     * Metodo que lista los proveedores que no estan ya asigandos a la tienda que esta seleccionada en el JList de tienda
     * @param listaTodos Lista de preveedores
     */
    public void listarProveedoresEnTienda(List<Proveedor> listaTodos){
        Tienda tienda = (Tienda) vista.listTienda.getSelectedValue();
        vista.dlmTiendaProveedor.clear();

        listaTodos.removeAll(tienda.getProveedor());

        for (Proveedor proveedor : listaTodos) {
            vista.dlmTiendaProveedor.addElement(proveedor);
        }
    }

    /**
     * Meotodo que lista las tiendas asociadasa un proveedor
     */
    public void listarTiendasProveedor(){
        Proveedor proveedor = (Proveedor) vista.listProveedor.getSelectedValue();
        vista.dlmTiendasProveedor.clear();
        for(Tienda tienda : proveedor.getTienda()){
            vista.dlmTiendasProveedor.addElement(tienda);
        }

    }

    /**
     * Metodo que rellena todo los comboBox de la palicación
     */
    public void rellenarCombos(){
        vista.comboTienda.removeAllItems();
        vista.comboTiendaRecambio.removeAllItems();
        vista.comboTrabajadorTienda.removeAllItems();
        modelo.setComboBoxEncargado(comboTienda(),vista.comboTienda);
        modelo.setComboBoxTienda(modelo.getTiendas(),vista.comboTrabajadorTienda);
        modelo.setComboBoxTienda(modelo.getTiendas(),vista.comboTiendaRecambio);

    }

    /**
     * Metodo que devuelve una lista de encargados que no tienen asignada una tienda
     * @return ArrayList de encargados
     */
    public ArrayList<Encargado> comboTienda(){
        ArrayList<Tienda> tiendas = modelo.getTiendasConEncargado();
        ArrayList<Encargado> encargados = modelo.getEncargados();
        ArrayList<Encargado> encargados1 = new ArrayList<>();

        for (Tienda tienda: tiendas) {
            encargados1.add(tienda.getEncargado());
        }
        encargados.removeAll(encargados1);
        return encargados;
    }

    /**
     * Metodo que pone en blanco los campos de tienda
     */
    public void resetTienda(){
        vista.txtCodigoTienda.setText("");
        vista.txtDireccionTienda.setText("");
        vista.txtTelefonoTienda.setText("");
    }

    /**
     * Metodo que pone en blanco los campos de trabajador
     */
    public void resetTrabajador(){
        vista.txtNombreTrabajador.setText("");
        vista.txtApellidosTrabajador.setText("");
        vista.txtTelefonoTrabajador.setText("");
    }

    /**
     * Meotodo que pone en blanco los campos de encargado
     */
    public void resetEncargado(){
        vista.txtNombreEncargado.setText("");
        vista.txtApellidosEncargado.setText("");
        vista.txtTelefonoEncargado.setText("");
    }

    /**
     * Metodo que pone en blanco los campos de recambio
     */
    public void resetRecambio(){
        vista.txtNombreRecambio.setText("");
        vista.txtCantidadRecambio.setText("");
        vista.txtPrecioRecambio.setText("");
    }

    /**
     * Meotodo que pone el blanco los campos de proveedor
     */
    public void resetProveedor(){
        vista.txtNombreProveedor.setText("");
        vista.txtDireccionProveedor.setText("");
        vista.txtTelefonoProveedor.setText("");
    }

    /**
     * Metodo que se ejecuta cuando hay un cambio en cualquier JList
     * @param e
     */
    @Override
    public void valueChanged(ListSelectionEvent e) {
        if(e.getValueIsAdjusting()){
            if(e.getSource() == vista.listTrabajador) {
                Trabajador trabajador = (Trabajador) vista.listTrabajador.getSelectedValue();
                vista.txtNombreTrabajador.setText(String.valueOf(trabajador.getNombre()));
                vista.txtApellidosTrabajador.setText(String.valueOf(trabajador.getTelefono()));
                vista.txtTelefonoTrabajador.setText(String.valueOf(trabajador.getTelefono()));
            }
            if(e.getSource() == vista.listTienda) {
                Tienda tienda = (Tienda) vista.listTienda.getSelectedValue();
                System.out.println("Estos son los proveedores de la tienda" + tienda.getProveedor());
                vista.txtCodigoTienda.setText(String.valueOf(tienda.getCodigo()));
                vista.txtDireccionTienda.setText(String.valueOf(tienda.getDireccion()));
                vista.txtTelefonoTienda.setText(String.valueOf(tienda.getTelefono()));
                listarProveedoresEnTienda(modelo.getProveedores());
            }
            if(e.getSource() == vista.listRecambios) {
                Recambio recambio = (Recambio) vista.listRecambios.getSelectedValue();
                vista.txtNombreRecambio.setText(String.valueOf(recambio.getNombre()));
                vista.txtCantidadRecambio.setText(String.valueOf(recambio.getCantidad()));
                vista.txtPrecioRecambio.setText(String.valueOf(recambio.getPrecio()));
                vista.comboTiendaRecambio.setSelectedItem(recambio.getTienda());
            }
            if(e.getSource() == vista.listEncargado) {
                Encargado encargado = (Encargado) vista.listEncargado.getSelectedValue();
                vista.txtNombreEncargado.setText(String.valueOf(encargado.getNombre()));
                vista.txtApellidosEncargado.setText(String.valueOf(encargado.getApellidos()));
                vista.txtTelefonoEncargado.setText(String.valueOf(encargado.getTelefono()));
            }
            if(e.getSource() == vista.listProveedor) {
                Proveedor proveedor = (Proveedor) vista.listProveedor.getSelectedValue();
                vista.txtNombreProveedor.setText(String.valueOf(proveedor.getNombre()));
                vista.txtDireccionProveedor.setText(String.valueOf(proveedor.getDireccion()));
                vista.txtTelefonoProveedor.setText(String.valueOf(proveedor.getTelefono()));

            }
        }
    }

    /**
     * Metodo que comprueba si una cadena solo contiene numeros
     * @param cadena String cadena a analizar
     * @return Boolean true o false
     */
    public boolean isNumeric(String cadena) {
        boolean resultado;
        try {
            Integer.parseInt(cadena);
            resultado = true;
        } catch (NumberFormatException excepcion) {
            try {
                Float.parseFloat(cadena);
                resultado = true;
            }catch (NumberFormatException excepcion2){
                resultado = false;
            }
        }
        return resultado;
    }
}
