package com.jgasca.practica2_UD3.com.gui;

/**
 * Clase Principal, ejecuta el programa
 */
public class Principal {
    /**
     * Metodo main que inicia la vista , el modelo y el controlador
     * @param args
     */
    public static void main(String[] args) {
        Vista vista = new Vista();
        Modelo modelo = new Modelo();
        Controlador controlador = new Controlador(vista,modelo);

    }
}
