package com.jgasca.practica2_UD3.com.gui;

import javax.swing.*;
import java.awt.*;

/**
 * Clase Vista
 */
public class Vista {
    JFrame frame;
    JPanel panel1;
    private JTabbedPane tabbedPane1;
    JTextField txtNombreTrabajador;
    JTextField txtNombreRecambio;
    JTextField txtCantidadRecambio;
    JTextField txtPrecioRecambio;
    JTextField txtCodigoTienda;
    JTextField txtDireccionTienda;
    JTextField txtTelefonoTienda;
    JTextField txtNombreEncargado;
    JTextField txtApellidosEncargado;
    JTextField txtTelefonoEncargado;
    JTextField txtApellidosTrabajador;
    JTextField txtTelefonoTrabajador;
    JTextField txtNombreProveedor;
    JTextField txtDireccionProveedor;
    JTextField txtTelefonoProveedor;
    JButton btnNuevoRecambio;
    JButton btnEliminarRecambio;
    JButton btnModificarRecambio;
    JButton btnNuevoTienda;
    JButton btnEliminarTienda;
    JButton btnModificarTienda;
    JButton btnNuevoEncargado;
    JButton btnEliminarEncargado;
    JButton btnModificarEncargado;
    JButton btnNuevoTrabajador;
    JButton btnEliminarTrabajador;
    JButton btnModificarTrabajador;
    JButton btnListarTrabajadores;
    JButton btnNuevoProveedor;
    JButton btnModificarProveedor;
    JButton btnEliminarProveedor;
    JButton btasignarTiendaProveedor;
    JButton btnlistarTiendasDelProveedor;
    JList listRecambios;
    JList listTienda;
    JList listEncargado;
    JList listTrabajador;
    JList listProveedor;
    JList listTrabajadoresTiendaSelec;
    JList listTiendaProveedor;
    JList listTiendasProveedor;
    JComboBox comboTienda;
    JComboBox comboTiendaRecambio;
    JComboBox comboTrabajadorTienda;


    DefaultListModel dlmRecambio;
    DefaultListModel dlmTienda;
    DefaultListModel dlmEncaragado;
    DefaultListModel dlmTrabajador;
    DefaultListModel dlmTrabajadorTienda;
    DefaultListModel dlmProveedor;
    DefaultListModel dlmTiendaProveedor;
    DefaultListModel dlmTiendasProveedor;

    JMenuItem itemConectar;
    JMenuItem itemSalir;

    /**
     * Constructor de la clase vista
     */
    public Vista(){
        frame = new JFrame("Vista");
        frame.setContentPane(panel1);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setSize(new Dimension(frame.getWidth(),frame.getHeight()+25));
        frame.setVisible(true);

        crearMenu();
        crearModelos();

        frame.setLocationRelativeTo(null);
    }

    /**
     * Metodo que crea el menu superior
     */
    private void crearMenu() {
        itemConectar = new JMenuItem("Conectar");
        itemConectar.setActionCommand("Conectar");
        //itemSalir = new JMenuItem("Salir");
        //itemSalir.setActionCommand("Salir");

        JMenu menuArchivo = new JMenu("Archivo");
        menuArchivo.add(itemConectar);
        //menuArchivo.add(itemSalir);
        JMenuBar barraMenu = new JMenuBar();
        barraMenu.add(menuArchivo);

        itemConectar.setEnabled(true);

        frame.setJMenuBar(barraMenu);
    }

    /**
     * Metodo que crear todos los DefaultListModel
     */
    private void crearModelos() {
        dlmEncaragado = new DefaultListModel();
        listEncargado.setModel(dlmEncaragado);

        dlmTienda = new DefaultListModel();
        listTienda.setModel(dlmTienda);

        dlmRecambio = new DefaultListModel();
        listRecambios.setModel(dlmRecambio);

        dlmTrabajador = new DefaultListModel();
        listTrabajador.setModel(dlmTrabajador);

        dlmTrabajadorTienda = new DefaultListModel();
        listTrabajadoresTiendaSelec.setModel(dlmTrabajadorTienda);

        dlmProveedor = new DefaultListModel();
        listProveedor.setModel(dlmProveedor);

        dlmTiendaProveedor = new DefaultListModel();
        listTiendaProveedor.setModel(dlmTiendaProveedor);

        dlmTiendasProveedor = new DefaultListModel();
        listTiendasProveedor.setModel(dlmTiendasProveedor);
    }

    /**
     * Metodo createUIComponents
     */
    private void createUIComponents() {
        // TODO: place custom component creation code here
    }
}
