package com.jgasca.practica2_UD3.com.gui;

import com.jgasca.practica2_UD3.com.*;
import com.sun.scenario.effect.impl.sw.sse.SSEBlend_SRC_OUTPeer;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.omg.CORBA.TIMEOUT;

import javax.persistence.Query;
import javax.swing.*;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 * Clase Modelo
 */
public class Modelo {
    SessionFactory sessionFactory;

    /**
     * Metodo que desconecta con la base de datos
     */
    public void desconectar(){
        if (sessionFactory != null && sessionFactory.isOpen()){
            sessionFactory.close();
        }
    }

    /**
     * Metodoq ue conecta con la base de datos
     */
    public void conectar(){
        Configuration configuracion =  new Configuration();
        configuracion.configure("hibernate.cfg.xml");
        //Tablas
        configuracion.addAnnotatedClass(Encargado.class);
        configuracion.addAnnotatedClass(Proveedor.class);
        configuracion.addAnnotatedClass(Recambio.class);
        configuracion.addAnnotatedClass(Tienda.class);
        configuracion.addAnnotatedClass(Trabajador.class);

        StandardServiceRegistry ssr = new StandardServiceRegistryBuilder().applySettings(configuracion.getProperties()).build();

        sessionFactory = configuracion.buildSessionFactory(ssr);
    }

    //************************ Altas *******************************
    /**
     * Metodo que da de alta un Trabajador
     * @param nuevoTrabajador Objeto de la clase Trabajador
     */
    public void altaTrabajador(Trabajador nuevoTrabajador){
        Session sesion = sessionFactory.openSession();

        sesion.beginTransaction();
        sesion.save(nuevoTrabajador);
        sesion.getTransaction().commit();

        sesion.clear();
    }

    /**
     * Metodo que da de alta un encargado
     * @param nuevoEncargado Objeto de la clase Encargado
     */
    public void altaEncargado(Encargado nuevoEncargado){
        Session sesion = sessionFactory.openSession();

        sesion.beginTransaction();
        sesion.save(nuevoEncargado);
        sesion.getTransaction().commit();

        sesion.clear();
    }

    /**
     * Metodo que da de alta un recambio
     * @param nuevoRecambio Objeto de la clase Recambio
     */
    public void altaRecambio(Recambio nuevoRecambio){
        Session sesion = sessionFactory.openSession();

        sesion.beginTransaction();
        sesion.save(nuevoRecambio);
        sesion.getTransaction().commit();

        sesion.clear();
    }

    /**
     * Metodo que da de alta un tienda
     * @param nuevaTienda Objeto de la clase Tienda
     */
    public void altaTienda(Tienda nuevaTienda){
        System.out.println(nuevaTienda.getEncargado().getId() + " Encargado02");
        Session sesion = sessionFactory.openSession();

        sesion.beginTransaction();
        sesion.save(nuevaTienda);
        sesion.getTransaction().commit();

        sesion.clear();
    }

    /**
     * Meotodo que da de alta un proveedor
     * @param proveedor Objeto de la clase Proveedor
     */
    public void altaProveedor(Proveedor proveedor){
        Session sesion = sessionFactory.openSession();

        sesion.beginTransaction();
        sesion.save(proveedor);
        sesion.getTransaction().commit();

        sesion.clear();
    }

    //*********************** Borrado *****************************

    /**
     * Metodo que borra una tienda
     * @param trabajador Objeto tienda a borrar
     */
    public void borrarTrabajador(Trabajador trabajador){
        Session session = sessionFactory.openSession();
        session.beginTransaction();

        session.delete(trabajador);
        session.getTransaction().commit();
        session.close();
    }

    /**
     * Metodooq que borra un encargado
     * @param encargado Objeto Encargado a borrar
     */
    public void borrarEncargado(Encargado encargado){
        Session session = sessionFactory.openSession();
        session.beginTransaction();

        session.delete(encargado);
        session.getTransaction().commit();
        session.close();
    }

    /**
     * Metodo que borra un recambio
     * @param recambio Objeto Recambio a borrar
     */
    public void borrarRecambio(Recambio recambio){
        Session session = sessionFactory.openSession();
        session.beginTransaction();

        session.delete(recambio);
        session.getTransaction().commit();
        session.close();
    }

    /**
     * Metodo que borra una tienda
     * @param tienda Objeto Tienda a borrar
     */
    public void borrarTienda(Tienda tienda){
        Session session = sessionFactory.openSession();
        session.beginTransaction();

        session.delete(tienda);
        session.getTransaction().commit();
        session.close();
    }

    /**
     * Metodo que borrar un proveedor
     * @param proveedor
     */
    public void borrarProveedor(Proveedor proveedor){
        Session session = sessionFactory.openSession();
        session.beginTransaction();

        session.delete(proveedor);
        session.getTransaction().commit();
        session.close();
    }

    /**
     * Metodo que borra un objeto si es de estas clases (Tienda,Recambio,Trabajador,Encargado)
     * @param objeto Objeto a borrar
     */
    public void borrarObjeto(Object objeto){
        if (objeto instanceof Tienda || objeto instanceof Recambio || objeto instanceof Trabajador || objeto instanceof Encargado){
            Session session = sessionFactory.openSession();
            session.beginTransaction();

            session.delete(objeto);
            session.getTransaction().commit();
            session.close();
        }
    }

    //*********************** Modificación ************************

    /**
     * Metodo que modifica un obejto
     * @param objeto Objeto a modificar
     */
    public void modificarObjeto(Object objeto) {
        Session sesion=sessionFactory.openSession();

        sesion.beginTransaction();
        sesion.saveOrUpdate(objeto);
        sesion.getTransaction().commit();

        sesion.close();
    }

    /**
     * Metodo que modifica una Tienda
     * @param tienda Tienda a modificar
     */
    public void modificarTienda(Tienda tienda) {
        System.out.println("Vamos a modificar la tienda");
        Session sesion = sessionFactory.openSession();
        sesion.beginTransaction();
        sesion.saveOrUpdate(tienda);
        sesion.getTransaction().commit();
        sesion.close();
    }

    /**
     * Metodoq que modifica un recambio
     * @param recambio Recambio a modificar
     */
    public void modificarRecambio(Recambio recambio) {
        Session sesion = sessionFactory.openSession();
        sesion.beginTransaction();
        sesion.saveOrUpdate(recambio);
        sesion.getTransaction().commit();
        sesion.close();
    }

    /**
     * Metodo que modifica un encargado
     * @param encargado Encargado a modificar
     */
    public void modificarEncargado(Encargado encargado) {
        Session sesion = sessionFactory.openSession();
        sesion.beginTransaction();
        sesion.saveOrUpdate(encargado);
        sesion.getTransaction().commit();
        sesion.close();
    }

    /**
     * Metodo que modifica un trabajador
     * @param trabajador Trabajador a modificar
     */
    public void modificarTrabajador(Trabajador trabajador) {
        Session sesion = sessionFactory.openSession();
        sesion.beginTransaction();
        sesion.saveOrUpdate(trabajador);
        sesion.getTransaction().commit();
        sesion.close();
    }

    /**
     * Metodo que modifica un proveedor
     * @param proveedor Proveedor a modificar
     */
    public void modificarProveedor(Proveedor proveedor) {
        Session sesion = sessionFactory.openSession();
        sesion.beginTransaction();
        sesion.saveOrUpdate(proveedor);
        sesion.getTransaction().commit();
        sesion.close();
    }

    //*********************** Get de Obsjetos *********************

    /**
     * Metodo que devuelve todos los trabajadores
     * @return Todos los trabajadores
     */
    public ArrayList<Trabajador> getTrabajadores(){
        Session session = sessionFactory.openSession();
        Query query = session.createQuery("FROM Trabajador");
        ArrayList<Trabajador> lista = (ArrayList<Trabajador>) query.getResultList();
        session.clear();
        return lista;
    }

    /**
     * Metodo que devuelce todos los encargados
     * @return Todos los encargados
     */
    public ArrayList<Encargado> getEncargados(){
        Session session = sessionFactory.openSession();
        Query query = session.createQuery("FROM Encargado");
        ArrayList<Encargado> lista = (ArrayList<Encargado>) query.getResultList();
        session.clear();
        return lista;
    }

    /**
     * Metodo que devuelve todos los remcambios
     * @return Todos los remcambios
     */
    public ArrayList<Recambio> getRecambios(){
        Session session = sessionFactory.openSession();
        Query query = session.createQuery("FROM Recambio");
        ArrayList<Recambio> lista = (ArrayList<Recambio>) query.getResultList();
        session.clear();
        return lista;
    }

    /**
     * Metodo que devuelve todas las tiendas
     * @return Todas las tiendas
     */
    public ArrayList<Tienda> getTiendas(){
        Session session = sessionFactory.openSession();
        Query query = session.createQuery("FROM Tienda");
        ArrayList<Tienda> lista = (ArrayList<Tienda>) query.getResultList();
        session.clear();
        return lista;
    }

    /**
     * Metodo que devuelve todos los proveedores
     * @return Todos los proveedores
     */
    public ArrayList<Proveedor> getProveedores(){
        Session session = sessionFactory.openSession();
        Query query = session.createQuery("FROM Proveedor");
        ArrayList<Proveedor> lista = (ArrayList<Proveedor>) query.getResultList();
        session.clear();
        return lista;
    }

    /**
     * Meotodo que devuelve todas las tiendas con encargado
     * @return Todas las tiendas con encargados
     */
    public ArrayList<Tienda> getTiendasConEncargado(){
        Session session = sessionFactory.openSession();
        Query query = session.createQuery("FROM Tienda where id_encargado!=null");
        ArrayList<Tienda> lista = (ArrayList<Tienda>) query.getResultList();
        session.clear();
        return lista;
    }


    /**
     * Metodo que devuelve todas las tiendas que no tienen encargados
     * @return Todas las tiendas que no tienen encargados
     */
    public ArrayList<Tienda> getTiendasNoEncaragado(){
        Session session = sessionFactory.openSession();
        Query query = session.createQuery("FROM Tienda WHERE id_encargado = null");
        ArrayList<Tienda> lista = (ArrayList<Tienda>) query.getResultList();
        session.clear();
        return lista;
    }

    /**
     * Metodo que devuelve todos los trabajadores de una tienda
     * @param tienda tienda de la que queremos sacar los trabajadores
     * @return ArrayList de trabajadores
     */
    public ArrayList<Trabajador> getTrabajadoresTienda(Tienda tienda){
        Session session = sessionFactory.openSession();
        Query query = session.createQuery("FROM Trabajador WHERE id_tienda = :prop");
        query.setParameter("prop", tienda.getId());
        ArrayList<Trabajador> lista = (ArrayList<Trabajador>) query.getResultList();
        session.clear();
        return lista;
    };
    /*
    public Encargado getEncargado(Encargado encargado){
        Session session = sessionFactory.openSession();
        Query query = session.createQuery("FROM Encargado WHERE nombre = :prop and apellidos = :prop2 and telefono = :prop3");
        query.setParameter("prop", encargado.getNombre());
        query.setParameter("prop2", encargado.getApellidos());
        query.setParameter("prop3", encargado.getTelefono());
        ArrayList<Encargado> lista = (ArrayList<Encargado>) query.getResultList();
        session.clear();
        return lista.get(0);
    }
    */

    /**
     * Metodo que rellena el ComboBox de tienda
     * @param lista lista de tiendas
     * @param combo ComboBox al que las queremos aññadir
     */
    public void setComboBoxTienda(ArrayList<Tienda> lista, JComboBox combo){
        for (Tienda tienda: lista) {
            combo.addItem(tienda);
        }
        combo.setSelectedIndex(-1);
    }

    /**
     * Metodo que rellena el ComboBox de encargado
     * @param lista lista de encargados
     * @param combo ComboBox al que los queremos aññadir
     */
    public void setComboBoxEncargado(ArrayList<Encargado> lista, JComboBox combo){
        for (Encargado encargado: lista) {
            combo.addItem(encargado);
        }
        combo.setSelectedIndex(-1);
    }
}
