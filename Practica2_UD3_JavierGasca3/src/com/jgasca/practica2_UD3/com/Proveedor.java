package com.jgasca.practica2_UD3.com;

import javax.persistence.*;
import java.util.List;
import java.util.Objects;

/**
 * Clase proveedor que contiene los atributos de proveedor
 */
@Entity
public class Proveedor {
    private int id;
    private String nombre;
    private String direccion;
    private String telefono;
    private List<Tienda> tienda;

    /**
     * Metodo que devuelve el ide del proveedor
     * @return id del proveedor
     */
    @Id
    @Column(name = "id")
    public int getId() {
        return id;
    }

    /**
     * Metodo que establece el id del proveedor
     * @param id id del proveedor
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * Metodo que devuelve el nombre del proveedor
     * @return Nombre del proveedor
     */
    @Basic
    @Column(name = "nombre")
    public String getNombre() {
        return nombre;
    }

    /**
     * Meotodo que establece el nombre del proveedor
     * @param nombre Nombre del proveedor
     */
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    /**
     * Metodo que devuelve la dirección del proveedor
     * @return Dirección del proveedor
     */
    @Basic
    @Column(name = "direccion")
    public String getDireccion() {
        return direccion;
    }

    /**
     * Metodo que establece la dirección del proveedor
     * @param direccion Dirección del proveedor
     */
    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    /**
     * Metodo que devuelve el telefono del proveedor
     * @return Telefono del proveedor
     */
    @Basic
    @Column(name = "telefono")
    public String getTelefono() {
        return telefono;
    }

    /**
     * Metodo que establece el telefono del proveedor
     * @param telefono Telefono del proveedor
     */
    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    /**
     * Metodo que compara dos objetos
     * @param o Objeto de la clase proveedor
     * @return True o false dependiendo de si la comparación es correcta o no
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Proveedor proveedor = (Proveedor) o;
        return id == proveedor.id &&
                Objects.equals(nombre, proveedor.nombre) &&
                Objects.equals(direccion, proveedor.direccion) &&
                Objects.equals(telefono, proveedor.telefono);
    }

    /**
     * Metodo que devuelve todos los atributos
     * @return Todos los atributos de la clase
     */
    @Override
    public int hashCode() {
        return Objects.hash(id, nombre, direccion, telefono);
    }

    /**
     * Metodo que devuelve la lista de tiendas
     * @return Lista de tiendas
     */
    @ManyToMany(fetch=FetchType.EAGER)
    @JoinTable(name = "tienda_proveedor" ,catalog = "", schema = "recambios_informatica4", joinColumns = @JoinColumn(name = "id_proveedor", referencedColumnName = "id", nullable = false), inverseJoinColumns = @JoinColumn(name = "id_tienda", referencedColumnName = "id", nullable = false))
    public List<Tienda> getTienda() {
        return tienda;
    }

    /**
     * Metodo que establece un List de tiendas
     * @param tienda List de tienda
     */
    public void setTienda(List<Tienda> tienda) {
        this.tienda = tienda;
    }

    /**
     * Metodo que pasa a String los atributos de la clase que la identifican
     * @return String con los atributos
     */
    @Override
    public String toString() {
        return nombre + "-" + direccion + "-" + telefono;
    }
}
