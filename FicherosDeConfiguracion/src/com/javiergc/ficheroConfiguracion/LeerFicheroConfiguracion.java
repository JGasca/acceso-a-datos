package com.javiergc.ficheroConfiguracion;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

public class LeerFicheroConfiguracion {
    public static void main(String[] args) {
        Properties configuracion = new Properties();
        try {
            configuracion.load(new FileInputStream("configuracion.conf"));
            String usuario = configuracion.getProperty("user");
            String password = configuracion.getProperty("password");
            String servidor = configuracion.getProperty("server");
            int puerto = Integer.valueOf(configuracion.getProperty("port"));
            System.out.println("Usuario: " + usuario + " \nPassswordd: " + password + "\nServidor: " + servidor + "\nPuerto: " + puerto);
        }catch (FileNotFoundException fnfe){
            fnfe.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
