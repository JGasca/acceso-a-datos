package com.javiergasca.Practica2.util;

import com.javiergasca.Practica2.gui.OrdenadorControlador;
import com.javiergasca.Practica2.gui.OrdenadorModelo;
import com.javiergasca.Practica2.gui.Ventana.Ventana;

public class Principal {
    public static void main(String[] args) {
        Ventana vista = new Ventana();
        OrdenadorModelo modelo = new OrdenadorModelo();
        OrdenadorControlador controlador = new OrdenadorControlador(vista, modelo);
    }
}
