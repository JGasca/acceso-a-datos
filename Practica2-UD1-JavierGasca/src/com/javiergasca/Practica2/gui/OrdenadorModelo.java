package com.javiergasca.Practica2.gui;

import com.javiergasca.Practica2.base.Ordenador;
import com.javiergasca.Practica2.base.Portatil;
import com.javiergasca.Practica2.base.SobreMesa;
import org.w3c.dom.*;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.*;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.File;
import java.io.IOException;
import java.time.LocalDate;
import java.util.ArrayList;

public class OrdenadorModelo {
    private ArrayList<Ordenador> listaRecambios;

    public OrdenadorModelo(){listaRecambios = new ArrayList<Ordenador>();}

    public ArrayList<Ordenador> obtenerRecambios(){return listaRecambios;}

    public void altaPortatil(String nombre, String cantidad, String precio, String touchpad, LocalDate fechaOferta){
        Portatil nuevoPortatil = new Portatil(nombre, cantidad, precio, fechaOferta,touchpad);
        listaRecambios.add(nuevoPortatil);
    }

    public void altaSobreMesa(String nombre, String cantidad, String precio, String raton, LocalDate fechaOferta){
        SobreMesa nuevoSobremesa = new SobreMesa(nombre, cantidad, precio, fechaOferta,raton);
        listaRecambios.add(nuevoSobremesa);
    }

    public boolean existeNombre(String nombre) {
        for (Ordenador ordenador:listaRecambios) {
            if(ordenador.getNombre().equals(nombre)) {
                return true;
            }
        }
        return false;
    }

    public boolean eliminarRecambio(String nombre){
        for (Ordenador ordenador:listaRecambios) {
            if(ordenador.getNombre().equals(nombre)) {
                listaRecambios.remove(ordenador);
                return true;
            }
        }
        return false;
    }

    public void exportarXML(File fichero) throws ParserConfigurationException, TransformerException {

        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();
        DOMImplementation dom = builder.getDOMImplementation();
        Document documento = dom.createDocument(null, "xml", null);

        Element raiz = documento.createElement("Recambios");
        documento.getDocumentElement().appendChild(raiz);

        Element nodoRecambios = null, nodoDatos = null;
        Text texto = null;

        for (Ordenador recambio : listaRecambios) {

            /*Añado dentro de la etiqueta raiz (Vehiculos) una etiqueta
            dependiendo del tipo de vehiculo que este almacenando
            (coche o moto)
             */
            if (recambio instanceof Portatil) {
                nodoRecambios = documento.createElement("Portatil");

            } else {
                nodoRecambios = documento.createElement("Sobremesa");
            }
            raiz.appendChild(nodoRecambios);

            /*Dentro de la etiqueta vehiculo le añado
            las subetiquetas con los datos de sus
            atributos (matricula, marca, etc)
             */
            nodoDatos = documento.createElement("nombre");
            nodoRecambios.appendChild(nodoDatos);

            texto = documento.createTextNode(recambio.getNombre());
            nodoDatos.appendChild(texto);

            nodoDatos = documento.createElement("cantidad");
            nodoRecambios.appendChild(nodoDatos);

            texto = documento.createTextNode(recambio.getCantidad());
            nodoDatos.appendChild(texto);

            nodoDatos = documento.createElement("precio");
            nodoRecambios.appendChild(nodoDatos);

            texto = documento.createTextNode(recambio.getPrecio());
            nodoDatos.appendChild(texto);

            nodoDatos = documento.createElement("fecha-oferta");
            nodoRecambios.appendChild(nodoDatos);

            texto = documento.createTextNode(recambio.getFechaOferta().toString());
            nodoDatos.appendChild(texto);

            if (recambio instanceof Portatil) {
                nodoDatos = documento.createElement("touchpad");
                nodoRecambios.appendChild(nodoDatos);
                texto = documento.createTextNode(String.valueOf(((Portatil) recambio).getTouchpad()));
                nodoDatos.appendChild(texto);
            } else {
                nodoDatos = documento.createElement("Raton");
                nodoRecambios.appendChild(nodoDatos);
                texto = documento.createTextNode(String.valueOf(((SobreMesa) recambio).getRaton()));
                nodoDatos.appendChild(texto);
            }

        }

        Source source = new DOMSource(documento);
        Result resultado = new StreamResult(fichero);

        Transformer transformer = TransformerFactory.newInstance().newTransformer();
        transformer.transform(source, resultado);
    }

    public void importarXML(File fichero) throws ParserConfigurationException, IOException, SAXException {
        listaRecambios = new ArrayList<Ordenador>();
        Portatil nuevoPortatil = null;
        SobreMesa nuevoSobremesa = null;

        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();
        Document documento = builder.parse(fichero);

        NodeList listaElementos = documento.getElementsByTagName("*");

        for (int i = 0; i < listaElementos.getLength(); i++) {
            Element nodoOrdenadores = (Element) listaElementos.item(i);

            if (nodoOrdenadores.getTagName().equals("Portatil")) {
                nuevoPortatil = new Portatil();
                nuevoPortatil.setNombre(nodoOrdenadores.getChildNodes().item(0).getTextContent());
                nuevoPortatil.setCantidad(nodoOrdenadores.getChildNodes().item(1).getTextContent());
                nuevoPortatil.setPrecio(nodoOrdenadores.getChildNodes().item(2).getTextContent());
                nuevoPortatil.setFechaOferta(LocalDate.parse(nodoOrdenadores.getChildNodes().item(3).getTextContent()));
                nuevoPortatil.setTouchpad(nodoOrdenadores.getChildNodes().item(4).getTextContent());

                listaRecambios.add(nuevoPortatil);
            } else {
                if (nodoOrdenadores.getTagName().equals("Sobremesa")) {
                    nuevoSobremesa = new SobreMesa();
                    nuevoSobremesa.setNombre(nodoOrdenadores.getChildNodes().item(0).getTextContent());
                    nuevoSobremesa.setCantidad(nodoOrdenadores.getChildNodes().item(1).getTextContent());
                    nuevoSobremesa.setPrecio(nodoOrdenadores.getChildNodes().item(2).getTextContent());
                    nuevoSobremesa.setFechaOferta(LocalDate.parse(nodoOrdenadores.getChildNodes().item(3).getTextContent()));
                    nuevoSobremesa.setRaton(nodoOrdenadores.getChildNodes().item(4).getTextContent());

                    listaRecambios.add(nuevoSobremesa);
                }
            }


        }
    }
}
