DELIMITER //
CREATE PROCEDURE crearTablaRecambios()

BEGIN
    CREATE TABLE IF NOT EXISTS recambios(
		   id int primary key auto_increment NOT NULL,
		   nombre varchar(50) NOT NULL,
		   cantidad varchar(150) NOT NULL,
		   precio varchar(10) NOT NULL,
		   fechaexpira timestamp NOT NULL,
		   tipo varchar(50) NULL);
END //