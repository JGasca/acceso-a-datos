package com.javiergc.Practica1USD2.gui;

import Encriptacion.Encriptar;
import com.javiergc.Practica1USD2.Util.Util;
import com.sun.scenario.effect.impl.sw.sse.SSEBlend_SRC_OUTPeer;

import java.io.*;
import java.sql.*;
import java.time.LocalDateTime;
import java.util.Properties;

/**
 * Clase Modelo
 */
public class Modelo {
    private Connection conexion;
    private String ip;
    private String user;
    private String password;
    private String adminPassword;

    /**
     * Construcotor de la clase Modelo, carga el archivo de configuración
     */
    public Modelo(){
        getPropValues();
    }

    /**
     * Metodo que devuelve la ip
     * @return String ip
     */
    String getIP() {
        return ip;
    }

    /**
     * Metodo que develve el usuario de la base de datos
     * @return Usuario
     */
    String getUser() {
        return user;
    }

    /**
     * Metodo que devuelve la contraseña de la base de datos
     * @return password
     */
    String getPassword() {
        return password;
    }

    /**
     * Metodo que devuelve la contraseña del administrador
     * @return adminpassword
     */
    String getAdminPassword() {
        return adminPassword;
    }

    /**
     * Metodo que crea la conexión con la base de datos
     * @throws SQLException
     */
    public void conectar() throws SQLException {
        conexion=null;
        conexion= DriverManager.getConnection("jdbc:mysql://"+ ip +":3306/recambios_informatica",user,password);
    }

    /**
     * Metodo que cierra la conexión con la base de datos
     * @throws SQLException
     */
    public void desconectar() throws SQLException {
        conexion.close();
        conexion=null;
    }

    /**
     * Metodo que añade un recambio a la base de datos
     * @param nombre String Nombre del recambio
     * @param cantidad String Cantidad del recambio
     * @param precio String Precio del recambio
     * @param fechaExpi Fecha de expiración de la oferta
     * @param tipo Tipo de maquina a la que va dirigido
     * @return Devuelve un numero que corresponde con un error
     * @throws SQLException
     */
    public int addRecambio(String nombre, String cantidad, String precio, LocalDateTime fechaExpi, String tipo) throws SQLException {
        if (conexion==null){
            Util.showErrorAlert("Error, no hay conexión con la base de datos");
            return -1;
        }
        if (conexion.isClosed())
            return -2;

        String consulta="INSERT INTO recambios(nombre, cantidad, precio, fechaexpira, tipo)"+ "VALUES (?,?,?,?,?)";

        PreparedStatement sentencia=null;

        sentencia=conexion.prepareStatement(consulta);

        sentencia.setString(1,nombre);
        sentencia.setString(2,cantidad);
        sentencia.setString(3,precio);
        sentencia.setTimestamp(4,Timestamp.valueOf(fechaExpi));
        sentencia.setString(5,tipo);

        int numeroRegistros=sentencia.executeUpdate();

        if (sentencia!=null) {
            sentencia.close();
        }

        return numeroRegistros;

    }

    /**
     * Metodo que obtiene todos los datos de la tabla recambios
     * @return Devuelve un ResultSet con todos los datos
     * @throws SQLException
     */
    public ResultSet obtenerDatos() throws SQLException {
        if (conexion==null) {
            Util.showErrorAlert("Error al cargar los datos");
            return null;
        }
        if (conexion.isClosed()) {
            return null;
        }
        String consulta ="SELECT * FROM recambios";
        PreparedStatement sentencia = null;
        sentencia=conexion.prepareStatement(consulta);
        ResultSet resultado=sentencia.executeQuery();
        return resultado;
    }

    /**
     * Metodo que modifica un Recambio en la base de datos
     * @param id id del recambio
     * @param nombre String Nombre del recambio
     * @param cantidad String Cantidad del recambio
     * @param precio String Precio del recambio
     * @param fechaExpi Fecha de expiración de la oferta
     * @param tipo Tipo de maquina a la que va dirigido
     * @return Devuelve un numero que corresponde a un codigo de error
     * @throws SQLException
     */
    public int modificarRecambio(int id, String nombre, String cantidad, String precio, LocalDateTime fechaExpi, String tipo) throws SQLException {
        if (conexion==null){
            Util.showErrorAlert("Error, no hay conexión con la base de datos");
            return -1;
        }
        if (conexion.isClosed())
            return -2;

        String consulta="UPDATE recambios SET nombre=?,cantidad = ? , precio = ?, fechaexpira = ?, tipo = ? WHERE id=?";

        PreparedStatement sentencia=null;

        sentencia=conexion.prepareStatement(consulta);

        sentencia.setString(1,nombre);
        sentencia.setString(2,cantidad);
        sentencia.setString(3,precio);
        sentencia.setTimestamp(4,Timestamp.valueOf(fechaExpi));
        sentencia.setString(5,tipo);
        sentencia.setInt(6,id);

        int numeroRegistros=sentencia.executeUpdate();

        if (sentencia!=null) {
            sentencia.close();
        }

        return 0;
    }

    /**
     * Metodo que elmimina un recambio
     * @param id id del recambio que queremos eliminar
     * @return Devuelve un numero que corresponde a un codigo de error
     * @throws SQLException
     */
    public int eliminarRecambio(int id) throws SQLException {
        if (conexion == null){
            return -1;
        }
        if (conexion.isClosed()){
            return -2;
        }

        String consulta = "DELETE FROM recambios WHERE id=?";
        PreparedStatement sentencia = null;

        sentencia=conexion.prepareStatement(consulta);
        sentencia.setInt(1,id);

        int resultado = sentencia.executeUpdate();

        if (sentencia != null){
            sentencia.close();
        }

        return resultado;
    }

    /**
     * Metodo que devuelve un ResultSet con todos los resultados segun el tipo de maquina introducida por parametro
     * @param tipo String tipo de maquina
     * @return Devuelve un Resultset
     * @throws SQLException
     */
    public ResultSet buscarTipo(String tipo) throws SQLException {
        if (conexion == null){
            return null;
        }
        if (conexion.isClosed()){
            return null;
        }
        String consulta = "";
        if (tipo.equals("Todos")){
            consulta = "SELECT * FROM recambios";
        }else{
            tipo = "'" + tipo + "'";
            consulta ="SELECT * FROM recambios WHERE tipo=" + tipo;
        }

        PreparedStatement sentencia = null;
        sentencia=conexion.prepareStatement(consulta);
        ResultSet resultado=sentencia.executeQuery();
        return resultado;
    }

    /**
     * Actualiza el archivo de configuración
     * @param ip String ip de la BBDD
     * @param user String usuario de la BBDD
     * @param pass String contraseña de la BBDD
     * @param adminPass String contraseña del administrador
     */
    void setPropValues(String ip, String user, String pass, String adminPass) {
        Boolean contra = false;
        Boolean contraAdmin = false;

        if (adminPass.equals(adminPassword)){
            contraAdmin = true;
        }
        if (pass.equals(password) && !pass.equals("")){
            contra = true;
        }
        /* Encripto la ip y el usuario, si la contraseña de la BBDD o la contraseña del admin no se han cambiado
            no se vuelven a encriptar.
        */
        try {
            ip = Encriptar.encriptar(ip);
            user = Encriptar.encriptar(user);
            if (!contra){
                pass = Encriptar.encriptar(pass);
            }
            if (!contraAdmin){
                adminPass = Encriptar.hasear(adminPass);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            Properties prop = new Properties();
            prop.setProperty("ip", ip);
            prop.setProperty("user", user);
            if (contra) {
                prop.setProperty("pass", Encriptar.encriptar(getPassword()));
            } else {
                prop.setProperty("pass", pass);
            }
            prop.setProperty("admin", adminPass);

            OutputStream out = new FileOutputStream("config.properties");
            prop.store(out, null);

        } catch (IOException ex) {
            ex.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
        this.ip = ip;
        this.user = user;
        this.password = pass;
        this.adminPassword = adminPass;
    }

    /**
     * Metodo que crea el archivo config.properties y le pasa los datos de la base de datos
     * Este metodo solo sirve en el caso que que no exista el archivo.
     * @param ip String ip de la BBDD
     * @param user String usuario de la BBDD
     * @param pass String contraseña de la BBDD
     * @param adminPass String contraseña del administrador
     */
   void setPropValuesSinEncrip(String ip, String user, String pass, String adminPass) {
        try {
            Properties prop = new Properties();
            prop.setProperty("ip", ip);
            prop.setProperty("user", user);
            prop.setProperty("pass", pass);
            prop.setProperty("admin", adminPass);

            OutputStream out = new FileOutputStream("config.properties");
            prop.store(out, null);

            getPropValues();

        } catch (IOException ex) {
            ex.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Lee el archivo de configuración y carga los atributos
     */
    private void getPropValues() {
        InputStream inputStream = null;
        try {
            Properties prop = new Properties();
            String propFileName = "config.properties";

            inputStream = new FileInputStream(propFileName);

            prop.load(inputStream);
            ip = Encriptar.desencriptar(prop.getProperty("ip"));
            user = Encriptar.desencriptar(prop.getProperty("user"));
            password = Encriptar.desencriptar(prop.getProperty("pass"));
            adminPassword = prop.getProperty("admin");
        } catch (Exception e) {
            System.out.println("Exception: " + e);
        } finally {
            try {
                if (inputStream != null) inputStream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Metodo que crea la tabla recambios si no esta creada
     * @throws SQLException
     */
    public void crearTablaRecambios() throws SQLException {
        conectar();

        String sentenciaSql="call crearTablaRecambios()";
        CallableStatement procedimiento=null;
        procedimiento=conexion.prepareCall(sentenciaSql);
        procedimiento.execute();

        desconectar();
    }
}
