package com.javiergc.Practica1USD2.gui;

import com.github.lgooddatepicker.components.DateTimePicker;
import com.javiergc.Practica1USD2.emun.TipoMaquina;

import javax.swing.*;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import java.awt.*;

/**
 * Clase que crea la vista del programa
 */
public class Vista {
    private JPanel panel1;
    JLabel lblNombre;
    JLabel lblCantidad;
    JTextField txtNombre;
    JTextField txtCantidad;
    JLabel lblPrecio;
    JTextField txtPrecio;
    JButton btnNuevo;
    JButton btnEliminar;
    JButton btnModificar;
    JTable table;
    JComboBox combo_tipo2;
    JButton btnBuscar;
    JComboBox combo_tipo;
    JLabel lblAccion;
    DateTimePicker dateTimePicker;

    DefaultTableModel dtm;

    JMenuItem itemConectar;
    JMenuItem itemOpcionesBBDD;
    JMenuItem itemSalir;
    JMenuItem itemCrearTablaRecambios;

    JFrame frame;

    /*OPTION DIALOG*/
    OptionDialog optionDialog;
    /*SAVECHANGESDIALOG*/
    JDialog adminPasswordDialog;
    JButton btnValidate;
    JPasswordField adminPassword;

    /**
     * Constructor de la clase Vista
     */
    public Vista() {
        frame = new JFrame("Vista");
        frame.setContentPane(panel1);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setSize(new Dimension(frame.getWidth()+200,frame.getHeight()+23));
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);

        dtm =new DefaultTableModel(){
            @Override
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        };
        table.setModel(dtm);

        optionDialog = new OptionDialog(frame);
        crearMenu();
        setEnumComboBox();
        setAdminDialog();

        DefaultTableCellRenderer centerRenderer = new DefaultTableCellRenderer();
        centerRenderer.setHorizontalAlignment( JLabel.CENTER );
        table.setDefaultRenderer(String.class, centerRenderer);

        btnNuevo.setEnabled(false);
        btnModificar.setEnabled(false);
        btnBuscar.setEnabled(false);
        btnEliminar.setEnabled(false);
    }

    /**
     * Metodo que crea el menu de arriba con las opciones de conectar, opciones BBDD, CrearTablaRecambios y salir
     */
    private void crearMenu() {
        itemConectar = new JMenuItem("Conectar");
        itemConectar.setActionCommand("Conectar");
        itemOpcionesBBDD = new JMenuItem("Opciones BBDD");
        itemOpcionesBBDD.setActionCommand("Opciones BBDD");
        itemCrearTablaRecambios = new JMenuItem("Crear tabla recambios");
        itemCrearTablaRecambios.setActionCommand("crearTablaRecambios");
        itemSalir = new JMenuItem("Salir");
        itemSalir.setActionCommand("Salir");

        JMenu menuArchivo = new JMenu("Archivo");
        menuArchivo.add(itemConectar);
        menuArchivo.add(itemOpcionesBBDD);
        menuArchivo.add(itemCrearTablaRecambios);
        menuArchivo.add(itemSalir);
        JMenuBar barraMenu = new JMenuBar();
        barraMenu.add(menuArchivo);

        frame.setJMenuBar(barraMenu);
    }

    /**
     * Metodo que establece los valores de los comboBox
     */
    public void setEnumComboBox(){
        combo_tipo2.addItem("Todos");
        for(TipoMaquina constant: TipoMaquina.values()) {
            combo_tipo.addItem(constant.getValor());
            combo_tipo2.addItem(constant.getValor());
        }
        combo_tipo.setSelectedIndex(-1);
    }

    /**
     * Metodo que inicializa la ventana de administración de los datos de la base de datos
     */
    private void setAdminDialog() {
        //contraseña para validar 1234
        btnValidate = new JButton("Validar");
        btnValidate.setActionCommand("abrirOpciones");
        adminPassword = new JPasswordField();
        adminPassword.setPreferredSize(new Dimension(100, 26));
        Object[] options = new Object[] {adminPassword, btnValidate};
        JOptionPane jop = new JOptionPane("Introduce la contraseña"
                , JOptionPane.WARNING_MESSAGE, JOptionPane.YES_NO_OPTION, null, options);

        adminPasswordDialog = new JDialog(frame,"Opciones",true);
        adminPasswordDialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
        adminPasswordDialog.setContentPane(jop);
        adminPasswordDialog.pack();
        adminPasswordDialog.setLocationRelativeTo(frame);
    }
}
