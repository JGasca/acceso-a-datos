package com.javiergc.Practica1USD2.gui;

import Encriptacion.Encriptar;
import com.javiergc.Practica1USD2.Util.Util;

import javax.swing.*;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.DefaultTableModel;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

/**
 * Clase Controlador
 */
public class Controlador implements ActionListener, TableModelListener {
    private Vista vista;
    private Modelo modelo;

    private enum tipoEstado {conectado, desconectado};
    private tipoEstado estado;

    /**
     * Constructor de la clase Controlador
     * @param vista Vista de nuestro programa
     * @param modelo Modelo de nuestro programa
     */
    public Controlador(Vista vista, Modelo modelo){
        this.modelo = modelo;
        File archivo = new File("config.properties");
        if (!archivo.exists()){
            modelo.setPropValuesSinEncrip("un6GhAmO3VCleL4w17yHJA==","VrBD1zB/fmBckQYAO2rlEw==","s6a2jguPXMTIQiaqBcTGvg==","1ARVn2Auq2/WAqx2gNrL+q3RNjAzXpUfCXrzkA6d4Xa22yhRLy4AC50E+6UTPoscbo31nbOoq51gvkuXzJ6B2w==");
        }
        this.vista = vista;
        setOptions();
        estado = tipoEstado.desconectado;
        iniciarTabla();
        addActionListener(this);
        addActionListenerTable();
    }

    /**
     * Metodo que establece los listener para todos los botones
     * @param listener ActionListener listener
     */
    private void addActionListener(ActionListener listener){
        vista.btnBuscar.addActionListener(listener);
        vista.btnEliminar.addActionListener(listener);
        vista.btnNuevo.addActionListener(listener);
        vista.btnModificar.addActionListener(listener);
        vista.itemConectar.addActionListener(listener);
        vista.itemSalir.addActionListener(listener);
        vista.itemOpcionesBBDD.addActionListener(listener);
        vista.btnValidate.addActionListener(listener);
        vista.optionDialog.btnGuardar.addActionListener(listener);
        vista.itemCrearTablaRecambios.addActionListener(listener);
    }

    /**
     * Metodo que esta a la escucha de cuando se selecciona una fila del JTable
     */
    private void addActionListenerTable(){
        vista.table.addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent e) {
                int row = vista.table.getSelectedRow();
                int col = vista.table.getSelectedColumn();
                vista.txtNombre.setText((String) vista.table.getValueAt(row,1));
                vista.txtCantidad.setText((String) vista.table.getValueAt(row,2));
                vista.txtPrecio.setText((String) vista.table.getValueAt(row,3));
                vista.dateTimePicker.setDateTimeStrict(formatearFecha(vista.table.getValueAt(row,4).toString()));
                vista.combo_tipo.setSelectedItem(vista.table.getValueAt(row,5));
                vista.btnNuevo.setEnabled(false);
            }
        });
    }

    /**
     * Metodo que formatea la fecha de String a LocalDateTime
     * @param fecha fecha en formato String
     * @return Devuelve la fecha en formato LocalDateTime
     */
    private LocalDateTime formatearFecha(String fecha){
        fecha =  fecha.substring(0, fecha.length() - 2);
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        LocalDateTime fecha2 = LocalDateTime.parse(fecha,formatter);
        return fecha2;
    }

    /**
     * Metodo que establece las cabeceras de la tabla
     */
    private void iniciarTabla(){
        String[] headers = {"id", "nombre", "cantidad","precio","fecha expiracion", "tipo"};
        vista.dtm.setColumnIdentifiers(headers);
    }

    /**
     * Metodo que carga los datos recibios en un JTable
     * @param resultSet Resultado de una consulta de obtención de datos
     * @throws SQLException
     */
    private void cargarFilas(ResultSet resultSet) throws SQLException {
        vista.btnNuevo.setEnabled(true);
        Object[] fila = new Object[6];
        vista.dtm.setRowCount(0);

        while(resultSet.next()){
            fila[0]=resultSet.getObject(1);
            fila[1]=resultSet.getObject(2);
            fila[2]=resultSet.getObject(3);
            fila[3]=resultSet.getObject(4);
            fila[4]=resultSet.getObject(5);
            fila[5]=resultSet.getObject(6);

            vista.dtm.addRow(fila);
        }
        if (resultSet.last()){
            vista.lblAccion.setVisible(true);
            vista.lblAccion.setText(resultSet.getRow() + " filas cargadas");
        }
    }

    /**
     * Metodo que ejecuta lo que correnponda segun el boton seleccionado
     * @param e
     */
    @Override
    public void actionPerformed(ActionEvent e) {
        String comando = e.getActionCommand();

        switch (comando){
            case "Nuevo":
                try {
                    if (vista.txtNombre.getText() != null && vista.lblCantidad.getText() != null && vista.txtPrecio.getText() != null && vista.dateTimePicker.getDateTimePermissive() != null && vista.combo_tipo.getSelectedItem() != null && isNumeric(vista.txtPrecio.getText()) && isNumeric(vista.txtCantidad.getText()) ){
                        int error = modelo.addRecambio(vista.txtNombre.getText(),vista.txtCantidad.getText(),vista.txtPrecio.getText(),vista.dateTimePicker.getDateTimePermissive(),vista.combo_tipo.getSelectedItem().toString());
                        System.out.println(error);
                        limpiarCampos();
                        cargarFilas(modelo.obtenerDatos());
                        vista.lblAccion.setText("Recambio añadido");
                    }else {
                        if (!isNumeric(vista.txtPrecio.getText())|| !isNumeric(vista.txtCantidad.getText())){
                            Util.showErrorAlert("Precio o cantidad no son numericos");
                        }else {
                            Util.showErrorAlert("No has rellenado todos los campos");
                        }
                    }
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
                break;
            case "Eliminar":
                vista.btnNuevo.setEnabled(true);
                try {
                    int filaBorrar = vista.table.getSelectedRow();
                    int idBorrar = (int) vista.dtm.getValueAt(filaBorrar,0);
                    modelo.eliminarRecambio(idBorrar);
                    vista.dtm.removeRow(filaBorrar);
                    vista.lblAccion.setText("Fila eliminada");
                    limpiarCampos();
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
                break;
            case "Modificar":
                int row = vista.table.getSelectedRow();
                if (vista.table.getSelectedRow() != -1){
                    int id = (int) vista.table.getValueAt(row,0);
                    try {
                        modelo.modificarRecambio(id,vista.txtNombre.getText(),vista.txtCantidad.getText(),vista.txtPrecio.getText(),vista.dateTimePicker.getDateTimePermissive(),vista.combo_tipo.getSelectedItem().toString());
                        cargarFilas(modelo.obtenerDatos());
                        vista.lblAccion.setText("Recambio modificado");
                    } catch (SQLException ex) {
                        ex.printStackTrace();
                        Util.showErrorAlert("Erro al modificar el recambio");
                    }
                }else {
                    Util.showWarningAlert("Selecciona el recambio a modificar");
                }
                limpiarCampos();
                break;
            case "Buscar":
                String tipo = vista.combo_tipo2.getSelectedItem().toString();
                try {
                    cargarFilas(modelo.buscarTipo(tipo));
                    vista.lblAccion.setText("Busqueda completada");
                } catch (SQLException ex) {
                    ex.printStackTrace();
                }
                break;
            case "Salir":
                System.exit(0);
                break;
            case "Conectar":
                if (estado == tipoEstado.desconectado){
                    try {
                        modelo.conectar();
                        vista.itemConectar.setText("Desconectar");
                        estado = tipoEstado.conectado;
                        vista.lblAccion.setText("Conectado");
                        cargarFilas(modelo.obtenerDatos());

                        vista.btnNuevo.setEnabled(true);
                        vista.btnModificar.setEnabled(true);
                        vista.btnBuscar.setEnabled(true);
                        vista.btnEliminar.setEnabled(true);
                    } catch (SQLException ex) {
                        JOptionPane.showMessageDialog(null,"Error de conexión", "Error", JOptionPane.ERROR_MESSAGE);
                        ex.printStackTrace();
                    }

                }else {
                    try {

                        modelo.desconectar();
                        vista.itemConectar.setText("Conectar");
                        estado = tipoEstado.desconectado;
                        vista.lblAccion.setText("Desconectando");
                        vista.lblAccion.setVisible(true);
                        limpiarTabla(vista.table);
                        limpiarCampos();

                        vista.btnNuevo.setEnabled(false);
                        vista.btnModificar.setEnabled(false);
                        vista.btnBuscar.setEnabled(false);
                        vista.btnEliminar.setEnabled(false);
                    } catch (SQLException ex) {
                        JOptionPane.showMessageDialog(null,"Error al desconectar", "Error", JOptionPane.ERROR_MESSAGE);
                    }
                }
                break;
            case "Opciones BBDD":
                vista.adminPasswordDialog.setVisible(true);
                break;
            case "abrirOpciones":

                Boolean verifi = null;
                try {
                    verifi = Encriptar.verificarhas(modelo.getAdminPassword(),String.valueOf(vista.adminPassword.getPassword()));
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
                if(verifi) {
                    vista.adminPassword.setText("");
                    vista.adminPasswordDialog.dispose();
                    vista.optionDialog.setVisible(true);
                } else {
                    Util.showErrorAlert("La contraseña introducida no es correcta.");
                }
                break;
            case "Guardar":
                String contrasenaAdmin;
                if (vista.optionDialog.txtContrasenaAdmin.getText().isEmpty()){
                    contrasenaAdmin = modelo.getAdminPassword();
                }else {
                    contrasenaAdmin = vista.optionDialog.txtContrasenaAdmin.getText();
                }

                String contrasena;
                if (vista.optionDialog.txtContrasena.getText().isEmpty()){
                    contrasena = "";
                }else {
                    contrasena = vista.optionDialog.txtContrasena.getText();
                }
                modelo.setPropValues(vista.optionDialog.txtIp.getText(), vista.optionDialog.txtUsuario.getText(),
                        contrasena, contrasenaAdmin);
                vista.optionDialog.dispose();
                vista.frame.dispose();
                new Controlador(new Vista(), new Modelo());
                break;
            case "crearTablaRecambios":
                try {
                    modelo.crearTablaRecambios();
                } catch (SQLException ex) {
                    Util.showErrorAlert("Error al crear la tabla recambios");
                    ex.printStackTrace();
                }
                break;
        }
    }

    /**
     * Metodo que vacia todos los campos del programa
     */
    private void limpiarCampos() {
        vista.txtPrecio.setText(null);
        vista.txtCantidad.setText(null);
        vista.txtNombre.setText(null);
        vista.txtPrecio.setText(null);
        vista.dateTimePicker.setDateTimePermissive(null);
        vista.combo_tipo.setSelectedIndex(-1);
        vista.txtNombre.requestFocus();
    }

    /**
     * Metodo que eliminar todas las filas del JTable
     * @param tabla JTable de nuestro programa
     */
    public void limpiarTabla(JTable tabla){
        try {
            DefaultTableModel modelo=(DefaultTableModel) tabla.getModel();
            int filas=tabla.getRowCount();
            for (int i = 0;filas>i; i++) {
                modelo.removeRow(0);
            }
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Error al limpiar la tabla.");
        }
    }

    /**
     * Metodo que establece la ip, usuario y password de la base de datos en la ventana de administracion de la base de datos
     */
    private void setOptions() {
        vista.optionDialog.txtIp.setText(modelo.getIP());
        vista.optionDialog.txtUsuario.setText(modelo.getUser());
        vista.optionDialog.txtContrasena.setText(modelo.getPassword());
    }

    /**
     * Metodo que comprueba si una cadena solo contiene numeros
     * @param cadena String cadena a analizar
     * @return Boolean true o false
     */
    public boolean isNumeric(String cadena) {
        boolean resultado;
        try {
            Integer.parseInt(cadena);
            resultado = true;
        } catch (NumberFormatException excepcion) {
            try {
                Float.parseFloat(cadena);
                resultado = true;
            }catch (NumberFormatException excepcion2){
                resultado = false;
            }
        }
        return resultado;
    }

    @Override
    public void tableChanged(TableModelEvent e) {

    }
}
