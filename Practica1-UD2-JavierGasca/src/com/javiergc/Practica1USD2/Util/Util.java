package com.javiergc.Practica1USD2.Util;

import javax.swing.*;

/**
 * Clase Util que contiene los mensajes de error que se pueden lanzar
 */
public class Util {
    /**
     * Metodo que lanza un mensaje de error introducido por parametro
     * @param message Mensaje que queremos que se muestre
     */
    public static void showErrorAlert(String message) {
        JOptionPane.showMessageDialog(null, message, "Error", JOptionPane.ERROR_MESSAGE);
    }

    /**
     * Metodo que lanza un mensaje de alerta introducido por parametro
     * @param mensaje Mensaje que queremos que se muestre
     */
    public static void showWarningAlert(String mensaje){
        JOptionPane.showMessageDialog(null, mensaje, "Error", JOptionPane.WARNING_MESSAGE);
    }
}
